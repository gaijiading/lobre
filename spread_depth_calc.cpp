/*
(C) Copyright 2012 The Board of Trustees of the University of Illinois.
All rights reserved.

Developed by:

                        Department of Finance
                University of Illinois, Urbana Champaign

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal with the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of the Department of Finance, the University of Illinois, 
nor the names of its contributors may be used to endorse or promote products 
derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
THE SOFTWARE.
*/

/*****************************************************************************

    File Name   [spread_depth_calc.cpp]

    Synopsis    [Bid-Ask spreads and market depth computation.]

    Description [.]
	
	History     [Originally designed by Yao Chen and written in STATA code 
	             format (v0.1); C++ porting to UNIX environment and code 
				 optimization by Jiading Gai.]

    Revision    [0.1; Yao Chen, Finance Department UIUC]
    Revision    [1.0; Jiading Gai, Finance Department UIUC]
    
	Date        [08/12/2013]

******************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <omp.h>
#include <cmath>
#include <vector>
#include <iomanip>

#include "LobUtility.h"

using namespace std;


void spread_depth_analysis(const char *stock,
						   const char *date,
						   const char *path_to_LOB_stock_date_csv,
                           const char *path_to_fleeting_ref_stock_date_csv,
						   const char *path_to_LOB_C_date,
						   const char *path_to_analysis_output_csv,
						   const char *path_to_midpt_ret_csv)
{
    int pos_in = 0, pos_at = 0, pos_out = 0;
    int pos_outlier = 0, pos_10cent = 0, add_n_flt = 0, add_n_total = 0;

    double sz_wt_eff_spread = 0.0;
    double td_wt_eff_spread = 0.0;
	
	double wt_dep_s = 0.0;
	double wt_dep_b = 0.0;
	double wt_dep10_b = 0.0;
	double wt_dep10_s = 0.0;
	
    double qt_spread = 0.0;
	
	vector<double> ret;
	vector<double> hour;
	vector<double> minute; 


    ofstream analysis_output;
    analysis_output.open(path_to_analysis_output_csv);
    analysis_output << "date" << "," 
                    << "stock" << ","
		            << "wt_dep_s" << ","
				    << "wt_dep_b" << ","
				    << "wt_dep10_b" << ","
				    << "wt_dep10_s" << ","
				    << "qt_spread" << ","
				    << "sz_wt_eff_spread" << ","
				    << "td_wt_eff_spread" << ","
				    << "pos_in" << ","
				    << "pos_at" << ","
				    << "pos_out" << ","
				    << "pos_outlier" << ","
				    << "pos_10cent" << ","
				    << "add_n_flt" << ","
				    << "add_n_total" << endl;
	
	
	ofstream midpt_ret;
	midpt_ret.open(path_to_midpt_ret_csv);
    midpt_ret << "date"   << ","
	          << "stock"  << ","
              << "hour"   << ","
              << "minute" << ","
              << "ret" << endl;


	
	unsigned int in_len = 0, trimmed_len = 0;
	struct LOB_stock_date *Lsd = read_LOB_stock_date_message
   								 (path_to_LOB_stock_date_csv,&in_len);

    struct LOB_stock_date *LOB_AA_nh = 
          keep_between_time_range(Lsd,in_len,&trimmed_len,34200,57600);

    free(Lsd);
    
	if (trimmed_len <= 0) {
        free(LOB_AA_nh);
	} else {
	    unsigned int fr_len = 0;
        struct fleeting_ref *fr = read_fleeting_ref_message
  	                              (path_to_fleeting_ref_stock_date_csv,&fr_len);
        #if 0
        printf("%d\n",fr_len);
        #endif

        qsort(LOB_AA_nh,trimmed_len,sizeof(struct LOB_stock_date),
		      compare_addref_LOB_stock_date);
        qsort(fr,fr_len,sizeof(struct fleeting_ref),compare_ref_fleeting_ref);


        int tracker_fleeting = 0;
        for (unsigned int i = 0; i < trimmed_len; i++) {
		    if (!strcmp(LOB_AA_nh[i].addmsg,"A") || 
			    !strcmp(LOB_AA_nh[i].addmsg,"F") || 
			    !strcmp(LOB_AA_nh[i].addmsg,"Au")) {
		        for (unsigned int j = tracker_fleeting; j < fr_len; j++) {
			        if (LOB_AA_nh[i].addref == fr[j].ref) {
				        if (((LOB_AA_nh[i].addprice < LOB_AA_nh[i].ask) && 
					         (LOB_AA_nh[i].addbuy == 'S')) ||
				            ((LOB_AA_nh[i].addprice > LOB_AA_nh[i].bid) && 
					         (LOB_AA_nh[i].addbuy == 'B'))) {
					        pos_in++;
				        }
				        if (((LOB_AA_nh[i].addprice == LOB_AA_nh[i].ask) && 
					         (LOB_AA_nh[i].addbuy == 'S')) ||
				            ((LOB_AA_nh[i].addprice == LOB_AA_nh[i].bid) && 
					         (LOB_AA_nh[i].addbuy == 'B'))) {
				            pos_at++;
				        }
                        if (((LOB_AA_nh[i].addprice > LOB_AA_nh[i].ask) && 
					         (LOB_AA_nh[i].addbuy == 'S')) ||
				            ((LOB_AA_nh[i].addprice < LOB_AA_nh[i].bid) && 
					         (LOB_AA_nh[i].addbuy == 'B'))) {
					        pos_out++;
				        }
				        if (((LOB_AA_nh[i].addprice > (1.25f * LOB_AA_nh[i].ask)) && 
					         (LOB_AA_nh[i].addbuy == 'S')) ||
				            ((LOB_AA_nh[i].addprice < (0.75f * LOB_AA_nh[i].bid)) && 
					         (LOB_AA_nh[i].addbuy == 'B'))) {
					        pos_outlier++;
				        }
				        if (((LOB_AA_nh[i].addprice > (LOB_AA_nh[i].ask + 0.1f)) && 
					         (LOB_AA_nh[i].addbuy == 'S')) ||
				            ((LOB_AA_nh[i].addprice < (LOB_AA_nh[i].bid - 0.1f)) && 
					         (LOB_AA_nh[i].addbuy == 'B'))) {
				            pos_10cent++;
				        }
				        add_n_flt++;
                    }
                }
                add_n_total++;
            }
        }
	
   
        // part 3.
	    unsigned int lcd_len = 0;
        struct LOB_C_date *lcd = read_LOB_C_date_message(path_to_LOB_C_date,
													 &lcd_len);
    
        char the_stock_id[9];
	    strcpy(the_stock_id,stock);	
	    struct LOB_stock_date *LOB_AA_nh2 = (struct LOB_stock_date *) 
                             calloc(trimmed_len,sizeof(struct LOB_stock_date));
        memcpy(LOB_AA_nh2,LOB_AA_nh,trimmed_len * sizeof(struct LOB_stock_date));

        for (unsigned int i = 0; i < trimmed_len; i++) {
		    if (!strcmp(LOB_AA_nh2[i].addmsg,"C")) {
			    for (unsigned int j = 0; j < lcd_len; j++) {
				    if (!strcmp(lcd[j].stock,the_stock_id) 
					    && (lcd[j].seqcurrent == LOB_AA_nh2[i].addseq)) {
					    LOB_AA_nh2[i].addprice = lcd[j].exec_price;
				    }
			    }
		    }
	    }

        int sum_addshare = 0;
        for (unsigned int i = 0; i < trimmed_len; i++) {
            if (!strcmp(LOB_AA_nh2[i].addmsg,"C") ||
			    !strcmp(LOB_AA_nh2[i].addmsg,"E")) {
			    sum_addshare += LOB_AA_nh2[i].addshare;
		    }
	    }

        int count_EC = 0;
        for (unsigned int i = 0; i < trimmed_len; i++) {
            if (!strcmp(LOB_AA_nh2[i].addmsg,"C") ||
                !strcmp(LOB_AA_nh2[i].addmsg,"E")) {

                double the_bid = LOB_AA_nh2[i].bid;
                double the_ask = LOB_AA_nh2[i].ask;
                double midpt = (the_bid + the_ask) / 2.0;

                int addshare = LOB_AA_nh2[i].addshare;
                double addprice = LOB_AA_nh2[i].addprice;
                double eff_spread = 0.0;
                if (LOB_AA_nh2[i].addbuy == 'S') {
                    eff_spread = addprice - midpt;
                } else if (LOB_AA_nh2[i].addbuy == 'B') {
                    eff_spread = midpt - addprice;
                }
                sz_wt_eff_spread += (eff_spread * addshare) / (double)sum_addshare;
                td_wt_eff_spread += eff_spread;
                count_EC++;
		    }
	    }
        td_wt_eff_spread /= (double)count_EC;
	

	    // part 4.
        qsort(LOB_AA_nh,trimmed_len,sizeof(struct LOB_stock_date),
		      compare_addseq_LOB_stock_date);


        for (unsigned int i = 0; i < (trimmed_len - 1); i++) {
		    double lag_addtime = LOB_AA_nh[i+1].addtime;
            double addtime = LOB_AA_nh[i].addtime;
            double time_dif = lag_addtime - addtime;
            int depth_s = 
                   LOB_AA_nh[i].depth_s == -1 ? 0 : LOB_AA_nh[i].depth_s;
            int depth_b = 
                   LOB_AA_nh[i].depth_b == -1 ? 0 : LOB_AA_nh[i].depth_b;
            int depth10_s = 
                   LOB_AA_nh[i].depth10_s == -1 ? 0 : LOB_AA_nh[i].depth10_s;
            int depth10_b = 
                   LOB_AA_nh[i].depth10_b == -1 ? 0 : LOB_AA_nh[i].depth10_b;
		
            wt_dep_s += (time_dif * (double)depth_s) / 23400.0;
            wt_dep_b += (time_dif * (double)depth_b) / 23400.0;
            wt_dep10_s += (time_dif * (double)depth10_s) / 23400.0;
            wt_dep10_b += (time_dif * (double)depth10_b) / 23400.0;
        }


        int the_next_nonempty = 0;
        int ppl = 0;
        for (unsigned int i = 0; i < (trimmed_len - 1); i++) {
		    double ask = LOB_AA_nh[i].ask;
            double bid = LOB_AA_nh[i].bid;

            double lag_addtime = 0.0;
            double addtime = 0.0;
            double time_dif = 0.0;

            double spread = 0.0;

            if (ask >= 0.0 && bid >= 0.0) {
                spread = (ask - bid);
                addtime = LOB_AA_nh[i].addtime;
                for (unsigned int j = i + 1; j < (trimmed_len); j++) {
                     if (LOB_AA_nh[j].ask >= 0.0 && LOB_AA_nh[j].bid >= 0.0) {
                         the_next_nonempty = j;
                         break;
                     }
                }
			    lag_addtime = LOB_AA_nh[the_next_nonempty].addtime;
			    time_dif = lag_addtime - addtime;

			    qt_spread += (time_dif * spread) / 23400.0;
                ppl++;
	        }
        }


        analysis_output << date   << "," 
	                    << stock  << ","
			            << setprecision(9) << wt_dep_s   << ","
					    << setprecision(9) << wt_dep_b   << ","
					    << setprecision(9) << wt_dep10_b << ","
					    << setprecision(9) << wt_dep10_s << ","
		                << setprecision(9) << qt_spread  << ","
                        << setprecision(9) << sz_wt_eff_spread << ","
                        << setprecision(9) << td_wt_eff_spread << ","
                        << pos_in  << ","
                        << pos_at  << ","
                        << pos_out << ","
                        << pos_outlier << ","
                        << pos_10cent << ","
                        << add_n_flt << ","
                        << add_n_total << endl;


        // last part
        double midpt_prev = 0.0, midpt_now = 0.0;
        double new_hour = 0.0;
        double new_minute = 0.0;
        double old_hour = floor(LOB_AA_nh[0].addtime / 3600.0);
        double old_minute = floor((LOB_AA_nh[0].addtime - old_hour * 3600.0) / 60.0);

        for (unsigned int i = 0; i < trimmed_len; i++) {
             double addtime = LOB_AA_nh[i].addtime;
             double new_hour = floor(addtime / 3600.0);
             double new_minute = floor((addtime - new_hour * 3600.0) / 60.0);

             double the_bid = LOB_AA_nh[i].bid;
             double the_ask = LOB_AA_nh[i].ask;

             if ((old_hour == new_hour) && (old_minute == new_minute)) {
			     midpt_now = (the_bid + the_ask) / 2.0;
		     } else {
			     hour.push_back(old_hour);
			     minute.push_back(old_minute);

			     if (midpt_prev != 0.0) {
			         double the_ret = log(midpt_now / midpt_prev);
				     ret.push_back(the_ret);
			     } else {
			         ret.push_back(-999999.0);
			     }

			     midpt_prev = midpt_now;
			     old_hour = new_hour;
			     old_minute = new_minute;
		     }
	    }

 	    if (midpt_prev != 0.0) {
		    double the_ret = log(midpt_now / midpt_prev);
		    ret.push_back(the_ret);
	        hour.push_back(old_hour);
	        minute.push_back(old_minute);
	    } else {
	        hour.push_back(old_hour);
	        minute.push_back(old_minute);
 		    ret.push_back(-999999.0);
	    }

        for (vector<double>::size_type i = 0; i != ret.size(); i++) {
    	     midpt_ret << date << ","
		               << stock << ","
				       << hour[i] << ","
                       << minute[i] << ","
                       << ret[i] << endl;
        }
		
        free(LOB_AA_nh);
        free(fr);
        free(lcd);
    }
    analysis_output.close();					
    midpt_ret.close();
}

void spread_depth_analysis_wof(const char *stock,
							   const char *date,
							   const char *path_to_LOB_wof_stock_date_csv,
							   const char *path_to_LOB_C_date,
							   const char *path_to_analysis_output_wof_csv,
							   const char *path_to_midpt_ret_wof_csv)
{
	double wt_dep_s = 0.0;
	double wt_dep_b = 0.0;
	double wt_dep10_b = 0.0;
	double wt_dep10_s = 0.0;
	double sz_wt_eff_spread = 0.0;
	double td_wt_eff_spread = 0.0;

	double qt_spread = 0.0;

	vector<double> ret;
	vector<double> hour;
	vector<double> minute; 

	ofstream analysis_output;
	analysis_output.open(path_to_analysis_output_wof_csv);
    analysis_output << "date"   << "," 
	                << "stock"     << ","
					<< "wt_dep_s"   << ","
					<< "wt_dep_b"   << ","
					<< "wt_dep10_b" << ","
					<< "wt_dep10_s" << ","
					<< "qt_spread"  << ","
					<< "sz_wt_eff_spread" << ","
					<< "td_wt_eff_spread" << endl;

	ofstream midpt_ret;
	midpt_ret.open(path_to_midpt_ret_wof_csv);
   	midpt_ret << "date"   << ","
	          << "stock"  << ","
			  << "hour"   << ","
			  << "minute" << ","
			  << "ret" << endl;

	unsigned int in_len = 0, trimmed_len = 0;
	struct LOB_stock_date *Lsd = read_LOB_stock_date_message
   								 (path_to_LOB_wof_stock_date_csv,&in_len);
  	struct LOB_stock_date *LOB_AA_nh = keep_between_time_range
  									   (Lsd,in_len,&trimmed_len,34200,57600);
	free(Lsd);
    
    if (trimmed_len <= 0) {
		free(LOB_AA_nh);
    } else {

	qsort(LOB_AA_nh,trimmed_len,sizeof(struct LOB_stock_date),
		  compare_addref_LOB_stock_date);

   
    // part 3.
	unsigned int lcd_len = 0;
    struct LOB_C_date *lcd = read_LOB_C_date_message(path_to_LOB_C_date, 
													 &lcd_len);
    
	char the_stock_id[9];
	strcpy(the_stock_id,stock);	
	struct LOB_stock_date *LOB_AA_nh2 = (struct LOB_stock_date *) 
						   calloc(trimmed_len,sizeof(struct LOB_stock_date));
	memcpy(LOB_AA_nh2,LOB_AA_nh,trimmed_len * sizeof(struct LOB_stock_date));

	for (unsigned int i = 0; i < trimmed_len; i++) {
		if (!strcmp(LOB_AA_nh2[i].addmsg,"C")) {
			for (unsigned int j = 0; j < lcd_len; j++) {
				if (!strcmp(lcd[j].stock,the_stock_id) 
					&& (lcd[j].seqcurrent==LOB_AA_nh2[i].addseq)) {
					LOB_AA_nh2[i].addprice = lcd[j].exec_price;
				}
			}
		}
	}


	int sum_addshare = 0;
	for (unsigned int i = 0; i < trimmed_len; i++) {
		if (!strcmp(LOB_AA_nh2[i].addmsg,"C") ||
			!strcmp(LOB_AA_nh2[i].addmsg,"E")) {
			sum_addshare += LOB_AA_nh2[i].addshare;
		}
	}
	int count_EC = 0;
	for (unsigned int i = 0; i < trimmed_len; i++) {
		if (!strcmp(LOB_AA_nh2[i].addmsg,"C") ||
			!strcmp(LOB_AA_nh2[i].addmsg,"E")) {

		double the_bid = LOB_AA_nh2[i].bid;
		double the_ask = LOB_AA_nh2[i].ask;
		double midpt = (the_bid + the_ask) / 2.0;

		int addshare = LOB_AA_nh2[i].addshare;
		double addprice = LOB_AA_nh2[i].addprice;
		double eff_spread = 0.0;
		if (LOB_AA_nh2[i].addbuy == 'S') {
			eff_spread = addprice - midpt;
		} else if (LOB_AA_nh2[i].addbuy == 'B') {
			eff_spread = midpt - addprice;
		}
		sz_wt_eff_spread += (eff_spread * addshare) / (double)sum_addshare;
		td_wt_eff_spread += eff_spread;
		count_EC++;
		}
	}
	td_wt_eff_spread /= (double)count_EC;
	
	// part 4.
	qsort(LOB_AA_nh,trimmed_len,sizeof(struct LOB_stock_date),
		  compare_addseq_LOB_stock_date);

	for (unsigned int i = 0; i < (trimmed_len - 1); i++) {
		double lag_addtime = LOB_AA_nh[i+1].addtime;
		double addtime = LOB_AA_nh[i].addtime;
		double time_dif = lag_addtime - addtime;
		int depth_s = 
			   (LOB_AA_nh[i].depth_s == -1) ? 0 : LOB_AA_nh[i].depth_s;
		int depth_b = 
			   (LOB_AA_nh[i].depth_b == -1) ? 0 : LOB_AA_nh[i].depth_b;
		int depth10_s = 
			   (LOB_AA_nh[i].depth10_s == -1) ? 0 : LOB_AA_nh[i].depth10_s;
		int depth10_b = 
			   (LOB_AA_nh[i].depth10_b == -1) ? 0 : LOB_AA_nh[i].depth10_b;
		
		wt_dep_s += (time_dif * (double)depth_s) / 23400.0;
		wt_dep_b += (time_dif * (double)depth_b) / 23400.0;
		wt_dep10_s += (time_dif * (double)depth10_s) / 23400.0;
		wt_dep10_b += (time_dif * (double)depth10_b) / 23400.0;
	}


    int the_next_nonempty = 0;
	for (unsigned int i = 0; i < (trimmed_len - 1); i++) {
		double ask = LOB_AA_nh[i].ask;
		double bid = LOB_AA_nh[i].bid;

		double lag_addtime = 0.0;
		double addtime = 0.0;
		double time_dif = 0.0;

		double spread = 0.0;

        if (ask >= 0.0 && bid >= 0.0) {
			spread = (ask - bid);
			addtime = LOB_AA_nh[i].addtime;
			for (unsigned int j = i + 1; j < (trimmed_len - 1); j++) {
				if (LOB_AA_nh[j].ask >= 0.0 && LOB_AA_nh[j].bid >= 0.0) {
					the_next_nonempty = j;
					break;
				}
			}
			lag_addtime = LOB_AA_nh[the_next_nonempty].addtime;
			time_dif = lag_addtime - addtime;

			qt_spread += (time_dif * spread) / 23400.0;
		}
	}

    analysis_output << date   << "," 
	                << stock  << ","
					<< setprecision(9) << wt_dep_s   << ","
					<< setprecision(9) << wt_dep_b   << ","
					<< setprecision(9) << wt_dep10_b << ","
					<< setprecision(9) << wt_dep10_s << ","
					<< setprecision(9) << qt_spread  << ","
					<< setprecision(9) << sz_wt_eff_spread << ","
					<< setprecision(9) << td_wt_eff_spread << endl;

    // last part

	double midpt_prev = 0.0, midpt_now = 0.0;
	double new_hour = 0.0;
	double new_minute = 0.0;
	double old_hour = floor(LOB_AA_nh[0].addtime / 3600.0);
	double old_minute = floor((LOB_AA_nh[0].addtime - old_hour * 3600.0) / 60.0);

	for (unsigned int i = 0; i < trimmed_len; i++) {
        double addtime = LOB_AA_nh[i].addtime;
		double new_hour = floor(addtime / 3600.0);
		double new_minute = floor((addtime - new_hour * 3600.0) / 60.0);

		double the_bid = LOB_AA_nh[i].bid;
		double the_ask = LOB_AA_nh[i].ask;

		if ((old_hour == new_hour) && (old_minute == new_minute)) {
			midpt_now = (the_bid + the_ask) / 2.0;
		} else {
			hour.push_back(old_hour);
			minute.push_back(old_minute);

			if (midpt_prev != 0.0) {
				double the_ret = log(midpt_now / midpt_prev);
				ret.push_back(the_ret);
			} else {
				ret.push_back(-999999.0);
			}

			midpt_prev = midpt_now;
			old_hour = new_hour;
			old_minute = new_minute;
		}
	}

	if (midpt_prev != 0.0) {
		double the_ret = log(midpt_now / midpt_prev);
		ret.push_back(the_ret);
	    hour.push_back(old_hour);
	    minute.push_back(old_minute);
	} else {
	    hour.push_back(old_hour);
	    minute.push_back(old_minute);
		ret.push_back(-999999.0);
	}

	for (vector<double>::size_type i = 0; i != ret.size(); i++) {
    	midpt_ret << date << ","
		          << stock << ","
				  << hour[i] << ","
				  << minute[i] << ","
				  << ret[i] << endl;
	}
		
	free(LOB_AA_nh);
	free(lcd);
	
}

	analysis_output.close();					
	midpt_ret.close();
}

int main(int argc, char *argv[])
{
  
	if(argc != 8) {
    	printf("Usage: ./spread_depth_calc [input dir path 1] [input dir path 2] [date] [output dir path] [tickers range start] [tickers range end] [full path to a ticker list]\n");
		exit(1);
  	}


  	/* 
	 * 1. IO disk 
	 *
     */
  
	FILE *fp_ticker_txt = fopen(argv[7],"r");


	int how_many_tickers = 0;
	char dummy_ticker[99];
	while(1){
		if(EOF==fscanf(fp_ticker_txt, "%s\n", dummy_ticker)) {
      		break;
    	}
    	how_many_tickers++;
  	}

	char **Targeted_ticker = (char**) calloc(how_many_tickers,sizeof(char*));
	CALLOC_SAFE_CALL(Targeted_ticker);
	for (int i = 0; i < how_many_tickers; i++) {
		Targeted_ticker[i] = (char*) calloc(9,sizeof(char));
		CALLOC_SAFE_CALL(Targeted_ticker[i]);
	}
  
    fseek(fp_ticker_txt, 0, SEEK_SET);
	int how_many_tickers2 = 0;
	while(!feof(fp_ticker_txt)){
		if(EOF==fscanf(fp_ticker_txt,"%s\n",
		   Targeted_ticker[how_many_tickers2])) {
      		break;
    	}
    	how_many_tickers2++;
  	}
  	
	if(how_many_tickers2!=how_many_tickers) {
    	printf("An error number of tickers read from the ticker list %s': \
		       %d v.s. %d\n", argv[7], how_many_tickers, how_many_tickers2);
		exit(1);
  	}
  	fclose(fp_ticker_txt);

  	int start_tickers = atoi(argv[5]) - 1;
  	int end_tickers = atoi(argv[6]) - 1;
  	if(start_tickers < 0 || end_tickers > (how_many_tickers - 1)) {
    	printf("Invalid tickers range: should be [1,%d]\n",how_many_tickers);
    	exit(1);
    }
  
  
  	create_folder_for_paulie(argv[4],0777);
  
	printf("\nInput directory 1 : %s\n",argv[1]);
	printf("Input directory 2   : %s\n",argv[2]);
	printf("Date to process     : %s\n",argv[3]);
	printf("Output directory    : %s\n",argv[4]);
	printf("Tickers Range       : [%s,%s]\n",argv[5],argv[6]);
	printf("Ticker list path    : %s\n\n\n",argv[7]);
  
  
  
	string the_date = argv[3];
	for (int i = start_tickers; i <= end_tickers; i++) {

		 string the_stock_name = Targeted_ticker[i];

		 printf("----------- %d. %s ----------\n",i,the_stock_name.c_str());
		 string path_to_LOB_stock_date = argv[1];
		 path_to_LOB_stock_date += "/" + the_stock_name + "/" + "LOB_" +
                                   the_stock_name + "_" + the_date + ".csv";
		 string path_to_fleeting_ref = argv[1];
		 path_to_fleeting_ref += "/" + the_stock_name + "/" + "fleeting_ref_" +
                                 the_stock_name + "_" + the_date + ".csv";
         string path_to_LOB_C_date = argv[2];
         path_to_LOB_C_date += "/LOB_C_" + the_date;

         string path_to_analysis_output = argv[4];
         path_to_analysis_output += "/analysis_output_" + the_stock_name + "_"
                                    + the_date + ".csv";
         string path_to_midpt_ret = argv[4];
         path_to_midpt_ret += "/midpt_ret_" + the_stock_name + "_" 
                              + the_date + ".csv";

         spread_depth_analysis(the_stock_name.c_str(),
                               the_date.c_str(),
                               path_to_LOB_stock_date.c_str(),
                               path_to_fleeting_ref.c_str(),
                               path_to_LOB_C_date.c_str(),
                               path_to_analysis_output.c_str(),
                               path_to_midpt_ret.c_str());


		 printf("----------- %d. %s (wof) ----------\n",i,the_stock_name.c_str());
         string path_to_LOB_wof_stock_date = argv[1];
         path_to_LOB_wof_stock_date += "/" + the_stock_name + "/" + "LOB_wof_" +
                                       the_stock_name + "_" + the_date + ".csv";

         string path_to_analysis_output_wof = argv[4];
         path_to_analysis_output_wof += "/analysis_output_wof_" +
                                        the_stock_name + "_" + the_date + ".csv";
         string path_to_midpt_ret_wof = argv[4];
         path_to_midpt_ret_wof += "/midpt_ret_wof_" + the_stock_name + 
                                  "_" + the_date + ".csv";

         spread_depth_analysis_wof(the_stock_name.c_str(),
                                   the_date.c_str(),
                                   path_to_LOB_wof_stock_date.c_str(),
                                   path_to_LOB_C_date.c_str(),
                                   path_to_analysis_output_wof.c_str(),
                                   path_to_midpt_ret_wof.c_str());
	}


    // 
	for (int i = 0; i < how_many_tickers; i++) {
         free(Targeted_ticker[i]);
    }
    free(Targeted_ticker);
  
    return 1;
}
