/*
(C) Copyright 2012 The Board of Trustees of the University of Illinois.
All rights reserved.

Developed by:

                        Department of Finance
                University of Illinois, Urbana Champaign

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal with the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of the Department of Finance, the University of Illinois, 
nor the names of its contributors may be used to endorse or promote products 
derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
THE SOFTWARE.
*/

/*****************************************************************************

    File Name   [lob_input.cpp]

    Synopsis    [NASDAQ ITCH message processing.]

    Description [Output consumed by lob_construction.]
	
	History     [Originally designed by Yao Chen and written in STATA code 
	             format (v0.1); C++ porting to UNIX environment and code 
				 optimization by Jiading Gai; Code improvements by Robert S. 
				 Sinkovits, David O'neal, and D.J. Choi.]

    Revision    [0.1; Yao Chen, Finance Department UIUC]
    Revision    [1.0; Jiading Gai, Finance Department UIUC]
    Revision    [1.1; Jiading Gai, Finance Department UIUC
	                  Robert S. Sinkovits, SDSC
					  David O'neal, PSC
					  D.J. Choi, SDSC]
    
	Date        [08/12/2013]

******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <string>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <float.h>
#include <map>
#include <sstream>
#include <iostream>
#include <omp.h>
#include <string>
#include <set>


#include "LobUtility.h"


std::set<std::string> ticker_uset; 

/*
 * Given a new_ref as the key, do binary search among 
 * the old_ref's.
 *
 * Question: is there a neat way to replace them with
 *           bsearch?
 */

static int 
binarySearch_oldref(struct U_message2 *sortedMsg,
                    int first, int last,
            unsigned long long key)
{
  while (first <= last) {
    int mid = (first + last) / 2;
    if (key > sortedMsg[mid].umsg.old_ref)
      first = mid + 1;
    else if (key < sortedMsg[mid].umsg.old_ref)
      last = mid - 1;
    else
      return mid; 
  }
  return -1;
}

static int
binarySearch_newref(struct U_message2 *sortedMsg,
                    int first, int last,
            unsigned long long key)
{
  while (first <= last) {
    int mid = (first + last) / 2;
    if (key > sortedMsg[mid].umsg.new_ref) 
      first = mid + 1;
    else if (key < sortedMsg[mid].umsg.new_ref) 
      last = mid - 1; 
    else
      return mid;
  }
  return -1;
}

struct quote_life {
  int which_msg;
  double timeDiff;
};

struct quote_life msg_minlife(struct LOB_message *msg, unsigned int len)
{
  struct quote_life min_lifequote;
  min_lifequote.which_msg = 0;
  min_lifequote.timeDiff = DBL_MAX;
  
  for (unsigned int i = 0; i < len; i++) {
    double timeDiff = msg[i].time - msg[i].timeAddAll;
    
    if (min_lifequote.timeDiff > timeDiff) {
      min_lifequote.which_msg = i;
      min_lifequote.timeDiff = timeDiff;
    }
  }
  return min_lifequote;
}

/* Check if the input ticker is in the database, 
 * if so return 1 otherwise return 0.
 */
int
check_ticker_database(char *input_ticker, 
                      char **ticker_database, 
                      int database_len) 
{
  for (int i = 0; i < database_len; i++) {
    if (!strcmp(input_ticker,ticker_database[i])) {
      return 1;//found in ticker database
    }
  }
  return 0;
}


void find_stock_quotelife (struct LOB_message *msg, int len,
                           char **ticker_database, int database_len)
{
  //Get what msg type we talking here:
  char msg_type[3];
  strcpy(msg_type, msg[0].msg_type);

  char prev_stck_nm[9];
  strcpy(prev_stck_nm, msg[0].stock);
  
  struct quote_life stck_minlife;
  stck_minlife.which_msg = 0;
  stck_minlife.timeDiff = DBL_MAX;
  
  int nmsg_per_stck = 0;
  struct quote_life stck_avglife;
  stck_avglife.which_msg = 0;
  stck_avglife.timeDiff = 0.0;
  for (int i = 0; i < len; i++) {
    if (!strcmp(prev_stck_nm, msg[i].stock)) {
      double curr_time_diff = msg[i].time - msg[i].timeAddAll;
      if (curr_time_diff < stck_minlife.timeDiff) {
          stck_minlife.timeDiff = curr_time_diff;
          stck_minlife.which_msg = i;
      }
      
      nmsg_per_stck++;
      stck_avglife.timeDiff += curr_time_diff;
    } else {
      // write results of the previous stock
      stck_avglife.timeDiff /= (double)nmsg_per_stck;
      //RSS if (check_ticker_database(msg[i].stock, ticker_database, database_len)) {
      if(ticker_uset.count(msg[i].stock) > 0) { //RSS
          printf("%s,%s,%.9f,%.9f,%d,%d\n", msg_type, prev_stck_nm, 
                  stck_minlife.timeDiff, stck_avglife.timeDiff, 
                  stck_minlife.which_msg,nmsg_per_stck);
          printf("-------------------------------------------------------\n");
      }
      
      //proceed to the next stock    
      strcpy(prev_stck_nm, msg[i].stock);
      stck_minlife.which_msg = i;
      stck_minlife.timeDiff = msg[i].time - msg[i].timeAddAll;
      
      nmsg_per_stck = 1;
      stck_avglife.which_msg = i;
      stck_avglife.timeDiff = msg[i].time - msg[i].timeAddAll;
    }
  }
}


void dump_stock_tm_tm0 (struct LOB_message *msg, int len,
                        char **ticker_database, int database_len, char *argv[])
{
  //Get what msg type we talking here:
  char msg_type[3];
  strcpy(msg_type, msg[0].msg_type);
  
  char prev_stck_nm[9];
  strcpy(prev_stck_nm, msg[0].stock);
  
  std::string msg_type_folder;
  msg_type_folder = std::string(argv[2]) + "/" + std::string(msg_type);
  create_folder_for_paulie(msg_type_folder.c_str(), 0777);
  
  std::string str_stck_nm (prev_stck_nm);
  std::string outfn;
  outfn = msg_type_folder + "/" + str_stck_nm + ".csv";
  
  FILE *fp_tm_tm0 = fopen(outfn.c_str(),"w");
  for (int i = 0; i < len; i++) {
    if (!strcmp(prev_stck_nm, msg[i].stock)) {
      fprintf(fp_tm_tm0, "%s,%s,%.9f,%.9f\n", msg_type, prev_stck_nm, msg[i].time, msg[i].timeAddAll);
    } else {
      fclose(fp_tm_tm0);
      
      //proceed to the next stock    
      strcpy(prev_stck_nm, msg[i].stock);
      str_stck_nm = prev_stck_nm;
      outfn = msg_type_folder + "/" + str_stck_nm + ".csv";
      std::cout << "Processing " << prev_stck_nm << ":" << outfn  << "\n";
      fp_tm_tm0 = fopen(outfn.c_str(),"w");
      fprintf(fp_tm_tm0, "%s,%s,%.9f,%.9f\n", msg_type, prev_stck_nm, msg[i].time, msg[i].timeAddAll);
    }
  }
  
  if(fp_tm_tm0) {
    fclose(fp_tm_tm0);
  }
}

int main(int argc, char *argv[])
{
  
  time_t start_lob =  time(NULL);
  printf("---------------------------------------------------------------------\n");
  
  if (argc < 5) {
    fprintf(stdout,
        "Usage: lob_input [input folder full path] [output folder full path] [mmddyy] [full path to a ticker list]\n"
        );
    exit(1);
  }
  
  int mkdir_return_value = mkdir(argv[2],0777);
  if (-1 == mkdir_return_value) {
    if (EEXIST == errno) {
      fprintf(stderr,
          "Output directory %s already exists ... overwrite!\n",argv[2]
          );
    }
    else if (EACCES == errno) {
      fprintf(stderr,
          "The parent directory does not allow write permission to the process.\n"
          );
      abort();
    }
    else if (ENAMETOOLONG == errno) {
      fprintf(stderr,
          "Pathname was too long.\n"
          );
      abort();
    }
  }
  
  /* 
   *
   * Step 0. Read U type message and backtrace the message chain via reference number.
   *         Label U message's ref0 using back-tracing and binary search (umsg2.ref0).
   *
   */
  time_t start_step0 = time(NULL);
  printf("Step 0. Read U type message and backtrace the message chain via ref number.\n");
  char full_path_to_seqU_csv[999];
  sprintf(full_path_to_seqU_csv,"%s/seq_U.csv",argv[1]);
  
  unsigned int totalU = 0, totalU2 = 0;
#if 1
  struct U_message2 *umsg2 = read_U2_message(full_path_to_seqU_csv, &totalU);
#else    
  struct U_message2 *umsg2 = read_U2_message_test(full_path_to_seqU_csv, &totalU);
#endif    
  totalU2 = totalU;//FIXME: get rid of totalU2.
  
  struct U_message2 *umsg2_sorted = 
    (struct U_message2*) calloc(totalU, sizeof(struct U_message2));
  CALLOC_SAFE_CALL(umsg2_sorted);
  
#pragma omp for
  for (unsigned int i = 0; i < totalU2; i++) {
    umsg2_sorted[i].ref0 = -1;
    umsg2_sorted[i].old_idx = i;
    umsg2_sorted[i].umsg.time = umsg2[i].umsg.time;
    umsg2_sorted[i].umsg.old_ref = umsg2[i].umsg.old_ref;
    umsg2_sorted[i].umsg.new_ref = umsg2[i].umsg.new_ref;
    umsg2_sorted[i].umsg.share = umsg2[i].umsg.share;
    umsg2_sorted[i].umsg.price = umsg2[i].umsg.price;
    umsg2_sorted[i].umsg.seq_no = umsg2[i].umsg.seq_no;
  }
  
  /* 
   * Sort 'U' w.r.t old_ref
   * note that 'U' comes with new_ref in sorted order.
   *
   * After the following qsort, umsg2_sorted contains 'U' 
   * in old_ref-sorted order.
   *
   */
  qsort(umsg2_sorted,totalU2,sizeof(U_message2),compare_oldref_U);
  
  /*
   * Set 'U' ref0
   */
  for (unsigned int i = 0; i < totalU2; i++)
    {
#if 0
      printf("%.9f,%lld,%lld,%d,%.4f,%d\n", 
         umsg2_sorted[i].umsg.time, 
         umsg2_sorted[i].umsg.old_ref, 
         umsg2_sorted[i].umsg.new_ref,
         umsg2_sorted[i].umsg.share, 
         umsg2_sorted[i].umsg.price, 
         umsg2_sorted[i].umsg.seq_no
         );
#endif
      
      unsigned long long target1 = umsg2[i].umsg.old_ref;
      unsigned long long target2 = umsg2[i].umsg.new_ref;
      
      /* 
       * For each 'U' message, first binary search its old_ref 
       * in the new_ref message column of the same struct array 
       * (REASON: new_ref in sorted order).
       */
      int key_loc = binarySearch_newref(umsg2, 0, totalU2-1, target1);
      
      if (key_loc==-1) {
    /* 
     * couldn't form a chain => this message 
     * is the head of a msg chain.
     */
    umsg2[i].ref0 = target1;
      }
      else {
    umsg2[i].ref0 = umsg2[key_loc].ref0;
    if (-1 == umsg2[i].ref0) {
      printf("Processing U message failed!");
      exit(1);
    }
      }
      
      /* 
       * After successfully setting its own ref0, 
       * looks for all possible messages down my 
       * msg chain and give them my ref0.
       *
       * This is done by doing a binary search in
       * umsg2_sorted (sorted w.r.t old_ref)
       */
      key_loc = binarySearch_oldref(umsg2_sorted,0,totalU2-1,target2);
      
      if (key_loc == -1) {
    /* 
     * no subsequent message that can form a chain 
     * with this one as ancester.
     */
      }
      else {
    umsg2[umsg2_sorted[key_loc].old_idx].ref0 = umsg2[i].ref0;
    if (-1 == umsg2[i].ref0) {
      printf("Processing U message failed!");
      exit(1);
    }
      }
    }
  
#if 0// Debug printf's
  for (int i = 0; i < 20; i++){
    printf("%.9f,%lld,%lld,%d,%.4f,%d,%d\n", 
       umsg2_sorted[i].umsg.time, umsg2_sorted[i].umsg.old_ref, 
       umsg2_sorted[i].umsg.new_ref, umsg2_sorted[i].umsg.share, 
       umsg2_sorted[i].umsg.price, umsg2_sorted[i].old_idx, 
       umsg2_sorted[i].ref0);
  }
  printf("========================================================\n");
  for (int i = 0; i < 20; i++){
    printf("%.9f,%lld,%lld,%d,%.4f,%d,%d\n", 
       umsg2[i].umsg.time, umsg2[i].umsg.old_ref, umsg2[i].umsg.new_ref, 
       umsg2[i].umsg.share, umsg2[i].umsg.price, umsg2[i].old_idx, 
       umsg2[i].ref0);
  }
#endif
  
  time_t end_step0 = time(NULL);
  printf("Step 0 took %ld sec\n",(end_step0-start_step0));
  printf("----------------------------------------------------------------------\n");
  
  
  
  
  
  /////////////////////////////////////////////////////////////////////////
  /////////////////////////// LOB input construction //////////////////////
  /////////////////////////////////////////////////////////////////////////
  /* 
   * Step 1. Read 'A' type message
   */
  time_t start_step1 = time(NULL);
  printf("Step 1. Read A type message!\n");
  
  char full_path_to_seqA_csv[999];
  sprintf(full_path_to_seqA_csv,"%s/seq_A.csv",argv[1]);
  unsigned int totalA = 0, totalA2 = 0;
#if 1
  struct A_message *amsg = read_A_message(full_path_to_seqA_csv, &totalA);
#else    
  struct A_message *amsg = read_A_message_test(full_path_to_seqA_csv, &totalA);
#endif    
  totalA2 = totalA;//FIXME: get rid of totalA2.
  
  time_t end_step1 = time(NULL);
  printf("Step 1 took %ld sec\n",(end_step1-start_step1));
  printf("----------------------------------------------------------------------\n");
  // End of Read 'A' type message 
  
  
  /* 
   * Step 2. Read 'F' type message
   */
  time_t start_step2 = time(NULL);
  printf("Step 2. Read F type message!\n");
  
  char full_path_to_seqF_csv[999];
  sprintf(full_path_to_seqF_csv,"%s/seq_F.csv",argv[1]);
  unsigned int totalF = 0, totalF2 = 0;
  struct F_message *fmsg = read_F_message(full_path_to_seqF_csv, &totalF);
  totalF2 = totalF;
  
  time_t end_step2 = time(NULL);
  printf("Step 2 took %ld sec\n",(end_step2-start_step2));
  printf("---------------------------------------------------------------------\n");
  // End of Step 2. Read 'F' type message
  
  
  /* 
   * Step 3. Fill in 'Au' and 'Du' from 'A' and 'F'
   */
  printf("Step 3. Fill in (Au and Du) from (A and F)\n");
  // sort 'U' w.r.t ref0
  // note that 'U' comes with new_ref in sorted order.
  printf("   1. Sort U message w.r.t ref0\n");
  time_t time_start_sort_U = time(NULL);
  qsort(umsg2,totalU2,sizeof(U_message2),compare_ref0_U);
  // From now on, 'U' stays sorted w.r.t ref0
  time_t time_end_sort_U = time(NULL);
  printf("      time spent: %ld sec\n",(time_end_sort_U-time_start_sort_U));
  
  // sort 'A' w.r.t refno
  // note that 'A' comes with ref_no in non-sorted order.
  printf("   2. Sort A message w.r.t ref_no\n");
  time_t time_start_sort_A = time(NULL);
  qsort(amsg,totalA2,sizeof(struct A_message),compare_refno_A);
  time_t time_end_sort_A = time(NULL);
  printf("      time spent: %ld sec\n",(time_end_sort_A-time_start_sort_A));
  
  // sort 'F' w.r.t refno
  // note that 'F' comes with ref_no in non-sorted order.
  printf("   3. Sort F message w.r.t ref_no\n");
  time_t time_start_sort_F = time(NULL);
  qsort(fmsg,totalF2,sizeof(F_message),compare_refno_F);
  time_t time_end_sort_F = time(NULL);
  printf("      time spent: %ld sec\n",(time_end_sort_F-time_start_sort_F));
  
  
  printf("   4. Populate (Au and Du) from (A and F)\n");
  time_t time_start_AuDu = time(NULL);
  struct LOB_message *Du = 
    (struct LOB_message*)calloc(totalU2,sizeof(struct LOB_message));
  CALLOC_SAFE_CALL(Du);
  
  struct LOB_message *Au = 
    (struct LOB_message*)calloc(totalU2,sizeof(struct LOB_message));
  CALLOC_SAFE_CALL(Au);
  
  /*
   * tracker_A - bookmark the last location in 'A' we have looked at so far
   * tracker_F - bookmark the last location in 'F' we have looked at so far
   */
  int tracker_A = 0;
  int tracker_F = 0;
  int found_in_A = 0;
  for(unsigned int i=0;i<totalU2;i++) {
#if 0
    printf("No.%d out of %lld - [ref0=%lld,seq_no=%d]\n",
       i,totalU2,umsg2[i].ref0,umsg2[i].umsg.seq_no);
#endif
    found_in_A = 0;
    
    Du[i].ref = umsg2[i].umsg.old_ref;
    Du[i].time = umsg2[i].umsg.time;
    Du[i].msg_type[0]='D';
    Du[i].msg_type[1]='u';
    Du[i].msg_type[2]='\0';
    Du[i].seq_no_current_order = umsg2[i].umsg.seq_no;
    
    Au[i].ref = umsg2[i].umsg.new_ref;
    Au[i].time = umsg2[i].umsg.time;
    Au[i].timeAddAll = Au[i].time;//Au's timeAddAll should be its own time.
    Au[i].msg_type[0]='A';
    Au[i].msg_type[1]='u';
    Au[i].msg_type[2]='\0';
    Au[i].seq_no_current_order = umsg2[i].umsg.seq_no;
    Au[i].seq_no_original_order = umsg2[i].umsg.seq_no;
    Au[i].share = umsg2[i].umsg.share;
    Au[i].price = umsg2[i].umsg.price;
    
    for(unsigned int iA = tracker_A;iA<totalA2;iA++) {
      if(umsg2[i].ref0==amsg[iA].ref_no) {
    
    Du[i].seq_no_original_order = amsg[iA].seq_no;
        
    Du[i].timeAddAll = amsg[iA].time;
    
    Du[i].BuySell = amsg[iA].BuySell;
    Au[i].BuySell = amsg[iA].BuySell;
    
    Du[i].share = amsg[iA].share;
    Du[i].price = amsg[iA].price;
    
    strcpy(Du[i].stock,amsg[iA].stock);
    strcpy(Au[i].stock,amsg[iA].stock);
    
    found_in_A = 1;
    tracker_A = iA;
    break;
      }
      else if(umsg2[i].ref0<amsg[iA].ref_no) {
    break;// target ref0 cannot be in 'A' message file.
      }
    }
    
    if(found_in_A==0) {
      // not found in A, keep looking in F.
      for(unsigned int iF=tracker_F;iF<totalF2;iF++) {
    if(umsg2[i].ref0==fmsg[iF].ref_no) {
      Du[i].seq_no_original_order = fmsg[iF].seq_no;
          
      Du[i].timeAddAll = fmsg[iF].time;
      Au[i].timeAddAll = fmsg[iF].time;
      
      Du[i].BuySell = fmsg[iF].BuySell;
      Au[i].BuySell = fmsg[iF].BuySell;
      
      Du[i].share = fmsg[iF].share;
      Du[i].price = fmsg[iF].price;
      
      strcpy(Du[i].stock, fmsg[iF].stock);
      strcpy(Au[i].stock, fmsg[iF].stock);
      
      tracker_F = iF;
      break;
    }
    else if(umsg2[i].ref0<fmsg[iF].ref_no) {
#if 0
      printf("Cannot match %d (U type,ref0:%lld) in A or F types\n",
         i,umsg2[i].ref0);
#endif
      break;
      //exit(1);
    }
      }
    }
  }
  
  
  /* 
   * cross-refine Du with Au according to ref number:
   *
   * sort 'Au' w.r.t ref
   * note that 'Au' comes with ref in unsorted order.
   *
   * tracker_Au - bookmark the last location in 'Au' 
   *              we have looked at so far
   */
  
  printf("   5. Cross-refine Du with Au according to ref number\n");
  printf("      *.Sort Au message w.r.t ref\n");
  qsort(Au,totalU2,sizeof(LOB_message),compare_ref_LOB);
  //FIXME: there's another qsort on Du at line 914, redundant?
  qsort(Du,totalU2,sizeof(LOB_message),compare_ref_LOB);
  
  int tracker_Au = 0;
  for(unsigned int i=0;i<totalU2;i++) {
    for(unsigned int iAu = tracker_Au;iAu<totalU2;iAu++) {
      if(Du[i].ref==Au[iAu].ref) {
    Du[i].timeAddAll = Au[iAu].time;
    Du[i].seq_no_original_order = Au[iAu].seq_no_current_order;
    Du[i].price = Au[iAu].price;
    Du[i].share = Au[iAu].share;
    
    tracker_Au = iAu;
    break;
      }
      else if(Du[i].ref<Au[iAu].ref) {
    break;// target ref0 cannot be in 'Au' message file.
      }
    }
  }
  
  
  
  
#if 0
  printf("   Ref.Time.TimeAddAll.BuySell.Share.Price.Stock.Msg_type.Seq_no_current_order.Seq_no_original_order\n");
  for(unsigned int i=0;i<totalU2;i++) {
    printf("%d:%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d\n",
       i,
       Du[i].ref,
       Du[i].time,
       Du[i].timeAddAll,
       Du[i].BuySell,
       Du[i].share,
       Du[i].price,
       Du[i].stock,
       Du[i].msg_type,
       Du[i].seq_no_current_order,
       Du[i].seq_no_original_order
       );
    
    printf("%d:%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d\n",
       i,
       Au[i].ref,
       Au[i].time,
       Au[i].timeAddAll,
       Au[i].BuySell,
       Au[i].share,
       Au[i].price,
       Au[i].stock,
       Au[i].msg_type,
       Au[i].seq_no_current_order,
       Au[i].seq_no_original_order
       );
  }
#endif
  
  time_t time_end_AuDu = time(NULL);
  printf("       time spent: %ld sec\n",(time_end_AuDu-time_start_AuDu));
  printf("---------------------------------------------------------------------\n");
  
  
  
  
  ////////////////////////////////////////////////////////////////////////
  ///* Step 4. Read 'E' type message and merge 'E' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  printf("Step 4. Read E type message!\n");
  time_t time_start_Step4 = time(NULL);
  
  char full_path_to_seqE_csv[999];
  sprintf(full_path_to_seqE_csv,"%s/seq_E.csv",argv[1]);
  unsigned int totalE = 0, totalE2 = 0;
  struct LOB_message *emsg = read_E_message(full_path_to_seqE_csv, &totalE);
  totalE2 = totalE;
  // End of Read 'E' type message
  
  
  // sort 'E' w.r.t ref
  // note that 'E' comes with ref in non-sorted order.
  printf("  *. Sort E message w.r.t ref\n");
  qsort(emsg,totalE2,sizeof(LOB_message),compare_ref_LOB);
  
  // sort 'Du' w.r.t ref
  // note that 'Du' comes with ref in non-sorted order.
  printf("  *. Sort Du message w.r.t ref\n");
  qsort(Du,totalU2,sizeof(LOB_message),compare_ref_LOB);
  
  tracker_A = 0;//bookmark the location in 'A' we have looked at 
  tracker_F = 0;
  tracker_Au = 0;
  found_in_A = 0;
  int found_in_F = 0;
  for(unsigned int i=0;i<totalE2;i++) {
#if 0
    printf("No.%d out of %lld - [ref0=%lld,seq_no=%d]\n",
       i,totalU2,umsg2[i].ref0,umsg2[i].umsg.seq_no);
#endif
    found_in_A = 0;
    found_in_F = 0;
    
    for(unsigned int iA = tracker_A;iA<totalA2;iA++) {
      if(emsg[i].ref==amsg[iA].ref_no) {
    
    emsg[i].timeAddAll = amsg[iA].time;
    emsg[i].BuySell = amsg[iA].BuySell;
    emsg[i].price = amsg[iA].price;
    emsg[i].seq_no_original_order = amsg[iA].seq_no;
    
    strcpy(emsg[i].stock,amsg[iA].stock);
    
    found_in_A = 1;
    tracker_A = iA;
    break;
      }
      else if(emsg[i].ref<amsg[iA].ref_no) {
    /* 
     * target ref in 'E' cannot be found 
     * in 'A' message file.
     */
    break;
      }
    }
    
    if(found_in_A==0) {
      // not found in A, keep look in F.
      for(unsigned int iF=tracker_F;iF<totalF2;iF++) {
    if(emsg[i].ref==fmsg[iF].ref_no) {
      
      emsg[i].timeAddAll = fmsg[iF].time;
      emsg[i].BuySell = fmsg[iF].BuySell;
      emsg[i].price = fmsg[iF].price;
      emsg[i].seq_no_original_order = fmsg[iF].seq_no;
      
      strcpy(emsg[i].stock,fmsg[iF].stock);
      
      found_in_F = 1;
      tracker_F = iF;
      break;
    }
    else if(emsg[i].ref<fmsg[iF].ref_no) {
      /* 
       * target ref in 'E' cannot be found in 
       * 'F' message file either.
       */
      break;
    }
      }
    }
    
    if(found_in_A==0&&found_in_F==0) {
      /* 
       * not found in A and F, keep looking in Au.
       * number of 'Au' = number of 'U' = totalU2
       */
      for(unsigned int iAu=tracker_Au;iAu<totalU2;iAu++) {
    if(emsg[i].ref==Au[iAu].ref) {
      
      emsg[i].timeAddAll = Au[iAu].time;
      emsg[i].BuySell = Au[iAu].BuySell;
      emsg[i].price = Au[iAu].price;
      emsg[i].seq_no_original_order = Au[iAu].seq_no_original_order;
      
      strcpy(emsg[i].stock,Au[iAu].stock);
      
      
      tracker_Au = iAu;
      break;
    }
    else if(emsg[i].ref<Au[iAu].ref) {
#if 0
      printf("Cannot match %d (E type,ref:%lld) in A or F or Au types\n",
         i,emsg[i].ref);
#endif
      break;
      //exit(1);
    }
      }
    }
  }
  
  char full_path_to_LOB_E[999];
  sprintf(full_path_to_LOB_E,"%s/LOB_E_%s",argv[2],argv[3]);
  FILE *fp_LOB_E_date = fopen(full_path_to_LOB_E,"w");
  fprintf(fp_LOB_E_date,"ref,time,timeAddAll,buy,share,price,stock,msg_type,SEQcurrent,SEQoriginal\n");
  for(unsigned int index=0;index<totalE2;index++) {
    fprintf(fp_LOB_E_date, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
        emsg[index].ref,
        emsg[index].time,
        emsg[index].timeAddAll,
        emsg[index].BuySell,
        emsg[index].share,
        emsg[index].price,
        emsg[index].stock,
        emsg[index].msg_type,
        emsg[index].seq_no_current_order,
        emsg[index].seq_no_original_order
        );
  }
  fclose(fp_LOB_E_date);
  
  time_t time_end_Step4 = time(NULL);
  printf("Step 4 took %ld sec\n",(time_end_Step4-time_start_Step4));
  printf("---------------------------------------------------------------------\n");
  ////////////////////////////////////////////////////////////////////////
  ///* Step 4. Read 'E' type message and merge 'E' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////   END:   //////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  
  
  
  ////////////////////////////////////////////////////////////////////////
  ///* Step 5. Read 'C' type message and merge 'C' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  printf("Step 5. Read C type message!\n");
  time_t time_start_C = time(NULL);
  
  char full_path_to_seqC_csv[999];
  sprintf(full_path_to_seqC_csv,"%s/seq_C.csv",argv[1]);
  unsigned int totalC = 0, totalC2 = 0;
  struct LOB_message *cmsg = read_C_message(full_path_to_seqC_csv, &totalC);
  totalC2 = totalC;
  
  double *cmsg_exec_price = (double*) calloc(totalC, sizeof(double));
  CALLOC_SAFE_CALL(cmsg_exec_price);
  // End of Read 'C' type message 
  
  /* 
   * sort 'C' w.r.t ref
   * note that 'C' comes with ref in non-sorted order.
   * 'Au' w.r.t ref already sorted during the processing of 'E' message.
   */
  printf("  *. Sort C message w.r.t ref\n");
  qsort(cmsg,totalC2,sizeof(LOB_message),compare_ref_LOB);
  
  
  tracker_A = 0;//bookmark the location in 'A' we have looked at 
  tracker_F = 0;
  tracker_Au = 0;
  found_in_A = 0;
  found_in_F = 0;
  for(unsigned int i=0;i<totalC2;i++) {
#if 0
    printf("No.%d out of %lld - [ref0=%lld,seq_no=%d]\n",
       i,totalU2,umsg2[i].ref0,umsg2[i].umsg.seq_no);
#endif
    found_in_A = 0;
    found_in_F = 0;
    cmsg_exec_price[i] = cmsg[i].price;
    
    for(unsigned int iA = tracker_A;iA<totalA2;iA++) {
      if(cmsg[i].ref==amsg[iA].ref_no) {
    
    cmsg[i].timeAddAll = amsg[iA].time;
    cmsg[i].BuySell = amsg[iA].BuySell;
    cmsg[i].price = amsg[iA].price;
    cmsg[i].seq_no_original_order = amsg[iA].seq_no;
    
    strcpy(cmsg[i].stock,amsg[iA].stock);
    
    found_in_A = 1;
    tracker_A = iA;
    break;
      }
      else if(cmsg[i].ref<amsg[iA].ref_no) {
    break;// target ref in 'C' cannot be found in 'A' message file.
      }
    }
    
    if(found_in_A==0) {
      // not found in A, keep look in F.
      for(unsigned int iF=tracker_F;iF<totalF2;iF++) {
    if(fmsg[iF].ref_no==cmsg[i].ref) {
      
      cmsg[i].timeAddAll = fmsg[iF].time;
      cmsg[i].BuySell = fmsg[iF].BuySell;
      cmsg[i].price = fmsg[iF].price;
      cmsg[i].seq_no_original_order = fmsg[iF].seq_no;
      
      strcpy(cmsg[i].stock,fmsg[iF].stock);
      
      found_in_F = 1;
      tracker_F = iF;
      break;
    }
    else if(cmsg[i].ref<fmsg[iF].ref_no) {
      break;// target ref in 'C' cannot be found in 'F' message file either.
    }
      }
    }
    
    if(found_in_A==0&&found_in_F==0) {
      /* 
       * not found in A and F, keep look in Au.
       * number of 'Au' = number of 'U' = totalU2
       */
      for(unsigned int iAu=tracker_Au;iAu<totalU2;iAu++) {
    if(cmsg[i].ref==Au[iAu].ref) {
      
      cmsg[i].timeAddAll = Au[iAu].time;
      cmsg[i].BuySell = Au[iAu].BuySell;
      cmsg[i].price = Au[iAu].price;
      cmsg[i].seq_no_original_order = Au[iAu].seq_no_original_order;
      
      strcpy(cmsg[i].stock,Au[iAu].stock);
      
      
      tracker_Au = iAu;
      break;
    }
    else if(cmsg[i].ref<Au[iAu].ref) {
#if 0
      printf("Cannot match %d (C type,ref:%lld) in A or F or Au types\n",
         i,cmsg[i].ref);
#endif
      break;
      //exit(1);
    }
      }
    }
  }
  char full_path_to_LOB_C[999];
  sprintf(full_path_to_LOB_C,"%s/LOB_C_%s",argv[2],argv[3]);
  FILE *fp_LOB_C_date = fopen(full_path_to_LOB_C,"w");
  fprintf(fp_LOB_C_date,"ref,time,timeAddAll,buy,share,price,exec_price,stock,msg,SEQcurrent,SEQoriginal\n");
  for(unsigned int index=0;index<totalC2;index++) {
    fprintf(fp_LOB_C_date, "%lld,%.9f,%.9f,%c,%d,%.4f,%.4f,%s,%s,%d,%d\n", 
        cmsg[index].ref,
        cmsg[index].time,
        cmsg[index].timeAddAll,
        cmsg[index].BuySell,
        cmsg[index].share,
        cmsg[index].price,
        cmsg_exec_price[index],
        cmsg[index].stock,
        cmsg[index].msg_type,
        cmsg[index].seq_no_current_order,
        cmsg[index].seq_no_original_order
        );
  }
  fclose(fp_LOB_C_date);
  
  time_t time_end_C = time(NULL);
  printf("Step 5 took %ld sec\n",(time_end_C-time_start_C));
  printf("---------------------------------------------------------------------\n");
  ////////////////////////////////////////////////////////////////////////
  ///* Step 5. Read 'C' type message and merge 'C' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////   END:   //////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  
  
  
  
  ////////////////////////////////////////////////////////////////////////
  ///* Step 6. Read 'X' type message and merge 'X' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  printf("Step 6. Read X type message!\n");
  time_t time_start_X = time(NULL);
  char full_path_to_seqX_csv[999];
  sprintf(full_path_to_seqX_csv,"%s/seq_X.csv",argv[1]);
  unsigned int totalX = 0, totalX2 = 0;
  struct LOB_message *xmsg = read_X_message(full_path_to_seqX_csv, &totalX);
  totalX2 = totalX;
  // End of Read 'X' type message
  
  /* 
   * sort 'X' w.r.t ref
   * note that 'C' comes with ref in non-sorted order.
   * 'Au' w.r.t ref already sorted in the processing of 'E' message.
   */
  printf("  *. Sort X message w.r.t ref\n");
  qsort(xmsg,totalX2,sizeof(LOB_message),compare_ref_LOB);
  
  tracker_A = 0;//bookmark the location in 'A' we have looked at 
  tracker_F = 0;
  tracker_Au = 0;
  found_in_A = 0;
  found_in_F = 0;
  for(unsigned int i=0;i<totalX2;i++) {
#if 0
    printf("No.%d out of %lld - [ref0=%lld,seq_no=%d]\n",
       i,totalU2,umsg2[i].ref0,umsg2[i].umsg.seq_no);
#endif
    found_in_A = 0;
    found_in_F = 0;
    
    for(unsigned int iA = tracker_A;iA<totalA2;iA++) {
      if(xmsg[i].ref==amsg[iA].ref_no) {
    
    xmsg[i].timeAddAll = amsg[iA].time;
    xmsg[i].BuySell = amsg[iA].BuySell;
    xmsg[i].price = amsg[iA].price;
    xmsg[i].seq_no_original_order = amsg[iA].seq_no;
    
    strcpy(xmsg[i].stock,amsg[iA].stock);
    
    found_in_A = 1;
    tracker_A = iA;
    break;
      }
      else if(xmsg[i].ref<amsg[iA].ref_no) {
    break;// target ref in 'X' cannot be found in 'A' message file.
      }
    }
    
    if(found_in_A==0) {
      // not found in A, keep look in F.
      for(unsigned int iF=tracker_F;iF<totalF2;iF++) {
    if(xmsg[i].ref==fmsg[iF].ref_no) {
      
      xmsg[i].timeAddAll = fmsg[iF].time;
      xmsg[i].BuySell = fmsg[iF].BuySell;
      xmsg[i].price = fmsg[iF].price;
      xmsg[i].seq_no_original_order = fmsg[iF].seq_no;
      
      strcpy(xmsg[i].stock,fmsg[iF].stock);
      
      found_in_F = 1;
      tracker_F = iF;
      break;
    }
    else if(xmsg[i].ref<fmsg[iF].ref_no) {
      /* 
       * target ref in 'X' cannot be found in 
       * 'F' message file either.
       */
      break;
    }
      }
    }
    
    if(found_in_A==0&&found_in_F==0) {
      /* 
       * not found in A and F, keep look in Au.
       *
       * number of 'Au' = number of 'U' = totalU2.
       */
      for(unsigned int iAu=tracker_Au;iAu<totalU2;iAu++) {
    if(xmsg[i].ref==Au[iAu].ref) {
      
      xmsg[i].timeAddAll = Au[iAu].time;
      xmsg[i].BuySell = Au[iAu].BuySell;
      xmsg[i].price = Au[iAu].price;
      xmsg[i].seq_no_original_order = Au[iAu].seq_no_original_order;
      
      strcpy(xmsg[i].stock,Au[iAu].stock);
      
      
      tracker_Au = iAu;
      break;
    }
    else if(xmsg[i].ref<Au[iAu].ref) {
#if 0
      printf("Cannot match %d (X type,ref:%lld) in A or F or Au types\n",
         i,xmsg[i].ref);
#endif
      break;
      //exit(1);
    }
      }
    }
  }
  
  time_t time_end_X = time(NULL);
  printf("Step 6 took %ld sec\n",(time_end_X-time_start_X));
  printf("----------------------------------------------------------------------\n");
  ////////////////////////////////////////////////////////////////////////
  ///* Step 6. Read 'X' type message and merge 'X' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////   END:   //////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  
  
  ////////////////////////////////////////////////////////////////////////
  ///* Step 7. Read 'D' type message and merge 'D' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  printf("Step 7. Read D type message!\n");
  time_t time_start_D = time(NULL);
  
  char full_path_to_seqD_csv[999];
  sprintf(full_path_to_seqD_csv,"%s/seq_D.csv",argv[1]);
  unsigned int totalD = 0, totalD2 = 0;
  struct LOB_message *dmsg = read_D_message(full_path_to_seqD_csv, &totalD);
  totalD2 = totalD;
  // End of Read 'D' type message
  
  
  /* 
   * sort 'D' w.r.t ref
   * note that 'C' comes with ref in non-sorted order.
   * 'Au' w.r.t ref already sorted in the processing of 'E' message.
   */
  printf("  *. Sort D message w.r.t ref\n");
  qsort(dmsg,totalD2,sizeof(LOB_message),compare_ref_LOB);
  
  
  tracker_A = 0;//bookmark the location in 'A' we have looked at 
  tracker_F = 0;
  tracker_Au = 0;
  found_in_A = 0;
  found_in_F = 0;
  for(unsigned int i=0;i<totalD2;i++) {
#if 0
    printf("No.%d out of %lld - [ref0=%lld,seq_no=%d]\n",
       i,totalU2,umsg2[i].ref0,umsg2[i].umsg.seq_no);
#endif
    found_in_A = 0;
    found_in_F = 0;
    
    for(unsigned int iA = tracker_A;iA<totalA2;iA++) {
      if(dmsg[i].ref==amsg[iA].ref_no) {
    
    dmsg[i].timeAddAll = amsg[iA].time;
    dmsg[i].BuySell = amsg[iA].BuySell;
    dmsg[i].share = amsg[iA].share;
    dmsg[i].price = amsg[iA].price;
    dmsg[i].seq_no_original_order = amsg[iA].seq_no;
    
    strcpy(dmsg[i].stock,amsg[iA].stock);
    
    found_in_A = 1;
    tracker_A = iA;
    break;
      }
      else if(dmsg[i].ref<amsg[iA].ref_no) {
    break;// target ref in 'D' cannot be found in 'A' message file.
      }
    }
    
    if(found_in_A==0) {
      // not found in A, keep look in F.
      for(unsigned int iF=tracker_F;iF<totalF2;iF++) {
    if(dmsg[i].ref==fmsg[iF].ref_no) {
      
      dmsg[i].timeAddAll = fmsg[iF].time;
      dmsg[i].BuySell = fmsg[iF].BuySell;
      dmsg[i].share = fmsg[iF].share;
      dmsg[i].price = fmsg[iF].price;
      dmsg[i].seq_no_original_order = fmsg[iF].seq_no;
      
      strcpy(dmsg[i].stock,fmsg[iF].stock);
      
      found_in_F = 1;
      tracker_F = iF;
      break;
    }
    else if(dmsg[i].ref<fmsg[iF].ref_no) {
      break;// target ref in 'D' cannot be found in 'F' message file either.
    }
      }
    }
    
    if(found_in_A==0&&found_in_F==0) {
      /* 
       * not found in A and F, keep look in Au.
       * 
       * number of 'Au' = number of 'U' = totalU2.
       */
      for(unsigned int iAu=tracker_Au;iAu<totalU2;iAu++) {
    if(dmsg[i].ref==Au[iAu].ref) {
      
      dmsg[i].timeAddAll = Au[iAu].time;
      dmsg[i].BuySell = Au[iAu].BuySell;
      dmsg[i].share = Au[iAu].share;
      dmsg[i].price = Au[iAu].price;
      dmsg[i].seq_no_original_order = Au[iAu].seq_no_original_order;
      
      strcpy(dmsg[i].stock,Au[iAu].stock);
      
      
      tracker_Au = iAu;
      break;
    }
    else if(dmsg[i].ref<Au[iAu].ref) {
#if 0
      printf("Cannot match %d (D type,ref:%lld) in A or F or Au types\n",
         i,dmsg[i].ref);
#endif
      break;
      //exit(1);
    }
      }
    }
  }
  
  time_t time_end_D = time(NULL);
  printf("Step 7 took %ld sec\n",(time_end_D-time_start_D));
  printf("---------------------------------------------------------------------\n");
  ////////////////////////////////////////////////////////////////////////
  ///* Step 7. Read 'D' type message and merge 'D' with 'A, 'F', 'Au' *///
  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////   END:   //////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  
  
  
  
  /////////////////////////////////////////////////////////////////////
  ///* Step 8. For D & Du: share = share - (shares in E,C,X by ref)*///
  /////////////////////////////////////////////////////////////////////
  printf("Step 8. For D & Du: share = share - (shares in E,C,X by ref)\n");
  time_t time_start_Step8 = time(NULL);
  //* First off with 'D' type.
  int tracker_E = 0;//bookmark the location in 'E' we have looked at 
  int tracker_C = 0;
  int tracker_X = 0;
  for(unsigned int i=0;i<totalD2;i++) {
    
    for(unsigned int iE = tracker_E;iE<totalE2;iE++) {
      if(dmsg[i].ref==emsg[iE].ref) {
    dmsg[i].share -= emsg[iE].share;
    tracker_E = iE + 1;
    continue;
      }
      else if(dmsg[i].ref<emsg[iE].ref) {
    break;// target ref in 'D' cannot be found in 'E' message file.
      }
    }
    
    for(unsigned int iC=tracker_C;iC<totalC2;iC++) {
      if(dmsg[i].ref==cmsg[iC].ref) {
    dmsg[i].share -= cmsg[iC].share;
    tracker_C = iC + 1;
    continue;
      }
      else if(dmsg[i].ref<cmsg[iC].ref) {
    break;// target ref in 'D' cannot be found in 'C' message file either.
      }
    }
    
    for(unsigned int iX=tracker_X;iX<totalX2;iX++) {
      if(dmsg[i].ref==xmsg[iX].ref) {
    dmsg[i].share -= xmsg[iX].share;
    tracker_X = iX + 1;
    continue;
      }
      else if(dmsg[i].ref<xmsg[iX].ref) {
    break;
      }
    }
    
    if(dmsg[i].share<0)
      {
    printf("Negative share number %d at the %d message (dmsg)\n",dmsg[i].share,i);
    exit(1);
      }
  }
  
  
  //* Next with 'Du' type.
  tracker_E = 0;//bookmark the location in 'E' we have looked at 
  tracker_C = 0;
  tracker_X = 0;
  for(unsigned int i=0;i<totalU2;i++) {
    
    for(unsigned int iE = tracker_E;iE<totalE2;iE++) {
      if(Du[i].ref==emsg[iE].ref) {
    Du[i].share -= emsg[iE].share;
    tracker_E = iE + 1;
    continue;
      }
      else if(Du[i].ref<emsg[iE].ref) {
    break;// target ref in 'Du' cannot be found in 'E' message file.
      }
    }
    
    for(unsigned int iC=tracker_C;iC<totalC2;iC++) {
      if(Du[i].ref==cmsg[iC].ref) {
    Du[i].share -= cmsg[iC].share;
    tracker_C = iC + 1;
    continue;
      }
      else if(Du[i].ref<cmsg[iC].ref) {
    break;// target ref in 'Du' cannot be found in 'C' message file either.
      }
    }
    
    for(unsigned int iX=tracker_X;iX<totalX2;iX++) {
      if(Du[i].ref==xmsg[iX].ref) {
    Du[i].share -= xmsg[iX].share;
    tracker_X = iX + 1;
    continue;
      }
      else if(Du[i].ref<xmsg[iX].ref) {
    break;
      }
    }
    
    if(Du[i].share<0)
      {
    printf("Negative share number %d at the %d message (Du)\n",Du[i].share,i);
    exit(1);
      }
  }
  
  time_t time_end_Step8 = time(NULL);
  printf("Step 8 took %ld sec\n",(time_end_Step8-time_start_Step8));
  printf("---------------------------------------------------------------------\n");
  /////////////////////////////////////////////////////////////////////
  ///* Step 8. For D & Du: share = share - (shares in E,C,X by ref)*///
  /////////////////////////////////////////////////////////////////////
  ////////////////////////////////// END //////////////////////////////
  /////////////////////////////////////////////////////////////////////
  
  
  
  
  
  
  /////////////////////////////////////////////////////////////
  ///* Step 9. msg_minlife_date: time_diff=time-timeAddAll *///  
  ///*         collapse (min) time_diff, by (msg)          *///
  /////////////////////////////////////////////////////////////
  printf("Step 9. msg_minlife_date: time_diff=time-timeAddAll\n");
  time_t time_start_minlife = time(NULL);
  
  char full_path_to_minlife[999];
  sprintf(full_path_to_minlife,"%s/msg_minlife_%s",argv[2],argv[3]);
  FILE *fp_msg_minlife_date = fopen(full_path_to_minlife,"w");
  
  struct quote_life  min_lifequote_E = msg_minlife(emsg, totalE2);
  fprintf(fp_msg_minlife_date,
      "The minimum quote life in E message is %.9f\n", 
      min_lifequote_E.timeDiff
      );
  print_LOB_message(fp_msg_minlife_date,emsg,min_lifequote_E.which_msg);
  
  struct quote_life min_lifequote_C = msg_minlife(cmsg, totalC2);
  fprintf(fp_msg_minlife_date,
      "The minimum quote life in C message is %.9f\n", 
      min_lifequote_C.timeDiff
      );
  print_LOB_message(fp_msg_minlife_date,cmsg,min_lifequote_C.which_msg);
  
  struct quote_life min_lifequote_X = msg_minlife(xmsg, totalX2);
  fprintf(fp_msg_minlife_date,
      "The minimum quote life in X message is %.9f\n", 
      min_lifequote_X.timeDiff
      );
  print_LOB_message(fp_msg_minlife_date,xmsg,min_lifequote_X.which_msg);
  
  struct quote_life min_lifequote_D = msg_minlife(dmsg, totalD2);
  fprintf(fp_msg_minlife_date,
      "The minimum quote life in D message is %.9f\n", 
      min_lifequote_D.timeDiff
      );
  print_LOB_message(fp_msg_minlife_date,dmsg,min_lifequote_D.which_msg);
  
  struct quote_life min_lifequote_Au = msg_minlife(Au, totalU2);
  fprintf(fp_msg_minlife_date,
      "The minimum quote life in Au message is %.9f\n", 
      min_lifequote_Au.timeDiff
      );
  print_LOB_message(fp_msg_minlife_date,Au,min_lifequote_Au.which_msg);
  
  struct quote_life min_lifequote_Du = msg_minlife(Du, totalU2);
  fprintf(fp_msg_minlife_date,
      "The minimum quote life in Du message is %.9f\n", 
      min_lifequote_Du.timeDiff
      );
  print_LOB_message(fp_msg_minlife_date,Du,min_lifequote_Du.which_msg);
  
  fclose(fp_msg_minlife_date);
  
  
  time_t time_end_minlife = time(NULL);
  printf("Step 9 took %ld sec\n", (time_end_minlife-time_start_minlife));
  printf("---------------------------------------------------------------------\n");
  /////////////////////////////////////////////////////////////
  ///* Step 9. msg_minlife_date: time_diff=time-timeAddAll *///  
  ///*  collapse (min) time_diff, by (msg)          *//////////
  /////////////////////////////////////////////////////////////
  ///////////////////// END ///////////////////////////////////
  /////////////////////////////////////////////////////////////
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  //////////////////////////////////////////////////////////////
  ///*  second_flow_date: count the total number of msg     *///
  ///*  (indepent of msg type) every stock at every second  *///  
  //////////////////////////////////////////////////////////////
  printf("Step 10. second_flow_date: count the total number of msg\n");
  time_t time_start_second_flow = time(NULL);
  ///* Count total how many seconds within the time frame
  std::map<int, int> time_table;
  int how_many_seconds = 0;
  for(int h=9;h<=16;h++)
    for(int m=0;m<=59;m++)
      for(int s=0;s<=59;s++)
    {
      //        if( (h==9&&m<=29) || (h==16&&m>=1) ){
      
      //        }
      //        else {
      time_table[h*3600+m*60+s] = how_many_seconds;
      how_many_seconds++;
      //        }
    }
  printf("  *. %d seconds within the specified range!\n",how_many_seconds);
  
  
  std::map<std::string, int> m[how_many_seconds];
  std::map<std::string, int> mEC[how_many_seconds];
  std::map<std::string, int> mECshare[how_many_seconds];
  
  for(unsigned int i=0;i<totalE2;i++) {
    std::string stock_str(emsg[i].stock);
    int the_sec = (int)(emsg[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      m[time_table[the_sec]][stock_str] += 1;
      mEC[time_table[the_sec]][stock_str] += 1;
      mECshare[time_table[the_sec]][stock_str] += emsg[i].share;
    }
  }
  for(unsigned int i=0;i<totalU2;i++) {
    // At this point, Au is sorted w.r.t ref, but Du is not.
    // Hence, Au and Du need to be treated differently below.
    int the_sec = (int)(Au[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      std::string stock_str1(Au[i].stock);
      m[time_table[the_sec]][stock_str1] += 1;//FIXME: ask xcc
    }
    
    the_sec = (int)(Du[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      std::string stock_str2(Du[i].stock);
      m[time_table[the_sec]][stock_str2] += 1;
    }
  }
  for(unsigned int i=0;i<totalC2;i++) {
    std::string stock_str(cmsg[i].stock);
    int the_sec = (int)(cmsg[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      m[time_table[the_sec]][stock_str] += 1;
      mEC[time_table[the_sec]][stock_str] += 1;
      mECshare[time_table[the_sec]][stock_str] += cmsg[i].share;
    }
  }
  for(unsigned int i=0;i<totalX2;i++) {
    std::string stock_str(xmsg[i].stock);
    int the_sec = (int)(xmsg[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      m[time_table[the_sec]][stock_str] += 1;
    }
  }
  for(unsigned int i=0;i<totalD2;i++) {
    std::string stock_str(dmsg[i].stock);
    int the_sec = (int)(dmsg[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      m[time_table[the_sec]][stock_str] += 1;
    }
  }
  for(unsigned int i=0;i<totalA2;i++) {
    std::string stock_str(amsg[i].stock);
    int the_sec = (int)(amsg[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      m[time_table[the_sec]][stock_str] += 1;
    }
  }
  for(unsigned int i=0;i<totalF2;i++) {
    std::string stock_str(fmsg[i].stock);
    int the_sec = (int)(fmsg[i].time);
    if(time_table.end()!=time_table.find(the_sec)) {
      m[time_table[the_sec]][stock_str] += 1;
    }
  }
  time_t time_end_second_flow = time(NULL);
  printf("Second Flow calculation took %ld sec.\n",(time_end_second_flow-time_start_second_flow));
  
  
  
  printf("  *. Write second flow to disk.\n");
  time_t time_start_write_second_flow = time(NULL);
  
  char full_path_to_second_flow[999];
  sprintf(full_path_to_second_flow,"%s/second_flow_%s",argv[2],argv[3]);
  FILE *fp_secflowdate = fopen(full_path_to_second_flow,"w");
  fprintf(fp_secflowdate,"stock,hour,minute,second,flow,EC,ECshare\n");
  for(std::map<int,int>::iterator j=time_table.begin();j!=time_table.end();++j) {
    for(std::map<std::string, int>::iterator ii=m[(*j).second].begin(); ii!=m[(*j).second].end(); ++ii) {
#if 1
      fprintf(fp_secflowdate,"%s,%d,%d,%d,%d,%d,%d\n",
          (*ii).first.c_str(),
          (int)((*j).first/3600.0f), 
          (int)(((*j).first%3600)/60.0f),
          (int)(((*j).first%60)),
          (*ii).second,
          mEC[(*j).second].find((*ii).first)->second,
          mECshare[(*j).second].find((*ii).first)->second
          );
#else
      fprintf(fp_secflowdate,"%s,%d,%d,%d,%d\n",
          (*ii).first.c_str(),
          (int)((*j).first/3600.0f), 
          (int)(((*j).first%3600)/60.0f),
          (int)(((*j).first%60)),
          (*ii).second
          );
#endif
      
    }
  }
  fclose(fp_secflowdate);
  
  time_t time_end_write_second_flow = time(NULL);
  printf("Output second flow took %ld sec.\n",(time_end_write_second_flow-time_start_write_second_flow));
  printf("---------------------------------------------------------------------\n");
  // */


  //RSS Removed big chunk of white space and commented code - placed in del1.cpp
    
  /////////////////////////////////////////////////////////////////////////////////////////
  ///*  write the big file                                                             *///
  ///*  (filtered by ticker.txt, n (e.g. by default 120) stock symbols): msg_seq_date  *///
  ///*  E,C,X,D,Au,Du,A,F                                                              *///
  ///*  Note that A and F do not have a distinct timeAddAll and seq_no_original order  *///
  ///*  So in A and F, time=timeAddAll and seq_no_original_order = seq_no_current_order*///
  /////////////////////////////////////////////////////////////////////////////////////////
  //////////////  Also write is:                                         //////////////////
  //////////////  msg_count_by_stock: day,stock,E,Au,Du,C,X,D,A,F,total  //////////////////
  /////////////////////////////////////////////////////////////////////////////////////////
  
  time_t time_start_write_big_file_ntickers = time(NULL);
  
  FILE *fp_ticker_txt = fopen(argv[4],"r");
  if (!fp_ticker_txt) {
    printf("error opening your ticker list file\n");
    exit(1);
  }
  
  
  int how_many_tickers = 0;//has to match how_many_tickers
  char dummy_ticker[9999];
  while(1){
    if(EOF==fscanf(fp_ticker_txt, "%s\n", dummy_ticker)) {
      break;
    }
#if 0
    printf("%d: %s\n",how_many_tickers2, Targeted_ticker[how_many_tickers2]);
#endif
    how_many_tickers++;
  }
  
  fseek(fp_ticker_txt, 0, SEEK_SET);
  printf("Step 12. write the big file (filtered by ticker.txt, %d stock symbols): msg_seq_date\n", how_many_tickers);
  char **Targeted_ticker = (char**) calloc(how_many_tickers,sizeof(char*));
  CALLOC_SAFE_CALL(Targeted_ticker);
  for(int i = 0; i < how_many_tickers; i++) {
    Targeted_ticker[i] = (char*) calloc(99,sizeof(char));
    CALLOC_SAFE_CALL(Targeted_ticker[i]);
  }
  
  int how_many_tickers2 = 0;
  //FIXME: better solution needed than testing against EOF at fscanf
  //REF: http://www.cs.bu.edu/teaching/c/file-io/intro/
  while(!feof(fp_ticker_txt)){
    if(EOF==fscanf(fp_ticker_txt,"%s\n",Targeted_ticker[how_many_tickers2])) {
      break;
    }
#if 0
    printf("%d: %s\n",how_many_tickers2, Targeted_ticker[how_many_tickers2]);
#endif
    how_many_tickers2++;
  }
  
  if(how_many_tickers2!=how_many_tickers) {
    printf("An error number of tickers read from 'ticker.txt': %d v.s. %d\n",
       how_many_tickers, how_many_tickers2);
    exit(1);
  }
  
  char full_path_to_seq_ntickers[999];
  sprintf(full_path_to_seq_ntickers,"%s/msg_seq_%s_%dtickers",argv[2],argv[3],how_many_tickers);
  FILE *fp_msg_seq_date_ntickers = fopen(full_path_to_seq_ntickers,"w");
  fprintf(fp_msg_seq_date_ntickers, 
      "ref,time,timeAddAll,buy,share,price,stock,msg,SEQoriginal,SEQcurrent\n");
  
  
  std::map<std::string,int> nE;
  std::map<std::string,int> nAu;
  std::map<std::string,int> nDu;
  std::map<std::string,int> nC;
  std::map<std::string,int> nX;
  std::map<std::string,int> nD;
  std::map<std::string,int> nA;
  std::map<std::string,int> nF;
  std::map<std::string,int> nTotal;
  

  //initialize maps
  for (int i = 0; i < how_many_tickers; i++) {
    std::string the_ticker;
    the_ticker.assign(Targeted_ticker[i]);
    ticker_uset.insert(the_ticker); // RSS Add ticker to unordered set
    nE[the_ticker] = 0;
    nAu[the_ticker] = 0;
    nDu[the_ticker] = 0;
    nC[the_ticker] = 0;
    nX[the_ticker] = 0;
    nD[the_ticker] = 0;
    nA[the_ticker] = 0;
    nF[the_ticker] = 0;
    nTotal[the_ticker] = 0;
  }
  
  
  ///*
  for(unsigned int i=0;i<totalE2;i++) {
#if 0
    print_LOB_message(fp_msg_seq_date,emsg,i);
#else
    //RSS if(check_ticker_database(emsg[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(emsg[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          emsg[i].ref,emsg[i].time,emsg[i].timeAddAll,emsg[i].BuySell,
          emsg[i].share,emsg[i].price,emsg[i].stock,emsg[i].msg_type,
          emsg[i].seq_no_current_order,emsg[i].seq_no_original_order);
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(emsg[i].stock);
      nE[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
#endif
  }
  for(unsigned int i=0;i<totalU2;i++) {
#if 0        
    print_LOB_message(fp_msg_seq_date,Au,i);
#else
    //RSS if(check_ticker_database(Au[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(Au[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          Au[i].ref,Au[i].time,Au[i].timeAddAll,Au[i].BuySell,
          Au[i].share,Au[i].price,Au[i].stock,Au[i].msg_type,
          Au[i].seq_no_current_order,Au[i].seq_no_original_order);
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(Au[i].stock);
      nAu[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
#endif
  }
  for(unsigned int i=0;i<totalU2;i++) {
#if 0
    print_LOB_message(fp_msg_seq_date,Du,i);
#else
    //RSS if(check_ticker_database(Du[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(Du[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          Du[i].ref,Du[i].time,Du[i].timeAddAll,Du[i].BuySell,
          Du[i].share,Du[i].price,Du[i].stock,Du[i].msg_type,
          Du[i].seq_no_current_order,Du[i].seq_no_original_order);
      
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(Du[i].stock);
      nDu[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
#endif
  }
  for(unsigned int i=0;i<totalC2;i++) {
#if 0        
    print_LOB_message(fp_msg_seq_date,cmsg,i);
#else
    //RSS if(check_ticker_database(cmsg[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(cmsg[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          cmsg[i].ref,cmsg[i].time,cmsg[i].timeAddAll,cmsg[i].BuySell,
          cmsg[i].share,cmsg[i].price,cmsg[i].stock,cmsg[i].msg_type,
          cmsg[i].seq_no_current_order,cmsg[i].seq_no_original_order);
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(cmsg[i].stock);
      nC[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
#endif
    
  }
  for(unsigned int i=0;i<totalX2;i++) {
#if 0
    print_LOB_message(fp_msg_seq_date,xmsg,i);
#else
    //RSS if(check_ticker_database(xmsg[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(xmsg[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          xmsg[i].ref,xmsg[i].time,xmsg[i].timeAddAll,xmsg[i].BuySell,
          xmsg[i].share,xmsg[i].price,xmsg[i].stock,xmsg[i].msg_type,
          xmsg[i].seq_no_current_order,xmsg[i].seq_no_original_order);
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(xmsg[i].stock);
      nX[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
#endif
    
  }
  for(unsigned int i=0;i<totalD2;i++) {
#if 0        
    print_LOB_message(fp_msg_seq_date,dmsg,i);
#else
    //RSS if(check_ticker_database(dmsg[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(dmsg[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          dmsg[i].ref,dmsg[i].time,dmsg[i].timeAddAll,dmsg[i].BuySell,
          dmsg[i].share,dmsg[i].price,dmsg[i].stock,dmsg[i].msg_type,
          dmsg[i].seq_no_current_order,dmsg[i].seq_no_original_order);
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(dmsg[i].stock);
      nD[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
#endif
  }
  for(unsigned int i=0;i<totalA2;i++) {
    //RSS if(check_ticker_database(amsg[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(amsg[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          amsg[i].ref_no,  
          amsg[i].time, 
          amsg[i].time,//timeAddAll
          amsg[i].BuySell,
          amsg[i].share,
          amsg[i].price,
          amsg[i].stock,
          "A",//msg_type
          amsg[i].seq_no,//seq_no_current_order
          amsg[i].seq_no //seq_no_original_order
          );
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(amsg[i].stock);
      nA[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
  }
  for(unsigned int i=0;i<totalF2;i++) {
    //RSS if(check_ticker_database(fmsg[i].stock, Targeted_ticker, how_many_tickers2)) {
    if(ticker_uset.count(fmsg[i].stock) > 0) { //RSS
      fprintf(fp_msg_seq_date_ntickers, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
          fmsg[i].ref_no,  
          fmsg[i].time, 
          fmsg[i].time,//timeAddAll
          fmsg[i].BuySell,
          fmsg[i].share,
          fmsg[i].price,
          fmsg[i].stock,
          "F",
          fmsg[i].seq_no,
          fmsg[i].seq_no
          );
      
      //2. save msg_count_by_count
      std::string the_ticker;
      the_ticker.assign(fmsg[i].stock);
      nF[the_ticker] += 1;
      nTotal[the_ticker] += 1;
    }
  }
  fclose(fp_msg_seq_date_ntickers);
  fclose(fp_ticker_txt);
  
  
  // write to disk msg_count_by_stock
  char full_path_to_msg_count[999];
  sprintf(full_path_to_msg_count, "%s/msg_count_by_stock_%s.csv",argv[2],argv[3]);
  FILE *fp_msg_count_by_stock = fopen(full_path_to_msg_count, "w");
  fprintf(fp_msg_count_by_stock, "day,stock,E,Au,Du,C,X,D,A,F,total\n");
  
  int msg_count_map_size = (int)nE.size();
  int msg_count_map_size1 = (int)nAu.size();
  int msg_count_map_size2 = (int)nDu.size();
  int msg_count_map_size3 = (int)nC.size();
  int msg_count_map_size4 = (int)nX.size();
  int msg_count_map_size5 = (int)nD.size();
  int msg_count_map_size6 = (int)nA.size();
  int msg_count_map_size7 = (int)nF.size();
  int msg_count_map_size8 = (int)nTotal.size();
  
  if ((msg_count_map_size != how_many_tickers) ||
      (msg_count_map_size != msg_count_map_size1) ||
      (msg_count_map_size != msg_count_map_size2) ||
      (msg_count_map_size != msg_count_map_size3) ||
      (msg_count_map_size != msg_count_map_size4) ||
      (msg_count_map_size != msg_count_map_size5) ||
      (msg_count_map_size != msg_count_map_size6) ||
      (msg_count_map_size != msg_count_map_size7) ||
      (msg_count_map_size != msg_count_map_size8) ) {
    printf("msg_count_map_size mismatches how_many_tickers:%d vs %d\n", msg_count_map_size, how_many_tickers);
    printf("%d,%d,%d,%d,%d,%d,%d,%d\n",
       msg_count_map_size1,
       msg_count_map_size2,
       msg_count_map_size3,
       msg_count_map_size4,
       msg_count_map_size5,
       msg_count_map_size6,
       msg_count_map_size7,
       msg_count_map_size8);
    exit(1);
  }
  for (int i = 0; i < msg_count_map_size; i++) {
    std::string the_ticker = Targeted_ticker[i];
    fprintf(fp_msg_count_by_stock,"%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
        argv[3],
        the_ticker.c_str(),
        nE[the_ticker],
        nAu[the_ticker],
        nDu[the_ticker],
        nC[the_ticker],
        nX[the_ticker],
        nD[the_ticker],
        nA[the_ticker],
        nF[the_ticker],
        nTotal[the_ticker]
        );
  }
  fclose(fp_msg_count_by_stock);
  
  
  for(int i=0;i<how_many_tickers;i++) {
    free(Targeted_ticker[i]);
  }
  free(Targeted_ticker);
  
  time_t time_end_write_big_file_ntickers = time(NULL);
  printf("Output filtered big files (filtered by %d ticker symbols) and msg_count_by_stock took %ld sec.\n", 
     how_many_tickers,
     (time_end_write_big_file_ntickers-time_start_write_big_file_ntickers));
  printf("---------------------------------------------------------------------\n");
  // */
  
  
  
  
   /////////////////////////////////////////////////////////////
  printf("Extra Task 1. per stock msg_minlife_date: time_diff=time-timeAddAll\n");
  time_t time_start_per_stock_minlife = time(NULL);
 
#if 0
  write_LOB_message("emsg.temp", emsg, totalE2);
  write_LOB_message("cmsg.temp", cmsg, totalC2);
  write_LOB_message("xmsg.temp", xmsg, totalX2);
  write_LOB_message("dmsg.temp", dmsg, totalD2);
  write_LOB_message("Au.temp", Au, totalU2);
  write_LOB_message("Du.temp", Du, totalU2);
#endif  
  
  
  qsort(emsg,totalE2,sizeof(LOB_message),compare_ticker_name_LOB);
  qsort(cmsg,totalC2,sizeof(LOB_message),compare_ticker_name_LOB);
  qsort(xmsg,totalX2,sizeof(LOB_message),compare_ticker_name_LOB);
  qsort(dmsg,totalD2,sizeof(LOB_message),compare_ticker_name_LOB);
  qsort(Au,totalU2,sizeof(LOB_message),compare_ticker_name_LOB);
  qsort(Du,totalU2,sizeof(LOB_message),compare_ticker_name_LOB);
  
 
  find_stock_quotelife (emsg, totalE2, Targeted_ticker, how_many_tickers2);
  find_stock_quotelife (cmsg, totalC2, Targeted_ticker, how_many_tickers2);
  find_stock_quotelife (xmsg, totalX2, Targeted_ticker, how_many_tickers2);
  find_stock_quotelife (dmsg, totalD2, Targeted_ticker, how_many_tickers2);
  find_stock_quotelife (Au, totalU2, Targeted_ticker, how_many_tickers2);
  find_stock_quotelife (Du, totalU2, Targeted_ticker, how_many_tickers2);
  
  
  dump_stock_tm_tm0 (emsg, totalE2, Targeted_ticker, how_many_tickers2, argv);
  dump_stock_tm_tm0 (cmsg, totalC2, Targeted_ticker, how_many_tickers2, argv);
  dump_stock_tm_tm0 (xmsg, totalX2, Targeted_ticker, how_many_tickers2, argv);
  dump_stock_tm_tm0 (dmsg, totalD2, Targeted_ticker, how_many_tickers2, argv);
  dump_stock_tm_tm0 (Au, totalU2, Targeted_ticker, how_many_tickers2, argv);
  dump_stock_tm_tm0 (Du, totalU2, Targeted_ticker, how_many_tickers2, argv);
  
  time_t time_end_per_stock_minlife = time(NULL);
  printf("Extra task 1 took %ld sec\n", 
     (time_end_per_stock_minlife-time_start_per_stock_minlife));
  printf("---------------------------------------------------------------------\n");
  /////////////////////////////////////////////////////////////
  ///////////////////// END ///////////////////////////////////
  /////////////////////////////////////////////////////////////
  
  time_t end_lob = time(NULL);
  printf("Time spent: %ld seconds\n", (end_lob-start_lob));
  
  free(dmsg);
  free(xmsg);
  free(cmsg_exec_price);
  free(cmsg);
  free(emsg);
  free(Au);
  free(Du);
  free(fmsg);
  free(amsg);
  free(umsg2_sorted);
  free(umsg2);
  
  // */
  
  return 1;
}
