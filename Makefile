# (C) Copyright 2012 The Board of Trustees of the University of Illinois.
# All rights reserved.

# Developed by:
#
#                        Department of Finance
#                University of Illinois, Urbana Champaign

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal with the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimers.

# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimers in the documentation
# and/or other materials provided with the distribution.

# Neither the names of the Department of Finance, University of Illinois, 
# nor the names of its contributors may be used to endorse or promote products 
# derived from this Software without specific prior written permission.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
# THE SOFTWARE.


# *****************************************************************************
#
#    File Name   [Makefile]
#
#    Synopsis    [Organize the code compilation.]
#
#    Description []
#
#    Revision    [1.0; Jiading Gai, Finance Department UIUC]
# 
#    Date        [08/12/2013]
#
# ******************************************************************************

INCLUDEFLAGS = LobUtility.h

# Separate compile options per configuration
CXXFLAGS += -fopenmp -O3 ${INCLUDEFLAGS} -w

EXECS=spread_depth_calc parse_msg_v41 parse_msg_v40 lob_construction lob_input

all:${EXECS}
%:%.c LobUtility.o
	g++ -lpthread -o $@ $^ ${CXXFLAGS}
%:%.cpp LobUtility.o
	g++ -lpthread -o $@ $^ ${CXXFLAGS}
LobUtility.o:LobUtility.c
	g++ -c $^ ${CXXFLAGS}
clean:
	rm -f *.o ${EXECS} *.gch .*sw*
