/*
(C) Copyright 2012 The Board of Trustees of the University of Illinois.
All rights reserved.

Developed by:

                        Department of Finance
                University of Illinois, Urbana Champaign

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal with the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of the Department of Finance, the University of Illinois, 
nor the names of its contributors may be used to endorse or promote products 
derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
THE SOFTWARE.
*/

/*****************************************************************************

    File Name   [LobUtility.c]

    Synopsis    [Utility functions for simple operations (sort, search etc).]

    Description []

    Revision    [1.0; Jiading Gai, Finance Department UIUC]
    
	Date        [08/12/2013]

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include "LobUtility.h"

void print_LOB_message(FILE *fp, struct LOB_message *msg, int index){

		fprintf(fp, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%d,%d\n", 
                     msg[index].ref,
                     msg[index].time,
                     msg[index].timeAddAll,
                     msg[index].BuySell,
                     msg[index].share,
                     msg[index].price,
                     msg[index].stock,
                     msg[index].msg_type,
                     msg[index].seq_no_current_order,
                     msg[index].seq_no_original_order
				);
}

int compare_ticker_name_LOB (const void * a, const void * b)
{
  return ( 
		   strcmp((*(struct LOB_message*)a).stock,  
		          (*(struct LOB_message*)b).stock)
		 );
}

int compare_ref_LOB (const void * a, const void * b)
{
  return ( 
		   (*(struct LOB_message*)a).ref - 
		   (*(struct LOB_message*)b).ref 
		 );
}

int compare_ref_LOB_FloatSeq_Fleeting (const void * a, const void * b)
{
  return ( 
		   (*(struct LOB_message_FloatSeq_Fleeting*)a).ref - 
		   (*(struct LOB_message_FloatSeq_Fleeting*)b).ref 
		 );

}

int compare_seqcurrent_LOB (const void * a, const void * b)
{
#if 1
	int ret_val = 0;
	if( (*(struct LOB_message*)a).seq_no_current_order >
		(*(struct LOB_message*)b).seq_no_current_order )
	{
	    ret_val = 1;
	}
	else if( (*(struct LOB_message*)a).seq_no_current_order <
		     (*(struct LOB_message*)b).seq_no_current_order )
	{
	    ret_val = -1;
	}
	else
	{
	    ret_val = 0;
	}
	return ret_val;
#else
	return ( 
		   (*(struct LOB_message*)a).seq_no_current_order - 
		   (*(struct LOB_message*)b).seq_no_current_order 
		 );
#endif
}

int compare_seqcurrent_LOB_FloatSeq_Fleeting (const void * a, const void * b)
{
#if 1
	int ret_val = 0;
	if( (*(struct LOB_message_FloatSeq_Fleeting*)a).seq_no_current_order >
		(*(struct LOB_message_FloatSeq_Fleeting*)b).seq_no_current_order )
	{
	    ret_val = 1;
	}
	else if( (*(struct LOB_message_FloatSeq_Fleeting*)a).seq_no_current_order <
		     (*(struct LOB_message_FloatSeq_Fleeting*)b).seq_no_current_order )
	{
	    ret_val = -1;
	}
	else
	{
	    ret_val = 0;
	}
	return ret_val;
#else
	return ( 
		   (*(struct LOB_message_FloatSeq_Fleeting*)a).seq_no_current_order - 
		   (*(struct LOB_message_FloatSeq_Fleeting*)b).seq_no_current_order 
		 );
#endif
}


void 
write_LOB_message(const char *output_filename, struct LOB_message *msg, int len)
{
   FILE *fp = fopen(output_filename,"w");
   checkOpenFile(fp,output_filename);

   for(int i = 0;i < len;i++)
   {
       print_LOB_message(fp, msg, i);
   }
   fclose(fp);
}


void 
write_LOB_Message_FloatSeq_Fleeting(struct LOB_message_FloatSeq_Fleeting *a,
                                    int len, const char *output_filename)
{
   FILE *fp = fopen(output_filename,"w");
   checkOpenFile(fp,output_filename);

   for(int i=0;i<len;i++)
   {
 	   fprintf(fp, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f,%.9f,%.9f,%d,%d\n", 
                    a[i].ref,
                    a[i].time,
                    a[i].timeAddAll,
                    a[i].BuySell,
                    a[i].share,
                    a[i].price,
                    a[i].stock,
                    a[i].msg_type,
                    a[i].seq_no_current_order,
                    a[i].seq_no_original_order,
                    a[i].settle_time,
                    a[i].min_settle,
                    a[i].flt,
                    a[i].flt_tlshare);
   }
   fclose(fp);
}

int create_folder_for_paulie(const char *pathname, mode_t mode)
{
    int mkdir_return_value = mkdir(pathname,mode);
    if(-1==mkdir_return_value) {
       if(EEXIST==errno){
	      fprintf(stderr,
            "Directory %s already exists ... overwrite!\n",pathname
          );
       }
       else if(EACCES==errno) {
          fprintf(stderr,
            "The parent directory does not allow write permission to the process.\n"
          );
          abort();
       }
       else if(ENAMETOOLONG==errno) {
          fprintf(stderr, 
            "Pathname was too long.\n"
          );
          abort();
       }
    }
    return 0;
}

/* Sorting functions on various message types
 * 
 * Used by qsort as an input argument.
 */
int compare_ref0_U (const void * a, const void * b)
{
  return ( 
		   (*(struct U_message2*)a).ref0 - 
		   (*(struct U_message2*)b).ref0 
		 );
}

int compare_oldref_U (const void * a, const void * b)
{
  return ( 
		   (*(struct U_message2*)a).umsg.old_ref - 
		   (*(struct U_message2*)b).umsg.old_ref 
		 );
}

int compare_refno_A (const void * a, const void * b)
{
  return ( 
		   (*(struct A_message*)a).ref_no - 
		   (*(struct A_message*)b).ref_no 
		 );
}

int compare_refno_F (const void * a, const void * b)
{
  return ( 
		   (*(struct F_message*)a).ref_no - 
		   (*(struct F_message*)b).ref_no 
		 );
}

int compare_addref_LOB_stock_date (const void * a, const void * b)
{
	return (
				(*(struct LOB_stock_date*)a).addref -
				(*(struct LOB_stock_date*)b).addref
		   );
}

int compare_addseq_LOB_stock_date(const void *a, const void *b)
{
	// b.c addseq number in LOB_stock_date is double-type
	// hence, we need explicit comparison, then return the
	// integer type return value indicating the result of
	// the explicit comparison.
	int ret_val = 0;
	double a_addseq = (*(struct LOB_stock_date*)a).addseq;
	double b_addseq = (*(struct LOB_stock_date*)b).addseq;

	if (a_addseq > b_addseq)
		ret_val = 1;
	else if (a_addseq == b_addseq)
		ret_val = 0;
	else
		ret_val = -1;

	return (ret_val);
}

int compare_ref_fleeting_ref (const void * a, const void * b)
{
	return (
				(*(struct fleeting_ref*)a).ref -
				(*(struct fleeting_ref*)b).ref
		   );
}

struct U_message2 *read_U2_message(const char *filename, unsigned int *plen)
{
	char dummy_msg[9999];
	FILE *fp_U = fopen(filename,"r");
	checkOpenFile(fp_U, filename);

    unsigned int totalU = 0;

	while (1) {
		if (EOF == fscanf(fp_U, "%s\n", dummy_msg)) {
			break;
		}
		totalU++;
	}
	printf("\nU has a total of %d messages\n",totalU);

    fseek( fp_U , 0 , SEEK_SET );

    struct U_message2 *umsg2 = 
		(struct U_message2*) calloc(totalU, sizeof(struct U_message2));
    CALLOC_SAFE_CALL(umsg2);


	unsigned int totalU2 = 0;
	while (1) {
		if (EOF == fscanf(fp_U, "%lf,%lld,%lld,%d,%lf,%d\n", 
				                &umsg2[totalU2].umsg.time, 
							    &umsg2[totalU2].umsg.old_ref, 
							    &umsg2[totalU2].umsg.new_ref, 
                                &umsg2[totalU2].umsg.share, 
							    &umsg2[totalU2].umsg.price, 
							    &umsg2[totalU2].umsg.seq_no)) {
			break;
		}
		
		//FIXME:unsigned integer is set to -1.
		umsg2[totalU2].ref0 = -1;//ref0 initialized to -1
        //record the current msg's location in seq_U.csv:
		umsg2[totalU2].old_idx = totalU2;


        #if 0 //debug
		printf("%lf,%lld,%lld,%d,%lf,%d\n", 
				umsg2[totalU2].umsg.time, 
				umsg2[totalU2].umsg.old_ref, 
				umsg2[totalU2].umsg.new_ref, 
				umsg2[totalU2].umsg.share, 
				umsg2[totalU2].umsg.price, 
				umsg2[totalU2].umsg.seq_no);
        #endif

		totalU2++;
	}
	if (totalU2 != totalU) {
    	printf("Inconsistent number of U-type messages! (%d v.s. %d)\n",
			   totalU2,totalU);
		exit(1);
	}
	fclose(fp_U);

	*plen = totalU2;
	return umsg2;
}

/* 
 * getline_paulie: read a line into s from strsrc.
 *
 * return value: the number of characters read or
 *               -1 if end-of-the-file 
 */
int getline_paulie(char *s, const char *strsrc, long len)
{
	static long start = 0;
    char c = '\0';
	int i;

    if (start < len) {
		for (i = start; i < len && (c=strsrc[i]) != '\n'; ++i)
			s[i-start] = c;
		if (c == '\n') {
			s[i-start] = c;
			++i;
		}
		s[i-start] = '\0';
        // number of chars read
		int nchars = i - start;
		start = i;

		return nchars;
	} else {
		// reset the starting point to the beginning upson reaching the 
		// end of the file so that the next call to getline_paulie will 
		// start from the beginning of the string stream
		start = 0;
		return -1;
	}
}

struct U_message2 *read_U2_message_test(const char *filename, unsigned int *plen)
{
	FILE *fp_U = fopen(filename,"r");
	checkOpenFile(fp_U, filename);

	fseek(fp_U, 0, SEEK_END);
	long fsize = ftell(fp_U);
	fseek(fp_U, 0, SEEK_SET);
	char *buffer = (char *) calloc(sizeof(char),fsize);
	CALLOC_SAFE_CALL(buffer);
    checkResults("LobUtility.c", fsize!=fread(buffer, sizeof(char), fsize, fp_U)); 
	fclose(fp_U);

    unsigned int totalU = 0;

	char dummy_msg[9999];
	while (getline_paulie(dummy_msg,buffer,fsize) > 0) {
//		if (EOF == sscanf(buffer, "%s\n", dummy_msg)) {
//			break;
//		}
//		printf("%s\n",dummy_msg);
//		buffer += strlen(dummy_msg) + 1;
		totalU++;
	}
	printf("\nU has a total of %d messages\n",totalU);

    struct U_message2 *umsg2 = 
		(struct U_message2*) calloc(totalU, sizeof(struct U_message2));
    CALLOC_SAFE_CALL(umsg2);

	unsigned int totalU2 = 0;
	while (getline_paulie(dummy_msg,buffer,fsize) > 0) {
		sscanf(dummy_msg, "%lf,%lld,%lld,%d,%lf,%d\n", 
                     &umsg2[totalU2].umsg.time, 
					 &umsg2[totalU2].umsg.old_ref, 
					 &umsg2[totalU2].umsg.new_ref, 
                     &umsg2[totalU2].umsg.share, 
					 &umsg2[totalU2].umsg.price, 
					 &umsg2[totalU2].umsg.seq_no);
		
		//FIXME:unsigned integer is set to -1.
		umsg2[totalU2].ref0 = -1;//ref0 initialized to -1
        //record the current msg's location in seq_U.csv:
		umsg2[totalU2].old_idx = totalU2;

        #if 0 //debug
		printf("%lf,%lld,%lld,%d,%lf,%d\n", 
				umsg2[totalU2].umsg.time, 
				umsg2[totalU2].umsg.old_ref, 
				umsg2[totalU2].umsg.new_ref, 
				umsg2[totalU2].umsg.share, 
				umsg2[totalU2].umsg.price, 
				umsg2[totalU2].umsg.seq_no);
        #endif

		totalU2++;
	}
	if (totalU2 != totalU) {
    	printf("Inconsistent number of U-type messages! (%d v.s. %d)\n",
			   totalU2,totalU);
		exit(1);
	}

	*plen = totalU2;
	return umsg2;
}



struct A_message *read_A_message(const char *filename, unsigned int *plen)
{
	char dummy_msg[9999];
    FILE *fp_A = fopen(filename,"r");
	checkOpenFile(fp_A, filename);
    
	unsigned int totalA = 0;
	while(1) {
		if(EOF==fscanf(fp_A, "%s\n", dummy_msg)) {
			break;
		}
		totalA++;
	}
	
	printf("\nA has a total of %d messages\n",totalA);

    fseek( fp_A , 0 , SEEK_SET );
    struct A_message *amsg = 
           (struct A_message*) calloc(totalA, sizeof(struct A_message));
	CALLOC_SAFE_CALL(amsg);

	unsigned int totalA2 = 0;
	while(1) {
		if(EOF==fscanf(fp_A, "%lf,%lld,%c,%d,\n", 
                              &amsg[totalA2].time, 
							  &amsg[totalA2].ref_no, 
							  &amsg[totalA2].BuySell, 
                              &amsg[totalA2].share)
          ) 
		{
			break;
		}

		char stock_character;
		int not_space = 0;
		for(int j=0;j<9;j++)
		{
			fscanf(fp_A,"%c",&stock_character);
			if(','==stock_character)
				break;
			else {
			    amsg[totalA2].stock[j] = stock_character;
				not_space++;
			}
		}
		amsg[totalA2].stock[not_space] = '\0';
		fscanf(fp_A,"%lf,%d\n",&amsg[totalA2].price, &amsg[totalA2].seq_no);
	
		totalA2++;
        #if 0
        printf("%lf,%lld,%c,%d,%s,%f,%d\n", 
				amsg[totalA2].time, 
				amsg[totalA2].ref_no, 
				amsg[totalA2].BuySell, 
				amsg[totalA2].share, 
				amsg[totalA2].stock, 
				amsg[totalA2].price, 
				amsg[totalA2].seq_no
              );
        #endif
	}
	if(totalA2!=totalA) {
       printf("Inconsistent number of A-type messages! (%d v.s. %d)\n",
			   totalA2,totalA);
	   exit(1);
	}
	fclose(fp_A);

	*plen = totalA2;
	return amsg;
}

struct A_message *read_A_message_test(const char *filename, unsigned int *plen)
{
	FILE *fp_A = fopen(filename,"r");
	checkOpenFile(fp_A,filename);

	fseek(fp_A, 0, SEEK_END);
	long fsize = ftell(fp_A);
	fseek(fp_A, 0, SEEK_SET);
	char *buffer = (char *) calloc(sizeof(char),fsize);
	CALLOC_SAFE_CALL(buffer);
    checkResults("LobUtility.c", fsize!=fread(buffer, sizeof(char), fsize, fp_A));
	fclose(fp_A);

	char dummy_msg[9999];
    
	unsigned int totalA = 0;
	while (getline_paulie(dummy_msg,buffer,fsize) > 0) {
		if((totalA > 41273470))
			printf("totalA=%d\n",totalA);
		totalA++;
	}
	
	printf("\nA has a total of %d messages\n",totalA);

    struct A_message *amsg = 
           (struct A_message*) calloc(totalA, sizeof(struct A_message));
	CALLOC_SAFE_CALL(amsg);

	unsigned int totalA2 = 0;
	while (getline_paulie(dummy_msg,buffer,fsize) > 0) {
		sscanf(dummy_msg,"%lf,%lld,%c,%d,\n", 
    	                  &amsg[totalA2].time, 
		                  &amsg[totalA2].ref_no, 
						  &amsg[totalA2].BuySell, 
                          &amsg[totalA2].share);

		char stock_character;
		int not_space = 0;
		for(int j=0;j<9;j++)
		{
			fscanf(fp_A,"%c",&stock_character);
			if(','==stock_character)
				break;
			else {
			    amsg[totalA2].stock[j] = stock_character;
				not_space++;
			}
		}
		amsg[totalA2].stock[not_space] = '\0';
		fscanf(fp_A,"%lf,%d\n",&amsg[totalA2].price, &amsg[totalA2].seq_no);
	
		totalA2++;
        #if 0
        printf("%lf,%lld,%c,%d,%s,%f,%d\n", 
				amsg[totalA2].time, 
				amsg[totalA2].ref_no, 
				amsg[totalA2].BuySell, 
				amsg[totalA2].share, 
				amsg[totalA2].stock, 
				amsg[totalA2].price, 
				amsg[totalA2].seq_no
              );
        #endif
	}
	if(totalA2!=totalA) {
       printf("Inconsistent number of A-type messages! (%d v.s. %d)\n",
			   totalA2,totalA);
	   exit(1);
	}
	fclose(fp_A);

	*plen = totalA2;
	return amsg;
}

struct F_message *read_F_message(const char *filename, unsigned int *plen)
{
	char dummy_msg[9999];
    FILE *fp_F = fopen(filename,"r");
	checkOpenFile(fp_F,filename);

    unsigned int totalF = 0;
	while(1) {
		if(EOF==fscanf(fp_F, "%s\n", dummy_msg)) {
			break;
		}
		totalF++;
	}
	
	printf("\nF has a total of %d messages\n",totalF);
    fseek( fp_F , 0 , SEEK_SET );
    struct F_message *fmsg = 
           (struct F_message*) calloc(totalF, sizeof(struct F_message));
    CALLOC_SAFE_CALL(fmsg);

	unsigned int totalF2 = 0;
	while(1) {
		if(EOF==fscanf(fp_F, "%lf,%lld,%c,%d,\n", 
                              &fmsg[totalF2].time, 
                              &fmsg[totalF2].ref_no,
							  &fmsg[totalF2].BuySell, 
                              &fmsg[totalF2].share)
          ) 
		{
			break;
		}

		char stock_character;
		int not_space = 0;
		for(int j=0;j<9;j++)
		{
			fscanf(fp_F,"%c",&stock_character);
			if(','==stock_character)
				break;
			else {
			    fmsg[totalF2].stock[j] = stock_character;
				not_space++;
			}
		}
		fmsg[totalF2].stock[not_space] = '\0';
		fscanf(fp_F,"%lf,",&fmsg[totalF2].price);
	
		not_space = 0;
		for(int j=0;j<5;j++)
		{
			fscanf(fp_F,"%c",&stock_character);
			if(','==stock_character)
				break;
			else {
			    fmsg[totalF2].mpid[j] = stock_character;
				not_space++;
			}
		}
		fmsg[totalF2].mpid[not_space] = '\0';
		fscanf(fp_F,"%d\n",&fmsg[totalF2].seq_no);


		totalF2++;
        #if 0
		printf("%lf,%lld,%c,%d,%s,%f,%d\n", 
				fmsg[totalF2].time, 
                fmsg[totalF2].ref_no, 
                fmsg[totalF2].BuySell, 
				fmsg[totalF2].share, 
                fmsg[totalF2].stock, 
                fmsg[totalF2].price, 
                fmsg[totalF2].seq_no);
        #endif
	}
	if(totalF2!=totalF) {
       printf("Inconsistent number of F-type messages! (%d v.s. %d)\n",
               totalF2,totalF);
	   exit(1);
	}
	fclose(fp_F);

	*plen = totalF2;
	return fmsg;
}

struct LOB_message *read_E_message(const char *filename, unsigned int *plen)
{
	unsigned long long dummyMatch;//not needed, but act as dummy for the ease of read in 'E'
	char dummy_msg[9999];

    FILE *fp_E = fopen(filename,"r");
	checkOpenFile(fp_E,filename);

    unsigned int totalE = 0;
	while(1) {
		if(EOF==fscanf(fp_E, "%s\n", dummy_msg)) {
			break;
		}
		totalE++;
	}
	
	printf("\nE has a total of %d messages\n",totalE);
    fseek( fp_E , 0 , SEEK_SET );
    struct LOB_message *emsg = 
           (struct LOB_message*) calloc(totalE, sizeof(struct LOB_message));
    CALLOC_SAFE_CALL(emsg);

	unsigned int totalE2 = 0;
	while(1) {
		if(EOF==fscanf(fp_E, "%lf,%lld,%d,%lld,%d\n", 
                              &emsg[totalE2].time, 
							  &emsg[totalE2].ref, 
							  &emsg[totalE2].share, 
							  &dummyMatch, 
							  &emsg[totalE2].seq_no_current_order)
          ) 
		{
			break;
		}
        emsg[totalE2].msg_type[0] = 'E';
        emsg[totalE2].msg_type[1] = '\0';
		totalE2++;
        #if 0
		printf("%lf,%lld,%d,%lld,%d\n",
				emsg[totalE2].time, 
				emsg[totalE2].ref, 
				emsg[totalE2].share,
				dummyMatch, 
				emsg[totalE2].seq_no_current_order);
        #endif
	}
	if(totalE2!=totalE) {
       printf("Inconsistent number of E-type messages! (%d v.s. %d)\n",
               totalE2,totalE);
	   exit(1);
	}
	fclose(fp_E);

	*plen = totalE2;
	return emsg;
}

struct LOB_message *read_C_message(const char *filename, unsigned int *plen)
{
    /*
	 * dummyCMatch and dummyCPrintable:
	 *   not needed, but act as dummy for the ease of read in 'C'
	 */
	unsigned long long dummyCMatch;
	char dummyCPrintable;
	char dummy_msg[9999];

    FILE *fp_C = fopen(filename,"r");
    checkOpenFile(fp_C, filename);

    unsigned int totalC = 0;
	while(1) {
		if(EOF==fscanf(fp_C, "%s\n",dummy_msg)) {
			break;
		}
		totalC++;
	}
	
	printf("\nC has a total of %d messages\n",totalC);
    fseek( fp_C , 0 , SEEK_SET );
    struct LOB_message *cmsg = 
           (struct LOB_message*) calloc(totalC, sizeof(struct LOB_message));
    CALLOC_SAFE_CALL(cmsg);

	unsigned int totalC2 = 0;
	while(1) {
		if(EOF==fscanf(fp_C, "%lf,%lld,%d,%lld,%c,%lf,%d\n", 
                              &cmsg[totalC2].time, 
							  &cmsg[totalC2].ref, 
							  &cmsg[totalC2].share, 
                              &dummyCMatch, &dummyCPrintable, 
							  &cmsg[totalC2].price, 
							  &cmsg[totalC2].seq_no_current_order)
          ) 
		{
			break;
		}
        cmsg[totalC2].msg_type[0] = 'C';
        cmsg[totalC2].msg_type[1] = '\0';
		totalC2++;
	}
	if(totalC2!=totalC) {
       printf("Inconsistent number of C-type messages! (%d v.s. %d)\n",totalC2,totalC);
	   exit(1);
	}
	fclose(fp_C);

	*plen = totalC2;
	return cmsg;
}

struct LOB_message *read_X_message(const char *filename, unsigned int *plen)
{
	char dummy_msg[9999];
    FILE *fp_X = fopen(filename,"r");
    checkOpenFile(fp_X, filename);

    unsigned int totalX = 0;
	while(1) {
		if(EOF==fscanf(fp_X, "%s\n", dummy_msg)) {
			break;
		}
		totalX++;
	}
	
	printf("\nX has a total of %d messages\n",totalX);
    fseek( fp_X, 0, SEEK_SET );
    struct LOB_message *xmsg = 
           (struct LOB_message*) calloc(totalX, sizeof(struct LOB_message));
    CALLOC_SAFE_CALL(xmsg);

	unsigned int totalX2 = 0;
	while(1) {
		if(EOF==fscanf(fp_X, "%lf,%lld,%d,%d\n", 
                              &xmsg[totalX2].time, 
							  &xmsg[totalX2].ref, 
							  &xmsg[totalX2].share, 
							  &xmsg[totalX2].seq_no_current_order)
		  )
		{
			break;
		}
        xmsg[totalX2].msg_type[0] = 'X';
        xmsg[totalX2].msg_type[1] = '\0';
		totalX2++;
        #if 0
		printf("%lf,%lld,%d,%lld,%c,%f,%d\n",
                xmsg[totalX2].time, 
				xmsg[totalX2].ref, 
				xmsg[totalX2].share, 
				xmsg[totalX2].seq_no_current_order);
        #endif
	}
	if(totalX2!=totalX) {
       printf("Inconsistent number of X-type messages! (%d v.s. %d)\n",
			   totalX2,totalX);
	   exit(1);
	}
	fclose(fp_X);
 
    *plen = totalX2;
	return xmsg; 		
}

struct LOB_message *read_D_message(const char *filename, unsigned int *plen)
{
	char dummy_msg[9999];
	FILE *fp_D = fopen(filename,"r");
    checkOpenFile(fp_D,filename);

    unsigned int totalD = 0;
	while(1) {
		if(EOF==fscanf(fp_D, "%s\n", dummy_msg)) {
			break;
		}
		totalD++;
	}
	
	printf("\nD has a total of %d messages\n",totalD);
    fseek( fp_D, 0, SEEK_SET );
    struct LOB_message *dmsg = 
           (struct LOB_message*) calloc(totalD, sizeof(struct LOB_message));
    CALLOC_SAFE_CALL(dmsg);

	if(NULL==dmsg) {
		printf("out of memory at line %d in file %s!\n",__LINE__,__FILE__);
		exit(1);
	}

	
	unsigned int totalD2 = 0;
	while(1) {
		if(EOF==fscanf(fp_D, "%lf,%lld,%d\n", 
                              &dmsg[totalD2].time, 
							  &dmsg[totalD2].ref, 
							  &dmsg[totalD2].seq_no_current_order)
          ) 
		{
			break;
		}
        dmsg[totalD2].msg_type[0] = 'D';
        dmsg[totalD2].msg_type[1] = '\0';
		totalD2++;
        #if 0
		printf("%lf,%lld,%d\n",
			    dmsg[totalD2].time, 
				dmsg[totalD2].ref, 
				dmsg[totalD2].seq_no_current_order);
        #endif
	}
	if(totalD2!=totalD) {
       printf("Inconsistent number of D-type messages! (%d v.s. %d)\n",
			   totalD2,totalD);
	   exit(1);
	}
	fclose(fp_D);

    *plen = totalD2;
	return dmsg;	
}

void assign_LOB_stock_date(struct LOB_stock_date *dst, struct LOB_stock_date *src)
{
	dst->ask = src->ask;
	dst->depth10_s = src->depth10_s;
	dst->depth_s = src->depth_s;
	dst->addseq = src->addseq;
	dst->bid = src->bid;
	dst->depth10_b = src->depth10_b;
	dst->depth_b = src->depth_b;
	dst->addref = src->addref;
	dst->addtime = src->addtime;
	dst->addprice = src->addprice;
	dst->addbuy = src->addbuy;
	dst->addshare = src->addshare;
	strcpy(dst->addmsg,src->addmsg);
}

struct LOB_stock_date *read_LOB_stock_date_message(const char *filename, 
                                                   unsigned int *plen)
{
	char msg_buffer[9999];
	char *field[20];

	FILE *fp_LOB_stock_date = fopen(filename,"r");
	checkOpenFile(fp_LOB_stock_date,filename);

	int totalMsg = 0;//total number of messages
	while (EOF != fscanf(fp_LOB_stock_date,"%s\n",msg_buffer)) {
		totalMsg++;
	}
	totalMsg -= 1;//minus the file heading
	printf("The file %s has %d messages\n",filename,totalMsg);

	fseek(fp_LOB_stock_date,0,SEEK_SET);
	fscanf(fp_LOB_stock_date,"%s\n",msg_buffer);//skip the heading line

	struct LOB_stock_date *lsd = (struct LOB_stock_date *) 
   		    calloc(totalMsg,sizeof(struct LOB_stock_date));
	CALLOC_SAFE_CALL(lsd);

	// totalMsg2 counts total message number starting from the 2nd line
	int totalMsg2 = 0;
	while (EOF != fscanf(fp_LOB_stock_date,"%s\n", msg_buffer)) {
		char *q, *p;
		int nfield = 0;
       
	    /*
		 * The following code section using strpbrk is modified
		 * from strtok.c (GNU) to handle the case of missing values.
		 * For example, in LOB_COST_050510.csv,
		 * Line 10:       60.1,500,500,587213.00,,,,941849,27000.753083184,0.0100,B,100,F
		 * Line 480393:   ,,,492689688.00,55,200,200,386895596,72000.388958612,55.0000,B,200,D
		 *
		 * Also, the missing value is filled with the number -1.
		 */ 
		p = msg_buffer;
		while ((q = strpbrk(p,",")) != NULL) {
			if (p == q) {
				field[nfield] = "-1";
				nfield++;
			} else {
				*q = '\0';
				field[nfield] = p;
				nfield++;
			}
			p = q + 1;
		};
		field[nfield] = p;

		lsd[totalMsg2].ask = atof(field[0]);
		lsd[totalMsg2].depth10_s = atoi(field[1]);
		lsd[totalMsg2].depth_s = atoi(field[2]);
		lsd[totalMsg2].addseq = atof(field[3]);
		lsd[totalMsg2].bid = atof(field[4]);
		lsd[totalMsg2].depth10_b = atoi(field[5]);
		lsd[totalMsg2].depth_b = atoi(field[6]);
		lsd[totalMsg2].addref = atoll(field[7]);
		lsd[totalMsg2].addtime = atof(field[8]);
		lsd[totalMsg2].addprice = atof(field[9]);
		lsd[totalMsg2].addbuy = field[10][0];
		lsd[totalMsg2].addshare = atoi(field[11]);
		strcpy(lsd[totalMsg2].addmsg,field[12]);

		totalMsg2++;
	}
	
	if (totalMsg2 != totalMsg) {
		printf("Line %d: inconsistent number of messages read in %s! \
		       (%d v.s. %d)\n", __LINE__, __FILE__, totalMsg,totalMsg2);
		exit(1);
	}
	fclose(fp_LOB_stock_date);

	*plen = totalMsg2;
	return lsd;
}

struct LOB_stock_date *keep_between_time_range(struct LOB_stock_date *in, 
											   unsigned int in_len,
											   unsigned int *pout_len,
											   double time_start,
											   double time_finish)
{
	int total_in_range = 0;
	for (int i = 0; i < in_len; i++) {
		if ((in[i].addtime >= time_start) && (in[i].addtime <= time_finish)) {
			total_in_range++;
		}
	}

	struct LOB_stock_date *trimmed = (struct LOB_stock_date *) 
		  calloc(total_in_range,sizeof(struct LOB_stock_date));
    CALLOC_SAFE_CALL(trimmed);

	int total_in_range2 = 0;
	for (int i = 0; i < in_len; i++) {
		if ((in[i].addtime >= time_start) && (in[i].addtime <= time_finish)) {
			assign_LOB_stock_date(&trimmed[total_in_range2],&in[i]);
			total_in_range2++;
		}
	}

	if (total_in_range2 != total_in_range) {
		printf("Line %d: inconsistent number of messages trimmed in %s! \
		       (%d v.s. %d)\n", __LINE__, __FILE__, total_in_range2,total_in_range);
		exit(1);
	}

	*pout_len = total_in_range2;
	return trimmed;
}

struct fleeting_ref *read_fleeting_ref_message(const char *filename, unsigned int *plen)
{
	char msg_buffer[9999];
	char *field[20];

	FILE *fp_fleeting_ref = fopen(filename,"r");
	checkOpenFile(fp_fleeting_ref,filename);

	int totalMsg = 0;//total number of messages
	while (EOF != fscanf(fp_fleeting_ref,"%s\n",msg_buffer)) {
		totalMsg++;
	}
	totalMsg -= 1;//minus the file heading
	printf("The file %s has %d messages\n",filename,totalMsg);

	fseek(fp_fleeting_ref,0,SEEK_SET);
	fscanf(fp_fleeting_ref,"%s\n",msg_buffer);//skip the heading line

	struct fleeting_ref *fr = (struct fleeting_ref *) 
   		    calloc(totalMsg,sizeof(struct fleeting_ref));
	CALLOC_SAFE_CALL(fr);

	// totalMsg2 counts total message number starting from the 2nd line
	int totalMsg2 = 0;
	while (EOF != fscanf(fp_fleeting_ref,"%s\n", msg_buffer)) {
		char *q, *p;
		int nfield = 0;
       
	    /*
		 * The following code section using strpbrk is modified
		 * from strtok.c (GNU) to handle the case of missing values.
		 * For example, in LOB_COST_050510.csv,
		 * Line 10:       60.1,500,500,587213.00,,,,941849,27000.753083184,0.0100,B,100,F
		 * Line 480393:   ,,,492689688.00,55,200,200,386895596,72000.388958612,55.0000,B,200,D
		 *
		 * Also, the missing value is filled with the number -1.
		 */ 
		p = msg_buffer;
		while ((q = strpbrk(p,",")) != NULL) {
			if (p == q) {
				field[nfield] = "-1";
				nfield++;
			} else {
				*q = '\0';
				field[nfield] = p;
				nfield++;
			}
			p = q + 1;
		};
		field[nfield] = p;


		fr[totalMsg2].ref = atoll(field[0]);
		fr[totalMsg2].flt_tlshare = atoi(field[1]);

		totalMsg2++;
	}
	
	if (totalMsg2 != totalMsg) {
		printf("Line %d: inconsistent number of messages read in %s! \
		       (%d v.s. %d)\n", __LINE__, __FILE__, totalMsg,totalMsg2);
		exit(1);
	}
	fclose(fp_fleeting_ref);

	*plen = totalMsg2;
	return fr;
}

struct LOB_C_date *read_LOB_C_date_message(const char *filename, unsigned int *plen)
{
    char msg_buffer[9999];
	char *field[20];

	FILE *fp_LOB_C_date = fopen(filename,"r");
	checkOpenFile(fp_LOB_C_date,filename);

	int totalMsg = 0;//total number of messages
	while (EOF != fscanf(fp_LOB_C_date,"%s\n",msg_buffer)) {
		totalMsg++;
	}
	totalMsg -= 1;//minus the file heading
	printf("The file %s has %d messages\n",filename,totalMsg);

	fseek(fp_LOB_C_date,0,SEEK_SET);
	fscanf(fp_LOB_C_date,"%s\n",msg_buffer);//skip the heading line

	struct LOB_C_date *lcd = (struct LOB_C_date *) 
   		    calloc(totalMsg,sizeof(struct LOB_C_date));
	CALLOC_SAFE_CALL(lcd);

	// totalMsg2 counts total message number starting from the 2nd line
	int totalMsg2 = 0;
	while (EOF != fscanf(fp_LOB_C_date,"%s\n", msg_buffer)) {
		char *q, *p;
		int nfield = 0;
       
	    /*
		 * The following code section using strpbrk is modified
		 * from strtok.c (GNU) to handle the case of missing values.
		 * For example, in LOB_COST_050510.csv,
		 * Line 10:       60.1,500,500,587213.00,,,,941849,27000.753083184,0.0100,B,100,F
		 * Line 480393:   ,,,492689688.00,55,200,200,386895596,72000.388958612,55.0000,B,200,D
		 *
		 * Also, the missing value is filled with the number -1.
		 */ 
		p = msg_buffer;
		while ((q = strpbrk(p,",")) != NULL) {
			if (p == q) {
				field[nfield] = "-1";
				nfield++;
			} else {
				*q = '\0';
				field[nfield] = p;
				nfield++;
			}
			p = q + 1;
		};
		field[nfield] = p;
	
		lcd[totalMsg2].ref = atoll(field[0]);
		lcd[totalMsg2].time = atof(field[1]);
		lcd[totalMsg2].timeAddAll = atof(field[2]);
		lcd[totalMsg2].buy = field[3][0];
		lcd[totalMsg2].share = atoi(field[4]);
		lcd[totalMsg2].price = atof(field[5]);
		lcd[totalMsg2].exec_price = atof(field[6]);
		strcpy(lcd[totalMsg2].stock,field[7]);
		strcpy(lcd[totalMsg2].msg,field[8]);
		lcd[totalMsg2].seqcurrent = atof(field[9]);
		lcd[totalMsg2].seqoriginal = atof(field[10]);

		totalMsg2++;
	}
	
	if (totalMsg2 != totalMsg) {
		printf("Line %d: inconsistent number of messages read in %s! \
		       (%d v.s. %d)\n", __LINE__, __FILE__, totalMsg,totalMsg2);
		exit(1);
	}
	fclose(fp_LOB_C_date);

	*plen = totalMsg2;
	return lcd;
}

struct LOB_message *read_LOB_message(const char *filename, unsigned int *plen)
{
    char msg_buffer[9999];
	char *field[20];

	FILE *fp_LOB = fopen(filename,"r");
	checkOpenFile(fp_LOB,filename);

	int totalMsg = 0;//total number of messages
	while (EOF != fscanf(fp_LOB,"%s\n",msg_buffer)) {
		totalMsg++;
	}
	totalMsg -= 1;//minus the file heading
	printf("The file %s has %d messages\n",filename,totalMsg);

	fseek(fp_LOB,0,SEEK_SET);
	fscanf(fp_LOB,"%s\n",msg_buffer);//skip the heading line

	struct LOB_message *lob = (struct LOB_message *) 
   		    calloc(totalMsg,sizeof(struct LOB_message));
	CALLOC_SAFE_CALL(lob);

	// totalMsg2 counts total message number starting from the 2nd line
	int totalMsg2 = 0;
	while (EOF != fscanf(fp_LOB,"%s\n", msg_buffer)) {
		char *q, *p;
		int nfield = 0;
       
	    /*
		 * The following code section using strpbrk is modified
		 * from strtok.c (GNU) to handle the case of missing values.
		 * For example, in LOB_COST_050510.csv,
		 * Line 10:       60.1,500,500,587213.00,,,,941849,27000.753083184,0.0100,B,100,F
		 * Line 480393:   ,,,492689688.00,55,200,200,386895596,72000.388958612,55.0000,B,200,D
		 *
		 * Also, the missing value is filled with the number -1.
		 */ 
		p = msg_buffer;
		while ((q = strpbrk(p,",")) != NULL) {
			if (p == q) {
				field[nfield] = "-1";
				nfield++;
			} else {
				*q = '\0';
				field[nfield] = p;
				nfield++;
			}
			p = q + 1;
		};
		field[nfield] = p;
	
		lob[totalMsg2].ref = atoll(field[0]);
		lob[totalMsg2].time = atof(field[1]);
		lob[totalMsg2].timeAddAll = atof(field[2]);
		lob[totalMsg2].BuySell = field[3][0];
		lob[totalMsg2].share = atoi(field[4]);
		lob[totalMsg2].price = atof(field[5]);
		strcpy(lob[totalMsg2].stock,field[6]);
		strcpy(lob[totalMsg2].msg_type,field[7]);
		lob[totalMsg2].seq_no_current_order = atoi(field[8]);
		lob[totalMsg2].seq_no_original_order = atoi(field[9]);

		totalMsg2++;
	}
	
	if (totalMsg2 != totalMsg) {
		printf("Line %d: inconsistent number of messages read in %s! \
		       (%d v.s. %d)\n", __LINE__, __FILE__, totalMsg,totalMsg2);
		exit(1);
	}
	fclose(fp_LOB);

	*plen = totalMsg2;
	return lob;
}
