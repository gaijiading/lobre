/*
(C) Copyright 2012 The Board of Trustees of the University of Illinois.
All rights reserved.

Developed by:

                        Department of Finance
                University of Illinois, Urbana Champaign

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal with the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of the Department of Finance, the University of Illinois, 
nor the names of its contributors may be used to endorse or promote products 
derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
THE SOFTWARE.
*/

/*****************************************************************************

    File Name   [lob_construction.cpp]

    Synopsis    [Limit Order Book Construction from NASDAQ ITCH messages.]

    Description [Output consumed by spread_depth_calc.]
	
	History     [Originally designed by Yao Chen and written in STATA code 
	             format (v0.1); C++ porting to UNIX environment and code 
				 optimization by Siraj Nyakhar and Jiading Gai; Code improv-
				 ements by Robert S. Sinkovits, David O'neal, and D.J. Choi.]

    Revision    [0.1; Yao Chen, Finance Department UIUC]
    Revision    [1.0; Siraj Nyakhar, CS UIUC
	                  Jiading Gai, Finance Department UIUC]
    Revision    [1.1; Jiading Gai, Finance Department UIUC
	                  Robert S. Sinkovits, SDSC
					  David O'neal, PSC
					  D.J. Choi, SDSC]
    
	Date        [08/12/2013]

******************************************************************************/

#include <iostream>
#include <fstream> // stream class to read and write from/to files
#include <sstream>
#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <omp.h>

#include "LobUtility.h"

using namespace std;


// struct we will use to pass info to our thread function
typedef struct _threadcomp_t
{
  int start;
  int end;
} threadcomp_t;

struct bobstruct {
  double fset5;
  double fset8;
  double fset9;
  int    iset4;
  short int isB;
  short int isS;
};


// global variables
// variables that will be accessed outside of main method 
// (primarily will be needed in the thread function)
string ** settled;
struct bobstruct *rss;

string ** fleeting;
int numRows;
char * writeFile;
int the_count;


/*
  This function takes in threadComp which will hold the values of when to 
  begin and end analyzing the data set (ie. which line to start at and which one to finish at). 
  It will then let a thread find in the MSFT_settled.csv data, lines that fit the boundaries. 
  Finally it writes the ones that do fit the boundaries into LOB_MSFT.csv (final output file).
*/
void* tFunc1 (int fRows) {
  
  int i;
  ostringstream accum[fRows]; //RSS

  //Jiading GAI: assume both fleeting.csv and MSFT_settled.csv are sorted w.r.t time
  //             then we can move int y = 0 from Line 59 to Line 43


#pragma omp parallel for private(i) schedule(dynamic,10)
  for (i = 1; i<fRows; ++i)  // RSSXX start at zero or one
    {
      //cout << i << endl;
      the_count++;
      // will report # of iterations completed each thousand lines analyzed
      if (the_count % 1000 == 0) 
	{
	  cout<<the_count<<" iterations completed"<<endl;
	}
      double macro_seqcurrent = atof(fleeting[i][6].c_str());
      double bestBid =  0.0;
      double bestAsk = 9999.0;//DBL_MAX;
      int totalSharesB = 0;
      int totalSharesB_10 = 0;
      int totalSharesS = 0;
      int totalSharesS_10 = 0;
      double seqoriginal, seqcurrent;
      
      /*
       * first time, calculate bestbid, bestask
       * second time, calculate 0.1 dollar around bestbid, bestask
       */
      if( !strcmp(fleeting[i][5].c_str(),"A") ||
	  !strcmp(fleeting[i][5].c_str(),"F") ||
	  !strcmp(fleeting[i][5].c_str(),"Au")  ) 
	{
	  for(int y = numRows-1; y >= 0; y--) 
	    {
	      //FIXME: be careful with seqcurrent and seqoriginal
	      seqcurrent = rss[y].fset8;
	      seqoriginal = rss[y].fset9;
	      if (seqcurrent < macro_seqcurrent) break;
	      ///*
	      if ( (seqcurrent  >  macro_seqcurrent) && 
		   (seqoriginal < macro_seqcurrent) )
		{
		  // find the best bid for 'B'
		  if (rss[y].isB == 0) 
		    {
		      if (rss[y].fset5 == bestBid) 
			{
			  totalSharesB += rss[y].iset4;
			}
		      else if (rss[y].fset5 > bestBid) 
			{
			  bestBid = rss[y].fset5;
			  totalSharesB = 0; //reset back to 0, new bestBid
			  totalSharesB = rss[y].iset4;
			}
		    }
		  // find the best ask for 'S'
		  if (rss[y].isS == 0) 
		    {
		      if (rss[y].fset5 == bestAsk) 
			{
			  totalSharesS += rss[y].iset4;
			}
		      else if (rss[y].fset5 < bestAsk) 
			{
			  bestAsk = rss[y].fset5;
			  totalSharesS = 0; //reset back to 0, new bestAsk
			  totalSharesS = rss[y].iset4;
			}
		    }
		}
	    }
	  for(int y = numRows-1; y >= 0; y--) 
	    {
	      seqcurrent = rss[y].fset8;
	      seqoriginal = rss[y].fset9;
	      if (seqcurrent < macro_seqcurrent) break;
	      ///*
	      if ( (seqcurrent  >  macro_seqcurrent) && 
		   (seqoriginal < macro_seqcurrent) )
		{
		  // find the best bid for 'B'
		  if (rss[y].isB == 0) 
		    {
		      if (rss[y].fset5 >= (bestBid-0.1)) 
			{
			  totalSharesB_10 += rss[y].iset4;
			}
		    }
		  // find the best ask for 'S'
		  if (rss[y].isS == 0) 
		    {
		      if (rss[y].fset5 <= (bestAsk+0.1))
			{
			  totalSharesS_10 += rss[y].iset4;
			}
		    }
		}
	    }
	}
      else
	{
	  for(int y = numRows-1; y >= 0; y--) 
	    {
	      //FIXME: be careful with seqcurrent and seqoriginal
	      seqcurrent = rss[y].fset8;
	      seqoriginal = rss[y].fset9;
	      if (seqcurrent < macro_seqcurrent) break;
	      ///*
	      if ( (seqcurrent  >=  macro_seqcurrent) && 
		   (seqoriginal < macro_seqcurrent) )
		{
		  // find the best bid for 'B'
		  if (rss[y].isB == 0) 
		    {
		      if (rss[y].fset5 == bestBid) 
			{
			  totalSharesB += rss[y].iset4;
			}
		      else if (rss[y].fset5 > bestBid) 
			{
			  bestBid = rss[y].fset5;
			  totalSharesB = 0; //reset back to 0, new bestBid
			  totalSharesB = rss[y].iset4;
			}
		    }
		  // find the best ask for 'S'
		  if (rss[y].isS == 0) 
		    {
		      if (rss[y].fset5 == bestAsk) 
			{
			  totalSharesS += rss[y].iset4;
			}
		      else if (rss[y].fset5 < bestAsk) 
			{
			  bestAsk = rss[y].fset5;
			  totalSharesS = 0; //reset back to 0, new bestAsk
			  totalSharesS = rss[y].iset4;
			}
		    }
		}
	    }
	  for(int y = numRows-1; y >= 0; y--) 
	    {
	      seqcurrent = rss[y].fset8;
	      seqoriginal = rss[y].fset9;
	      if (seqcurrent < macro_seqcurrent) break;
	      ///*
	      if ( (seqcurrent  >=  macro_seqcurrent) && 
		   (seqoriginal < macro_seqcurrent) )
		{
		  // find the best bid for 'B'
		  if (rss[y].isB == 0) 
		    {
		      if (rss[y].fset5 >= (bestBid-0.1)) 
			{
			  totalSharesB_10 += rss[y].iset4;
			}
		    }
		  // find the best ask for 'S'
		  if (rss[y].isS == 0) 
		    {
		      if (rss[y].fset5 <= (bestAsk+0.1))
			{
			  totalSharesS_10 += rss[y].iset4;
			}
		    }
		}
	    }		
	}
      
      if(bestAsk!=9999.0 || bestBid!=0.0)
	{
	  ostringstream out; //stream we will use to convert values to string
	  
	  //Siraj: loads in fleeting.csv as the current working file
	  //Jgai: loads in msg.csv as the current working file
	  //msg.csv has 7 columns:
	  //
	  //  0 . 1  . 2 .  3  .  4  . 5 .    6
	  // ref.time.buy.share.price.msg.seqcurrent (macro_seqcurrent under the context)
	  // Manually write to final array
	  string finalSet [1][13];
	  
	  if (bestAsk == 9999.0)
	    {
	      //bestAsk
	      finalSet[0][0] = "";
	      
	      // total number of shares for < best ask+0.1
	      finalSet[0][1] = "";
	      
	      // total number of shares for best ask
	      finalSet[0][2] = "";
	    }
	  else
	    {
	      out << bestAsk;
	      finalSet[0][0] = out.str();
	      out.str("");
	      
	      // total number of shares for < best ask+0.1
	      out<<totalSharesS_10;
	      finalSet[0][1] = out.str();
	      out.str("");
	      
	      // total number of shares for best ask
	      out<<totalSharesS;
	      finalSet[0][2] = out.str();
	      out.str("");
	    }
	  
	  //add seq
	  finalSet[0][3] = fleeting[i][6];
	  
	  
	  if (bestBid==0.0)
	    {
	      //bestBid
	      finalSet[0][4] = "";
	      
	      // total number of shares for > best bid-0.1
	      finalSet[0][5] = "";
	      
	      // total number of shares for best bid
	      finalSet[0][6] = "";
	    }
	  else
	    {
	      out<<bestBid;
	      finalSet[0][4] = out.str();
	      out.str("");
	      
	      // total number of shares for > best bid-0.1
	      out<<totalSharesB_10;
	      finalSet[0][5] = out.str();
	      out.str("");
	      
	      // total number of shares for best bid
	      out<<totalSharesB;
	      finalSet[0][6] = out.str();
	      out.str("");
	    }
	  
	  //add ref
	  finalSet[0][7] = fleeting[i][0];
	  //add time
	  finalSet[0][8] = fleeting[i][1];
	  //add price
	  finalSet[0][9] = fleeting[i][4];
	  //add buy
	  finalSet[0][10] = fleeting[i][2];
	  //add share
	  finalSet[0][11] = fleeting[i][3];
	  //add msg
	  finalSet[0][12] = fleeting[i][5];
	  
	  // RSS accumulating a big string to print at end of loop
	  for (int x = 0; x < 12; x++)
	    accum[i]<<finalSet[0][x]<<",";
	  accum[i]<<finalSet[0][12]<<"\n";
	  // RSS
	  
	  
	}
    } // end of for loop
  
  /*
    CHANGE: 
    "LOB_MSFT.csv" can be replaced with the of the filename 
    you want to write to write the final data to
  */
  // the ios::app tells the program begin where last written to
  ///*
  /* critical section */
  
  ofstream lob_msft;
  lob_msft.open(writeFile,ios::app);
  for(int i=1; i<fRows; i++) {
    lob_msft<<accum[i].str();
  }
  lob_msft.close();
    
  return NULL;
}

int lob_main (int lob_argc, char *lob_argv [])
{
  time_t start_main =  time(NULL);
  
  bool valid = true;
  // find other paramters to possibly add for extreme cases
  // if valid is false, we will skip right to the end and exit program
  if (lob_argc != 5) 
    {
      cout<<"Please recheck the input"<<endl;
      cout<<"Program expected four inputs"<<endl;
      valid = false;
    }
  if (valid == true) 
    {
      // will display the three inputted files
      
      /*
       * ./lob_main ./msg_AA_050310.csv ./settled_AA_050310.csv ./LOB_AA_050310.csv 16
       *
       */
      cout << "./lob_main "
	   << lob_argv[1]<< " "
	   << lob_argv[2]<< " "
	   << lob_argv[3]<< " "
	   << lob_argv[4]<< endl;
      //the_count = 1;
      //cout<<"current iteration is "<<the_count<<endl;
      
      
      
      
      
      
      
      
      
      writeFile = lob_argv[3];
      
      
      
      
      string line;
      //Siraj: loads in fleeting.csv as the current working file
      //Jgai: loads in msg.csv as the current working file
      //msg.csv has 7 columns:
      //
      //  0 . 1  . 2 .  3  .  4  . 5 .    6
      // ref.time.buy.share.price.msg.seqcurrent
      ifstream myfile (lob_argv[1]); // 
      getline (myfile,line); //skip this line, contains the headings
      getline (myfile,line);
      string item[7]; // array for holdin each value in line
      stringstream linestream(line);
      int i = 0;
      while (getline(linestream,item[i],',')) 
	{
	  i++;
	}
      double macro_seqcurrent = atof(item[6].c_str());
      myfile.close();
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      /* load in the file (AA_settled.csv) that will be used for comparisons
       * Now AA_settle.csv has 10 columns:
       *         struct LOB_message{
       *              unsigned long long ref;
       *              double time;
       *              double timeAddAll;
       *              char BuySell;
       *              unsigned int share;
       *              float price;
       *              char stock[9];
       *              char msg_type[3];
       *              int seq_no_current_order;
       *              int seq_no_original_order;
       *          };
       */
      ifstream settledfile (lob_argv[2]);
      //cout<<"Here2"<<endl;
      int numColumns = 10;
      numRows = 0; //we will calculate this here
      getline (settledfile,line); //skip this line, contains the headings	
      while (getline(settledfile,line)) 
	{
	  numRows++;
	}
      //cout<<"Here3"<<endl;
      settledfile.close();
      settledfile.open(lob_argv[2]);
      getline (settledfile,line); // skip this line, contains the headings
      //cout<<"before getline?"<<endl;
      //getline (settledfile,line);
      cout<<"settled numRows "<<numRows<<endl;
      settled = new string * [numRows];
      for (int i = 0; i < numRows; i++) 
	{
	  settled[i] = new string[numColumns];
	}
      //copy the CSV into our 2D string array declared above
      int current_row = 0;
      int current_column = 0;
      
      while (getline(settledfile,line)) 
	{
	  stringstream settledstream(line);
	  current_column = 0;
	  while (getline(settledstream,settled[current_row][current_column],',')) 
	    {
	      current_column++;
	    }
	  //settled[current_row][0] = line;	
	  current_row++;	
	}
      settledfile.close();
      //print the array to see if it works so far
      //cout<<"exits loop"<<endl;
      
      // RSS
      rss = (struct bobstruct *) malloc(numRows * sizeof(bobstruct));
      
      for (int y=0; y < numRows; y++) {
	rss[y].fset5 = atof(settled[y][5].c_str());
	rss[y].fset8 = atof(settled[y][8].c_str());
	rss[y].fset9 = atof(settled[y][9].c_str());
	rss[y].iset4 = atoi(settled[y][4].c_str());
	rss[y].isB   = settled[y][3].compare("B");
	rss[y].isS   = settled[y][3].compare("S");
      }
      // RSS
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      //new trimmed array with only price, buy, and share	
      string ** trimSet = new string * [numRows];
      
      for (int i = 0; i <numRows; i++) 
	{
	  trimSet[i] = new string [3];
	}
      
      //remove entries where time > macro_time or timeAddAll < macro_time
      current_row = 0;
      double seqcurrent, seqoriginal;
      for (int y = 0; y<numRows; y++) {
	seqcurrent = rss[y].fset9;
	seqoriginal = rss[y].fset8;
	
	if( !strcmp(item[5].c_str(),"A") &&
	    !strcmp(item[5].c_str(),"F") &&	
	    !strcmp(item[5].c_str(),"Au")  )
	  {
	    if ( seqcurrent > macro_seqcurrent && 
		 seqoriginal < macro_seqcurrent  
		 ) 
	      {
		trimSet[current_row][0] = settled[y][5];
		trimSet[current_row][1] = settled[y][3];
		trimSet[current_row][2] = settled[y][4];
		current_row++;
	      }
	  }
	else
	  {
	    if ( seqcurrent >= macro_seqcurrent &&
		 seqoriginal < macro_seqcurrent   ) 
	      {
		trimSet[current_row][0] = settled[y][5];
		trimSet[current_row][1] = settled[y][3];
		trimSet[current_row][2] = settled[y][4];
		current_row++;
	      }
	    
	  }
      }	
      //Jiading GAI: what for: 
      // test to see if trimSet is working
      // first iteration done?
      long addref = atol(item[0].c_str());
      double addprice = atof(item[4].c_str());
      char addbuy = (item[2])[0];
      long addshare = atol(item[3].c_str());
      
      /*
       * write first iteration to LOB_MSFT
       */
      ofstream lob_msft;
      lob_msft.open (writeFile);
      //headings written to final output
      lob_msft<<"ask,depth10_s,depth_s,addseq,bid,depth10_b,depth_b,addref,addtime,addprice,addbuy,addshare,addmsg"<<endl;
      for (int y= 0; y<current_row; y++) 
	{
	  for (int x = 0; x<3; x++) 
	    { //3-1=2
	      lob_msft<<trimSet[y][x]<<",";
	    }
	  lob_msft<<item[0]<<",";
	  lob_msft<<item[4]<<",";
	  lob_msft<<item[2]<<",";
	  lob_msft<<item[3]<<"\n";
	}
      lob_msft.close();	
      // free trimSet array
      for (int i = 0; i < numRows; i++) 
	{
	  delete [] trimSet [i];
	}
      delete [] trimSet;
      //begin for loop iteration for the rest of the entries
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      //Siraj: We will first move 'fleeting.csv' to an array for easier manipulation
      //JGAI: We will first move 'msg.csv' to an array for easier manipulation
      // Future version of program: do this in beginning for congeniality
      myfile.open(lob_argv[1]);
      getline(myfile,line); // skip this line, contains the heads
      int fColumns = 7; // # of columns in 'fleeting.csv'
      int fRows = 0; // we will calculate the # of rows in 'fleeting.csv' below
      
      while (getline(myfile,line)) 
	{
	  //will run till end of file
	  fRows++;
	}
      
      cout << "Fleeting rows:" << fRows <<endl;
      myfile.close(); // cloe so we can open again and start back at top
      myfile.open(lob_argv[1]);
      getline(myfile,line); // skip this line, contains the headings
      
      //array we will use to store 'fleeting.csv' in
      fleeting = new string * [fRows];
      
      for (int i = 0; i < fRows; i++)
	{
	  fleeting[i] = new string[fColumns];	
	}
      
      // variables used to keep track of where we are in array transfer process
      // set back to 0 to be on safe side
      current_row = 0;
      current_column = 0;
      
      // code that does the actual moving of 'fleeting.csv' into array
      while (getline(myfile,line)) 
	{
	  stringstream fleetstream(line);
	  current_column = 0;
	  // continue to get column till last comma delimiter is found
	  while(getline(fleetstream,fleeting[current_row][current_column],',')) 
	    {
	      current_column++;
	    }
	  current_row++;
	}
      
      myfile.close(); // 'fleeting.csv' no in array, no need for file itself
      
      
      tFunc1(fRows); // RSSXX Simple call replaces pthreads code

      cout<<"End of Program"<<endl; // indidcates program has ran to completetion
      
      // free settled array
      for (int i =0; i<numRows; i++) 
	{
	  delete [] settled [i];
	}
      delete [] settled;
      
      // free fleeting array
      for (int i =0; i<fRows; i++) 
	{
	  delete [] fleeting [i];
	}
      delete [] fleeting;
    }
  time_t end_main =  time(NULL);
  printf("time spent: %ld seconds\n",(end_main-start_main));
  
  return 1;
}

int main(int argc, char *argv[])
{
  
  time_t rss_start, rss_end;
  
  if(argc != 8) {
    printf("Usage: ./lob_construction [input dir path] [date] [output path] [number of cores] [ntickers range start] [ntickers range end] [full path to a_ticker_list]\n");
    exit(1);
  }


  /* 
   * 1. Load in the stock symbols from the ticker list, given by argv[7]
   *
   */
  
  FILE *fp_ticker_txt = fopen(argv[7],"r");
 
  int how_many_tickers = 0;//has to match how_many_tickers
  char dummy_ticker[99];
  while(1){
    if(EOF==fscanf(fp_ticker_txt,"%s\n",dummy_ticker)) {
      break;
    }
    how_many_tickers++;
  }
  
  printf("1. Load in the %d stock symbols from ticker.txt\n", how_many_tickers);
  
  char **Targeted_ticker = (char**) calloc(how_many_tickers,sizeof(char*));
  CALLOC_SAFE_CALL(Targeted_ticker);
  for (int i = 0; i < how_many_tickers; i++) {
    Targeted_ticker[i] = (char*) calloc(9,sizeof(char));
    CALLOC_SAFE_CALL(Targeted_ticker[i]);
  }
  
  fseek(fp_ticker_txt,0,SEEK_SET);
  int how_many_tickers2 = 0;//has to match how_many_tickers
  while(!feof(fp_ticker_txt)){
    if(EOF==fscanf(fp_ticker_txt,"%s\n",Targeted_ticker[how_many_tickers2])) {
      break;
    }
    how_many_tickers2++;
  }
  if(how_many_tickers2!=how_many_tickers) {
    printf("An error number of tickers read from %s: %d v.s. %d\n",
	  argv[7], how_many_tickers, how_many_tickers2);
    exit(1);
  }
  fclose(fp_ticker_txt);
  


  /*
   * 1-based index to 0-based index
   */
  int start_ntickers = atoi(argv[5]) - 1;
  int end_ntickers = atoi(argv[6]) - 1;
  if (start_ntickers < 0 || end_ntickers > (how_many_tickers - 1)) {
      printf("Invalid ntickers range: should be [1,%d]\n",how_many_tickers);
      exit(1);
  }
  
  
  /*
   * Create output directories if not exist yet
   *
   * Example: output dir path (argv[3]) might be:
   *     
   *     /oasis/scratch/ddsun/temp_project/lob_deadline/lob_050610
   */
  create_folder_for_paulie(argv[3],0777);
  
  
  
  printf("\nInput directory : %s\n",argv[1]);
  printf("Date to process   : %s\n",argv[2]);
  printf("Output directory  : %s\n",argv[3]);
  printf("Cores             : %s\n",argv[4]);
  printf("Tickers Range     : [%s,%s]\n\n\n",argv[5],argv[6]);
  printf("Ticker list path  : %s\n\n\n",argv[7]);
  
  

  
  
  /*
   * 2. Load in msg_seq_ntickers.txt
   *
   */
  printf("2. Load in msg_seq_%dtickers.txt\n",how_many_tickers);
  char full_path_to_seq_ntickers[999];
  sprintf(full_path_to_seq_ntickers,"%s/msg_seq_%s_%dtickers",argv[1],argv[2],how_many_tickers);
  FILE *fp_msg_seq_date_ntickers = fopen(full_path_to_seq_ntickers,"r");
  if (!fp_msg_seq_date_ntickers) {
	  printf("Error opening %s\n",full_path_to_seq_ntickers);
	  exit(1);
  }
  
  /*
   * ntickers.txt has heading: 
   * ref,time,timeAddAll,buy,share,price,stock,msg,SEQoriginal,SEQcurrent
   *
   */
  int total_ntickers = 0;//total number of messages in ntickers file.
  char msg_buffer[9999];
  char *field[20];
  
  while(1) {
    if(EOF==fscanf(fp_msg_seq_date_ntickers,"%s\n",msg_buffer)) {
      break;
    }
    total_ntickers++;
  }
  total_ntickers -= 1; // minus the file heading
  printf("   This ntickers has a total of %d messages\n",total_ntickers);
  
  
  fseek(fp_msg_seq_date_ntickers, 0, SEEK_SET);
  fscanf(fp_msg_seq_date_ntickers, "%s\n", msg_buffer);//skip the heading line
  struct LOB_message_FloatSeq_Fleeting *stocksn = 
    (struct LOB_message_FloatSeq_Fleeting*)
    calloc(total_ntickers,sizeof(struct LOB_message_FloatSeq_Fleeting));
  CALLOC_SAFE_CALL(stocksn);
  
  //total_ntickers2 counts total line number from the second line.
  int total_ntickers2 = 0;
  while(1) {
    if(EOF==fscanf(fp_msg_seq_date_ntickers, "%s\n", msg_buffer)) {
      break;
    }
    
    char *q, *p;
    int nfield = 0;
    for(q=msg_buffer;(p=strtok(q,","))!=NULL;q=NULL) {
      field[nfield] = p;
      nfield++;
    }
    
    stocksn[total_ntickers2].ref = atoll(field[0]);
    stocksn[total_ntickers2].time = atof(field[1]);
    stocksn[total_ntickers2].timeAddAll = atof(field[2]);
    stocksn[total_ntickers2].BuySell = field[3][0];
    stocksn[total_ntickers2].share = atoi(field[4]);
    stocksn[total_ntickers2].price = atof(field[5]);
    strcpy(stocksn[total_ntickers2].stock, field[6]);
    strcpy(stocksn[total_ntickers2].msg_type, field[7]);
    //FIXME:current and original seq no-s should be switched
    stocksn[total_ntickers2].seq_no_current_order =  atof(field[8]);
    stocksn[total_ntickers2].seq_no_original_order = atof(field[9]);
    // Initialization the rest:
    stocksn[total_ntickers2].settle_time = DBL_MAX;
    stocksn[total_ntickers2].min_settle = DBL_MAX;
    stocksn[total_ntickers2].flt = -1;
    stocksn[total_ntickers2].flt_tlshare = -1;
    
    total_ntickers2++;
  }
  
  if(total_ntickers!=total_ntickers2) {
    printf("Line %d: inconsistent number of messages read in ntickers.csv! (%d v.s. %d)\n",
	   __LINE__,total_ntickers,total_ntickers2);
    exit(1);
  }
  fclose(fp_msg_seq_date_ntickers);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  /* Loop through n tickers */
  for(int i=start_ntickers;i<=end_ntickers;i++)
    {
      char the_stock_name[9];
      strcpy(the_stock_name,Targeted_ticker[i]);
      
      /* make dir for this ticker */
      string ticker_foldername_output(argv[3]);
      ticker_foldername_output += "/";
      ticker_foldername_output.append(the_stock_name);
      create_folder_for_paulie(ticker_foldername_output.c_str(),0777);
      
      
      printf("3. For stock %s, generate many per-stock files for LOB construction (based off AA_seq_LOB.do)\n",the_stock_name);
      /*
       * find, in stock_ntickers, the total msg number 
       * of the stock name in the current iteration.
       */
      int totalMsg_per_stock = 0;
      for(int j=0;j<total_ntickers;j++)
	{
	  if(!strcmp(the_stock_name,stocksn[j].stock))
	    {
	      totalMsg_per_stock += 1;
	    }
	}
      
      struct LOB_message_FloatSeq_Fleeting *MSFT = 
	(struct LOB_message_FloatSeq_Fleeting*)
	calloc(totalMsg_per_stock,sizeof(struct LOB_message_FloatSeq_Fleeting));
      CALLOC_SAFE_CALL(MSFT);
      
      int totalMsg_per_stock2 = 0;
      int totalBigFile_except_A_F_Au = 0;
      
      for(int j=0;j<total_ntickers;j++)
	{
	  
	  if(!strcmp(the_stock_name,stocksn[j].stock))
	    {
	      MSFT[totalMsg_per_stock2].ref = stocksn[j].ref;
	      MSFT[totalMsg_per_stock2].time = stocksn[j].time;
	      MSFT[totalMsg_per_stock2].timeAddAll = stocksn[j].timeAddAll;
	      MSFT[totalMsg_per_stock2].BuySell = stocksn[j].BuySell;
	      MSFT[totalMsg_per_stock2].share = stocksn[j].share;
	      MSFT[totalMsg_per_stock2].price = stocksn[j].price;
	      strcpy(MSFT[totalMsg_per_stock2].stock, stocksn[j].stock);
	      strcpy(MSFT[totalMsg_per_stock2].msg_type, stocksn[j].msg_type);
	      //FIXME:current and original seq no-s should be switched
	      MSFT[totalMsg_per_stock2].seq_no_current_order = stocksn[j].seq_no_current_order;
	      MSFT[totalMsg_per_stock2].seq_no_original_order = stocksn[j].seq_no_original_order;
	      // Initialization the rest:
	      MSFT[totalMsg_per_stock2].settle_time = DBL_MAX;
	      MSFT[totalMsg_per_stock2].min_settle = DBL_MAX;
	      MSFT[totalMsg_per_stock2].flt = -1;
	      MSFT[totalMsg_per_stock2].flt_tlshare = -1;
	      
	      
	      if(!strcmp(MSFT[totalMsg_per_stock2].msg_type,"Au")) {
		MSFT[totalMsg_per_stock2].seq_no_current_order += 0.1f;
	      }
	      
	      if ( (!strcmp(MSFT[totalMsg_per_stock2].msg_type,"Au")) || 
		   (!strcmp(MSFT[totalMsg_per_stock2].msg_type,"A"))  || 
		   (!strcmp(MSFT[totalMsg_per_stock2].msg_type,"F"))   ) {
		/* drop A,Au,F */
	      }
	      else {
		//
		totalBigFile_except_A_F_Au++;
	      }
	      
	      totalMsg_per_stock2++;
	    }
	}
      
      if(totalMsg_per_stock2!=totalMsg_per_stock) {
	printf("Line %d: inconsistent number of messages read in stock ID %s)! (%d v.s. %d)\n",
	       __LINE__,the_stock_name,totalMsg_per_stock,totalMsg_per_stock);
	exit(1);
      }
      
      
      qsort(MSFT,totalMsg_per_stock, sizeof(LOB_message_FloatSeq_Fleeting),
	    compare_seqcurrent_LOB_FloatSeq_Fleeting);
      
      struct LOB_message_FloatSeq_Fleeting *MSFT_settled = (struct LOB_message_FloatSeq_Fleeting *)
	calloc(totalBigFile_except_A_F_Au,sizeof(LOB_message_FloatSeq_Fleeting));
      CALLOC_SAFE_CALL(MSFT_settled);
      
      int totalBigFile_except_A_F_Au2 = 0;
      for(int i=0;i<totalMsg_per_stock2;i++)
	{
	  if ( (!strcmp(MSFT[i].msg_type,"Au")) || 
	       (!strcmp(MSFT[i].msg_type,"A"))  || 
	       (!strcmp(MSFT[i].msg_type,"F"))   ) {
	    /* drop Au, A, F */
	  }
	  else {
	    MSFT_settled[totalBigFile_except_A_F_Au2].ref = MSFT[i].ref;
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].time = MSFT[i].time;
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].timeAddAll = MSFT[i].timeAddAll;
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].BuySell = MSFT[i].BuySell;
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].share = MSFT[i].share;
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].price = MSFT[i].price;
	    
	    strcpy(MSFT_settled[totalBigFile_except_A_F_Au2].stock, MSFT[i].stock);
	    
	    strcpy(MSFT_settled[totalBigFile_except_A_F_Au2].msg_type, MSFT[i].msg_type);
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].seq_no_current_order = 
	      MSFT[i].seq_no_current_order;
	    
	    MSFT_settled[totalBigFile_except_A_F_Au2].seq_no_original_order = 
	      MSFT[i].seq_no_original_order;
	    
	    // Initialization the rest:
            MSFT_settled[totalBigFile_except_A_F_Au2].settle_time = DBL_MAX;
            MSFT_settled[totalBigFile_except_A_F_Au2].min_settle = DBL_MAX;
            MSFT_settled[totalBigFile_except_A_F_Au2].flt = -1;
            MSFT_settled[totalBigFile_except_A_F_Au2].flt_tlshare = -1;
	    
	    totalBigFile_except_A_F_Au2++;
	  }
	}
      if(totalBigFile_except_A_F_Au!=totalBigFile_except_A_F_Au2) {
        printf("Line %d: inconsistent number of messages from MSFT to MSFT_settled! (%d v.s. %d)\n",
	       __LINE__, totalBigFile_except_A_F_Au, totalBigFile_except_A_F_Au2);
	exit(1);
      }
      
      //write MSFT_settled.csv and msg.csv
      char full_path_to_MSFT_settled[9999];
      sprintf(full_path_to_MSFT_settled,"%s/settled_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_MSFT_settled = fopen(full_path_to_MSFT_settled,"w");
      fprintf(fp_MSFT_settled,"ref,time,timeaddall,buy,share,price,stock,msg,seqcurrent,seqoriginal\n");
      for(int i=0;i<totalBigFile_except_A_F_Au2;i++)
	{
#if 0
	  fprintf(fp_MSFT_settled, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f,%.9f,%.9f,%d,%d\n", 
		  MSFT_settled[i].ref,
		  MSFT_settled[i].time,
		  MSFT_settled[i].timeAddAll,
		  MSFT_settled[i].BuySell,
		  MSFT_settled[i].share,
		  MSFT_settled[i].price,
		  MSFT_settled[i].stock,
		  MSFT_settled[i].msg_type,
		  MSFT_settled[i].seq_no_current_order,
		  MSFT_settled[i].seq_no_original_order,
		  //The following are not in the stata code, but fine:
		  MSFT_settled[i].settle_time,
		  MSFT_settled[i].min_settle,
		  MSFT_settled[i].flt,
		  MSFT_settled[i].flt_tlshare
		  );
#else
	  fprintf(fp_MSFT_settled, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f\n", 
		  MSFT_settled[i].ref,
		  MSFT_settled[i].time,
		  MSFT_settled[i].timeAddAll,
		  MSFT_settled[i].BuySell,
		  MSFT_settled[i].share,
		  MSFT_settled[i].price,
		  MSFT_settled[i].stock,
		  MSFT_settled[i].msg_type,
		  MSFT_settled[i].seq_no_current_order,
		  MSFT_settled[i].seq_no_original_order
		  );
#endif
	}
      fclose(fp_MSFT_settled);
      
      char full_path_to_MSFT_msg[9999];
      sprintf(full_path_to_MSFT_msg,"%s/msg_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_MSFT_msg = fopen(full_path_to_MSFT_msg,"w");
      fprintf(fp_MSFT_msg,"ref,time,buy,share,price,msg,seqcurrent\n");
      for(int i=0;i<totalMsg_per_stock2;i++)
	{
	  //2. MSFT_msg.csv
	  fprintf(fp_MSFT_msg,"%lld,%.9f,%c,%d,%.4f,%s,%.2f\n",
		  MSFT[i].ref,
		  MSFT[i].time,
		  MSFT[i].BuySell,
		  MSFT[i].share,
		  MSFT[i].price,
		  MSFT[i].msg_type,
		  MSFT[i].seq_no_current_order
		  );
	}
      fclose(fp_MSFT_msg);
      
      
      
      /*
       * MSFT_settled ----------------> fleeting_share and fleeting_ref
       *
       * Step 1. Get rid of E,C messages from MSFT_settled.
       *
       * Step 2. Set fleeting_share to the MSFT_settled free of E,C message.
       *
       *
       */
      qsort(MSFT_settled,totalBigFile_except_A_F_Au2,
	    sizeof(LOB_message_FloatSeq_Fleeting),compare_ref_LOB_FloatSeq_Fleeting);
      
      int totalSettled_except_EC = 0;
      for(int i=0;i<totalBigFile_except_A_F_Au2;i++) {
	if( !strcmp(MSFT_settled[i].msg_type,"E") ||
	    !strcmp(MSFT_settled[i].msg_type,"C")  )
	  {
	    
	  }
	else
	  {
	    totalSettled_except_EC += 1;
	  }
      }
      
      
      struct LOB_message_FloatSeq_Fleeting *fleeting_share = 
	(struct LOB_message_FloatSeq_Fleeting *) 
	calloc (totalSettled_except_EC, sizeof(LOB_message_FloatSeq_Fleeting));
      CALLOC_SAFE_CALL(fleeting_share);
      int totalSettled_except_EC2 = 0;
      for(int i=0;i<totalBigFile_except_A_F_Au2;i++) {
	if( !strcmp(MSFT_settled[i].msg_type,"E") ||
	    !strcmp(MSFT_settled[i].msg_type,"C")  )
	  {
	    
	  }
	else
	  {
	    
	    fleeting_share[totalSettled_except_EC2].ref = MSFT_settled[i].ref;
	    fleeting_share[totalSettled_except_EC2].time = MSFT_settled[i].time;
	    fleeting_share[totalSettled_except_EC2].timeAddAll = MSFT_settled[i].timeAddAll;
	    fleeting_share[totalSettled_except_EC2].BuySell = MSFT_settled[i].BuySell;
	    fleeting_share[totalSettled_except_EC2].share = MSFT_settled[i].share;
	    fleeting_share[totalSettled_except_EC2].price = MSFT_settled[i].price;
	    strcpy(fleeting_share[totalSettled_except_EC2].stock,MSFT_settled[i].stock);
	    strcpy(fleeting_share[totalSettled_except_EC2].msg_type,MSFT_settled[i].msg_type);
	    fleeting_share[totalSettled_except_EC2].seq_no_current_order = MSFT_settled[i].seq_no_current_order;
	    fleeting_share[totalSettled_except_EC2].seq_no_original_order = MSFT_settled[i].seq_no_original_order;
	    
	    //
	    fleeting_share[totalSettled_except_EC2].settle_time = 
	      (fleeting_share[totalSettled_except_EC2].time - fleeting_share[totalSettled_except_EC2].timeAddAll);
	    fleeting_share[totalSettled_except_EC2].min_settle = DBL_MAX;
	    fleeting_share[totalSettled_except_EC2].flt = -1;//default to not a fleeting order
	    fleeting_share[totalSettled_except_EC2].flt_tlshare = -1;
	    
	    totalSettled_except_EC2 += 1;
	  }
      }
      
      if(totalSettled_except_EC!=totalSettled_except_EC2)
	{
	  printf("Error: inconsistent number from settled to fleeting_share.\n");	   
	  exit(1);
	}
      // finding fleeting orders requires sorted in ref.
      qsort(fleeting_share,totalSettled_except_EC2,
	    sizeof(LOB_message_FloatSeq_Fleeting),compare_ref_LOB_FloatSeq_Fleeting);
      
      // Mark fleeting.flt in fleeting_share (excluding "E","C" and within 0.05 seconds)
      int start_a_ref = 0;
      int end_a_ref = 0;
      int total_share_by_ref = fleeting_share[0].share;
      double minimum_settle = fleeting_share[0].settle_time;
      for(int i=1;i<totalSettled_except_EC2;i++) {
	
	if(fleeting_share[i].ref==fleeting_share[start_a_ref].ref)
	  {
	    if(minimum_settle>fleeting_share[i].settle_time)
	      minimum_settle = fleeting_share[i].settle_time;
	    
	    total_share_by_ref += fleeting_share[i].share;
	    end_a_ref = i;
	  }
	else
	  {
	    for(int j=start_a_ref;j<=end_a_ref;j++)
	      {
		fleeting_share[j].min_settle = minimum_settle;
		fleeting_share[j].flt_tlshare = total_share_by_ref;
		
		if(minimum_settle<=0.05)
		  {
		    fleeting_share[j].flt = 1;
		  }
	      }
	    
	    start_a_ref = i;
	    end_a_ref = i;
	    total_share_by_ref = fleeting_share[i].share;
	    minimum_settle = fleeting_share[i].settle_time;
	  }
      }
      
      //write fleeting_share.csv  and fleeting_ref.csv
      char full_path_to_fleeting_share[9999];
      sprintf(full_path_to_fleeting_share,"%s/fleeting_share_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_fleeting_share = fopen(full_path_to_fleeting_share,"w");
      fprintf(fp_fleeting_share,"ref,seqcurrent,flt_tlshare\n");
      for(int i=0;i<totalSettled_except_EC2;i++)
	{
	  /*E,C are not fleeting orders*/
	  if(fleeting_share[i].flt==1)
	    {
	      fprintf(fp_fleeting_share, "%lld,%.2f,%d\n",
		      fleeting_share[i].ref,
		      fleeting_share[i].seq_no_current_order,
		      fleeting_share[i].flt_tlshare
		      );
	    }
	}
      fclose(fp_fleeting_share);
      
      char full_path_to_fleeting_ref[9999];
      sprintf(full_path_to_fleeting_ref,"%s/fleeting_ref_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_fleeting_ref = fopen(full_path_to_fleeting_ref,"w");
      fprintf(fp_fleeting_ref,"ref,flt_tlshare\n");
      unsigned long long ref_fleeting = -1;
      for(int i=0;i<totalSettled_except_EC;i++)
	{
	  if(fleeting_share[i].flt==1)
	    {
	      if(ref_fleeting!=fleeting_share[i].ref)
		{
		  fprintf(fp_fleeting_ref, "%lld,%d\n",
			  fleeting_share[i].ref,
			  fleeting_share[i].flt_tlshare
			  );
		  
		  ref_fleeting = fleeting_share[i].ref;
		}
	    }
	}
      fclose(fp_fleeting_ref);
      
      ////// Generate settlement file without fleeting orders
      // merge MSFT with fleeting_share
      // use AA,clear
      
      rss_start = time(NULL);
      qsort(MSFT,totalMsg_per_stock2,
	    sizeof(LOB_message_FloatSeq_Fleeting),compare_ref_LOB_FloatSeq_Fleeting);
      rss_end = time(NULL);
      printf("RSS part 1: %ld seconds\n", rss_end-rss_start);
      
      rss_start = time(NULL);
      qsort(fleeting_share,totalSettled_except_EC,
	    sizeof(LOB_message_FloatSeq_Fleeting),compare_ref_LOB_FloatSeq_Fleeting);
      rss_end = time(NULL);
      printf("RSS part 2: %ld seconds\n", rss_end-rss_start);
      
      struct LOB_message_FloatSeq_Fleeting *MSFT_wof = (struct LOB_message_FloatSeq_Fleeting*)
	calloc(totalMsg_per_stock2,sizeof(LOB_message_FloatSeq_Fleeting));
      CALLOC_SAFE_CALL(MSFT_wof);
      
      rss_start = time(NULL);
      for(int i=0;i<totalMsg_per_stock2;i++)
	{
	  MSFT_wof[i].ref = MSFT[i].ref;
	  MSFT_wof[i].time = MSFT[i].time;
	  MSFT_wof[i].timeAddAll = MSFT[i].timeAddAll;
	  MSFT_wof[i].BuySell = MSFT[i].BuySell;
	  MSFT_wof[i].share = MSFT[i].share;
	  MSFT_wof[i].price = MSFT[i].price;
	  strcpy(MSFT_wof[i].stock,MSFT[i].stock);
	  strcpy(MSFT_wof[i].msg_type, MSFT[i].msg_type);
	  MSFT_wof[i].seq_no_current_order = MSFT[i].seq_no_current_order;
	  MSFT_wof[i].seq_no_original_order = MSFT[i].seq_no_original_order;
	  // The following four variables are un-used in the context below:
	  MSFT_wof[i].settle_time = -1;
	  MSFT_wof[i].min_settle = -1;
	  MSFT_wof[i].flt = -1;
	  MSFT_wof[i].flt_tlshare = -1;
	}
      rss_end = time(NULL);
      printf("RSS part 3: %ld seconds\n", rss_end-rss_start);
      
      
      // actual merge
      //merge 1:1 ref seqcurrent using fleeting_share, nogenerate
      rss_start = time(NULL);
      int tracker_in_fleeting_share = 0;
#pragma omp parallel for private(i, j) firstprivate(tracker_in_fleeting_share) schedule(dynamic, 100)
      for(int i=0;i<totalMsg_per_stock2;i++) {
	for(int j=tracker_in_fleeting_share;j<totalSettled_except_EC2;j++) {
	  if( (MSFT_wof[i].ref==fleeting_share[j].ref) && 
	      (MSFT_wof[i].seq_no_current_order==fleeting_share[j].seq_no_current_order) &&
	      (fleeting_share[j].flt==1) ) {
	    
	    MSFT_wof[i].flt = fleeting_share[j].flt;
	    MSFT_wof[i].flt_tlshare = fleeting_share[j].flt_tlshare;
	    tracker_in_fleeting_share = j;
	    
	    break;
	  }
	}
      }
      rss_end = time(NULL);
      printf("RSS part 4: %ld seconds\n", rss_end-rss_start);
      
      
      // keep in mind:
      // MSFT_wof should be treated as after "drop if flt_flshare != ."
      // merge m:1 ref using fleeting_ref, nogenerate
      
      // RSSXXX --- Start loop had been taking hour for tickerno=10
      rss_start = time(NULL);
      tracker_in_fleeting_share = 0;
#pragma omp parallel for private(i, j) firstprivate(tracker_in_fleeting_share) schedule(dynamic, 100)
      for(int i=0;i<totalMsg_per_stock2;i++) {
	if (MSFT_wof[i].flt_tlshare != -1) continue;
	for(int j=tracker_in_fleeting_share;j<totalSettled_except_EC2;j++) {
	  if( (MSFT_wof[i].ref==fleeting_share[j].ref) && 
	      (fleeting_share[j].flt==1) )/*bc: drop if flt_tlshare != . */
	    {
	      MSFT_wof[i].flt_tlshare = fleeting_share[j].flt_tlshare;
	      tracker_in_fleeting_share = j;
	      
	      //replace share = share - flt_tlshare if (flt_tlshare != . & (msg == "A" | msg =="F" | msg == "Au"))
	      if( (!strcmp(MSFT_wof[i].msg_type,"A")) ||
		  (!strcmp(MSFT_wof[i].msg_type,"F")) ||
		  (!strcmp(MSFT_wof[i].msg_type,"Au")) )
		{
		  MSFT_wof[i].share -= MSFT_wof[i].flt_tlshare;
		}
	      
	      //skip to the next different reference number entry
	      //tracker_in_fleeting_share = j;
	      //while(MSFT_wof[i].ref==fleeting_share[tracker_in_fleeting_share].ref)
	      //{
	      // tracker_in_fleeting_share += 1;
	      //}
	      
	      break;
	    }
	}
	
      }
      rss_end = time(NULL);
      printf("RSS part 5: %ld seconds\n", rss_end-rss_start);
      // RSSXXX --- End loop had been taking hour for tickerno=10
      
      
      rss_start = time(NULL);
      qsort(MSFT_wof,totalMsg_per_stock2,
	    sizeof(LOB_message_FloatSeq_Fleeting),compare_seqcurrent_LOB_FloatSeq_Fleeting);
      rss_end = time(NULL);
      printf("RSS part 6: %ld seconds\n", rss_end-rss_start);
      
      
      
      //write MSFT_wof, MSFT_wof_settled.csv and msg_wof.csv
      char full_path_to_MSFT_wof[9999];
      sprintf(full_path_to_MSFT_wof,"%s/MSFT_wof_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_MSFT_wof = fopen(full_path_to_MSFT_wof,"w");
      fprintf(fp_MSFT_wof,"ref,time,timeaddall,buy,share,price,stock,msg,seqcurrent,seqoriginal\n");
      
      char full_path_to_MSFT_wof_settled[9999];
      sprintf(full_path_to_MSFT_wof_settled,"%s/settled_wof_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_MSFT_wof_settled = fopen(full_path_to_MSFT_wof_settled,"w");
      fprintf(fp_MSFT_wof_settled,"ref,time,timeaddall,buy,share,price,stock,msg,seqcurrent,seqoriginal\n");
      
      char full_path_to_msg_wof[9999];
      sprintf(full_path_to_msg_wof,"%s/msg_wof_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      FILE *fp_msg_wof = fopen(full_path_to_msg_wof,"w");
      fprintf(fp_msg_wof,"ref,time,buy,share,price,msg,seqcurrent\n");
      
      for(int i=0;i<totalMsg_per_stock2;i++)
	{
	  
	  //0. MSFT_wof.csv
	  if( (MSFT_wof[i].share!=0) &&
	      (MSFT_wof[i].flt==-1)   )
	    {
#if 0		   
	      fprintf(fp_MSFT_wof, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f,%.9f,%.9f,%d,%d\n", 
		      MSFT_wof[i].ref,
		      MSFT_wof[i].time,
		      MSFT_wof[i].timeAddAll,
		      MSFT_wof[i].BuySell,
		      MSFT_wof[i].share,
		      MSFT_wof[i].price,
		      MSFT_wof[i].stock,
		      MSFT_wof[i].msg_type,
		      MSFT_wof[i].seq_no_current_order,
		      MSFT_wof[i].seq_no_original_order,
		      MSFT_wof[i].settle_time,
		      MSFT_wof[i].min_settle,
		      MSFT_wof[i].flt,
		      MSFT_wof[i].flt_tlshare
		      );
#else
	      fprintf(fp_MSFT_wof, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f\n", 
		      MSFT_wof[i].ref,
		      MSFT_wof[i].time,
		      MSFT_wof[i].timeAddAll,
		      MSFT_wof[i].BuySell,
		      MSFT_wof[i].share,
		      MSFT_wof[i].price,
		      MSFT_wof[i].stock,
		      MSFT_wof[i].msg_type,
		      MSFT_wof[i].seq_no_current_order,
		      MSFT_wof[i].seq_no_original_order
		      );
#endif
	    }
	  
	  
	  
	  //1. MSFT_settled.csv
	  if ( (!strcmp(MSFT_wof[i].msg_type,"Au")) || 
	       (!strcmp(MSFT_wof[i].msg_type,"A"))  || 
	       (!strcmp(MSFT_wof[i].msg_type,"F"))   ) {
	    
	  }
	  else {
	    if ( (MSFT_wof[i].share!=0) && 
		 (MSFT_wof[i].flt==-1)   )
	      {
#if 0
 	        fprintf(fp_MSFT_wof_settled, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f,%.9f,%.9f,%d,%d\n", 
			MSFT_wof[i].ref,
			MSFT_wof[i].time,
			MSFT_wof[i].timeAddAll,
			MSFT_wof[i].BuySell,
			MSFT_wof[i].share,
			MSFT_wof[i].price,
			MSFT_wof[i].stock,
			MSFT_wof[i].msg_type,
			MSFT_wof[i].seq_no_current_order,
			MSFT_wof[i].seq_no_original_order,
			MSFT_wof[i].settle_time,
			MSFT_wof[i].min_settle,
			MSFT_wof[i].flt,
			MSFT_wof[i].flt_tlshare
			);
#else
 	        fprintf(fp_MSFT_wof_settled, "%lld,%.9f,%.9f,%c,%d,%.4f,%s,%s,%.2f,%.2f\n", 
			MSFT_wof[i].ref,
			MSFT_wof[i].time,
			MSFT_wof[i].timeAddAll,
			MSFT_wof[i].BuySell,
			MSFT_wof[i].share,
			MSFT_wof[i].price,
			MSFT_wof[i].stock,
			MSFT_wof[i].msg_type,
			MSFT_wof[i].seq_no_current_order,
			MSFT_wof[i].seq_no_original_order
			);
		
#endif
	      }
	  }
	  
	  //2. write msg_wof
	  if ( (MSFT_wof[i].share!=0) && 
	       (MSFT_wof[i].flt==-1)   )
	    {
	      fprintf(fp_msg_wof, "%lld,%.9f,%c,%d,%.4f,%s,%.2f\n", 
		      MSFT_wof[i].ref,
		      MSFT_wof[i].time,
		      MSFT_wof[i].BuySell,
		      MSFT_wof[i].share,
		      MSFT_wof[i].price,
		      MSFT_wof[i].msg_type,
		      MSFT_wof[i].seq_no_current_order
		      );
	    }
	  
	}
      fclose(fp_msg_wof);
      fclose(fp_MSFT_wof);
      fclose(fp_MSFT_wof_settled);
      
      
      
      
      
      
      
      
      
      
      
      /* LOB construction for the stock (out of n tickers) */
      printf("LOB construction for the stock %s\n",the_stock_name);
      int lob_argc = 5;
      int lob_arg_size = 9999;
      char **lob_argv = (char**) calloc(lob_argc,sizeof(char*));
      for(int i=0;i<lob_argc;i++)
	{
	  lob_argv[i] = (char*) calloc(lob_arg_size,sizeof(char));
	}
      
      /* 
       * e.g., path-to-dir/msg_GOOG_052311.csv 
       *
       * ./lob_main [full path to msg.csv] [full path to settled.csv] [full path to output LOB.csv] [number of cores]
       *
       */
      
      // first, do msg.csv and settled.csv pair
      the_count = 0;
      sprintf(lob_argv[0],"lob_main");
      sprintf(lob_argv[1],"%s/msg_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      sprintf(lob_argv[2],"%s/settled_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      sprintf(lob_argv[3],"%s/LOB_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      sprintf(lob_argv[4],"%s",argv[4]);
      lob_main(lob_argc, lob_argv);
      
      // second, do msg_wof.csv and settled.csv pair
      the_count = 0;
      sprintf(lob_argv[0],"lob_main");
      sprintf(lob_argv[1],"%s/msg_wof_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      sprintf(lob_argv[2],"%s/settled_wof_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      sprintf(lob_argv[3],"%s/LOB_wof_%s_%s.csv",
	      ticker_foldername_output.c_str(),the_stock_name,argv[2]);
      sprintf(lob_argv[4],"%s",argv[4]);
      lob_main(lob_argc, lob_argv);
      
      
      
      
      
      
      // garbage collection 
      for(int i=0;i<lob_argc;i++)
	{
	  free(lob_argv[i]);
	}
      free(lob_argv);
      free(MSFT);
      free(MSFT_settled);
      free(fleeting_share);
      free(MSFT_wof);
      
    }
  
  
  
  // gabage collection
  free(stocksn);
  for(int i=0;i<how_many_tickers;i++) {
    free(Targeted_ticker[i]);
  }
  free(Targeted_ticker);
  
  return 1;
}
