/*
(C) Copyright 2012 The Board of Trustees of the University of Illinois.
All rights reserved.

Developed by:

                        Department of Finance
                University of Illinois, Urbana Champaign

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal with the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of the IMPACT Research Group, MRFIL Research Group, the
University of Illinois, nor the names of its contributors may be used to
endorse or promote products derived from this Software without specific
prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
THE SOFTWARE.
*/

/*****************************************************************************

    File Name   [LobUtility.h]

    Synopsis    [Utility functions for simple operations (sort, search etc).]

    Description []

    Revision    [1.0; Jiading Gai, Finance Department UIUC]
    
	Date        [08/12/2013]

******************************************************************************/


#ifndef LOBUTILITY_H
#define LOBUTILITY_H

#include <stdio.h>
#include <sys/types.h>

#define CALLOC_SAFE_CALL(return_value_from_calloc)                  \
    if (NULL == return_value_from_calloc) {                         \
        fprintf(stderr, "Calloc failed in file '%s' in line %i.\n", \
                __FILE__, __LINE__);                                \
	    exit(1);                                                    \
    }

#define checkOpenFile(fid,fnstr)                      \
    if (NULL == fid) {                                \
        fprintf(stderr, "error opening %s\n", fnstr); \
		exit(1);                                      \
	}                                                 \
    

#define checkResults(string, val) {                                  \
 if (val) {                                                          \
   printf("Failed with %d at %s (Line:%i)\n", val, string,__LINE__); \
   exit(1);                                                          \
 }                                                                   \
}

/* U message structure (Nasdaq ITCH 4.1)
 *
 * seq_no - the overall sequence number over all 
 *          msg types in a day.
 */
struct U_message{
	double time;
	unsigned long long old_ref;
	unsigned long long new_ref;
	unsigned int share;
	double price;
	int seq_no;
};

/* Augumented on top of U message structure
 *
 * ref0 - 
 * old_idx - index to each message location before sort.
 *           U_message needs to be sorted w.r.t. new_ref,
 *           but, after sorting w.r.t. new_ref, we need 
 *           to know each msg's old index in the original 
 *           unsorted U message array.
 *           note that 'U' comes with new_ref in sorted order.
 *           
 */
struct U_message2{
    struct U_message umsg;
	unsigned long long ref0;
	int old_idx;
};

struct A_message {
    double time;
	unsigned long long ref_no;
	char BuySell;
	unsigned int share;
	char stock[9];
	double price;
	/*
     * seq_no is the overall sequence number over 
     * all msg types in a day
     */
	int seq_no;
};

struct F_message {
    double time;
	unsigned long long ref_no;
	char BuySell;
	unsigned int share;
	char stock[9];
	double price;
	char mpid[5];
	/*
     * seq_no is the overall sequence number over 
     * all msg types in a day
     */
	int seq_no;
};

/*struct LOB_message is the msg format of the BIG FILE
 * 
 *
 * ref  - reference number.
 * time - the message's own start time.
 * timeAddAll - the start time for the 'A' or 'F' message 
 *              that this message traces back to.
 * seq_no_original_order - original order to the trace-backed 'A' or 'F'.
 */
struct LOB_message{
    unsigned long long ref;
	double time;
	double timeAddAll;
	char BuySell;
    unsigned int share;
	double price;
	char stock[9];
    char msg_type[3];
	int seq_no_current_order;
	int seq_no_original_order;
};
struct LOB_message *read_LOB_message(const char *filename, unsigned int *plen);

/* LOB_message_FloatSeq is same as LOB_message, 
 * only with seq no in float type.
 * 
 * ref  - reference number
 * time - the message's own start time
 * timeAddAll - the start time for the 'A' or 'F' message that this message 
 *              traces back to
 * seq_no_original_order - original order to the trace-backed 'A' or 'F'
 * min_settle - by ref
 */
struct LOB_message_FloatSeq{
    unsigned long long ref;
	double time;
	double timeAddAll;
	char BuySell;
    unsigned int share;
	double price;
	char stock[9];
    char msg_type[3];
	double seq_no_current_order;
	double seq_no_original_order;
};

/* LOB_message_FloatSeq_Fleeting is same as LOB_message, 
 * only with seq no in float type and four additional members
 * related to fleeting etc.
 * 
 * ref  - reference number
 * time - the message's own start time
 * timeAddAll - the start time for the 'A' or 'F' message that this message 
 *              traces back to
 * seq_no_original_order - original order to the trace-backed 'A' or 'F'
 * min_settle - by ref
 */
struct LOB_message_FloatSeq_Fleeting{
    unsigned long long ref;
	double time;
	double timeAddAll;
	char BuySell;
    unsigned int share;
	double price;
	char stock[9];
    char msg_type[3];
	double seq_no_current_order;
	double seq_no_original_order;
	double settle_time;
	double min_settle;
	int flt;
	int flt_tlshare;
};

/*
 * LOB_stock_date structure is defined to facilitate
 * the I/O of the limit order book for a particular 
 * stock. For example, the following file and its 
 * format declared within:
 *
 *   lob_deadline/lob_050510/COST/LOB_COST_050510.csv
 *
 */
 //FIXME: I think in this paper the 'price' type should
 //all be double as opposed to float, even 'price' only 
 //has 4 digits after the decimal point. The reason is 
 //that not all real stock prices can be described without
 //precision error, this can cause problems when a given
 //price is to be compared against a bid-ask range, for
 //exampe, the following situation:
 //     price = 59.98, bid = 60.08
 //     => (price < (bid-1)) is false
 // 
 //     Now, assume both price and bid are in float type
 //     then, price = 59.980000, bid = 60.080002 = atof("60.08")
 //     => (price < (bid - 1)) is now true (wrong)
 //
 //Hence, this FIXME involves changing all price type from float to double;
 //Don't forget to do regression testing along the way, just to make sure.
struct LOB_stock_date{
	double ask;
	int depth10_s;
	int depth_s;
	double addseq;
	double bid;
	int depth10_b;
	int depth_b;
	unsigned long long addref;
	double addtime;
	double addprice;
	char addbuy;
	int addshare;
	char addmsg[3];
};

/*
 * Assign a struct LOB_stock_date object pointed to by src to
 * a struct LOB_stock_date object pointed to by dst
 */
void assign_LOB_stock_date(struct LOB_stock_date *dst, struct LOB_stock_date *src);

struct LOB_stock_date *read_LOB_stock_date_message(const char *filename, unsigned int *plen);

/*
 * This function is used to trim the 'addtime' member of 'in' to be
 * between 9:30:00 - 15:59:59.
 *
 * time_start and time_finish are in seconds.
 *
 * For example,  9:30:00 translates to 34200 seconds
 *				15:59:59 translates to 57599 seconds 
 *
 * unsigned int 'in_len' is the length of the input LOB_stock_date structure
 * unsigned int 'pout_len' is the length of the trimmed output structure
 */
struct LOB_stock_date *keep_between_time_range(struct LOB_stock_date *in, 
											   unsigned int in_len,
											   unsigned int *pout_len,
											   double time_start,
											   double time_finish);

/*
 *
 */
struct fleeting_ref {
	unsigned long long ref;
	int flt_tlshare;
};
struct fleeting_ref *read_fleeting_ref_message(const char *filename, unsigned int *plen);

/*
 *
 */
 struct LOB_C_date {
	unsigned long long ref;
	double time;
	double timeAddAll;
	char buy;
	int share;
	double price;
	double exec_price;
	char stock[9];
	char msg[3];
	double seqcurrent;
	double seqoriginal;
 };
 struct LOB_C_date *read_LOB_C_date_message(const char *filename, unsigned int *plen);


/*
 * Read seq_U.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct U_message2'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct U_message2 *read_U2_message(const char *filename, unsigned int *plen);
struct U_message2 *read_U2_message_test(const char *filename, unsigned int *plen);

/*
 * Read seq_A.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct A_message'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct A_message *read_A_message(const char * filename, unsigned int *plen);
struct A_message *read_A_message_test(const char * filename, unsigned int *plen);

/*
 * Read seq_F.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct F_message'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct F_message *read_F_message(const char * filename, unsigned int *plen);

/*
 * Read seq_E.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct LOB_message'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct LOB_message *read_E_message(const char * filename, unsigned int *plen);

/*
 * Read seq_C.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct LOB_message'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct LOB_message *read_C_message(const char * filename, unsigned int *plen);

/*
 * Read seq_X.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct LOB_message'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct LOB_message *read_X_message(const char * filename, unsigned int *plen);

/*
 * Read seq_D.csv from disk msg-by-msg, and save them in an array of 
 * type 'struct LOB_message'; Meanwhile, the length of the array is 
 * given by the pass-in argument 'unsigned int *plen'.
 */
struct LOB_message *read_D_message(const char * filename, unsigned int *plen);



void print_LOB_message(FILE *fp, struct LOB_message *msg, int index);
void write_LOB_message(const char *output_filename, struct LOB_message *msg, int len);

void 
write_LOB_Message_FloatSeq_Fleeting(struct LOB_message_FloatSeq_Fleeting *a, 
                                     int len, const char *output_filename);

/* Sorting functions on various message types
 * 
 * Used by qsort as an input argument.
 */
int compare_ticker_name_LOB (const void * a, const void * b);
int compare_ref_LOB (const void * a, const void * b);
int compare_ref_LOB_FloatSeq_Fleeting (const void * a, const void * b);

int compare_seqcurrent_LOB (const void * a, const void * b);
int compare_seqcurrent_LOB_FloatSeq_Fleeting (const void * a, const void * b);

int compare_ref0_U (const void * a, const void * b);
int compare_oldref_U (const void * a, const void * b);
int compare_refno_A (const void * a, const void * b);
int compare_refno_F (const void * a, const void * b);

int compare_addref_LOB_stock_date(const void *a, const void *b);
int compare_addseq_LOB_stock_date(const void *a, const void *b);
int compare_ref_fleeting_ref(const void *a, const void* b);

int create_folder_for_paulie(const char *pathname, mode_t mode);
#endif
