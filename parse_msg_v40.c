/*
(C) Copyright 2012 The Board of Trustees of the University of Illinois.
All rights reserved.

Developed by:

                        Department of Finance
                University of Illinois, Urbana Champaign

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal with the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of the Department of Finance, the University of Illinois, 
nor the names of its contributors may be used to endorse or promote products 
derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH
THE SOFTWARE.
*/

/*****************************************************************************

    File Name   [parse_msg_v40.c]

    Synopsis    [NASDAQ ITCH 4.0 parser.]

    Description [NASDAQ TotalView-ITCH 4.0 reference manual.]

    Revision    [1.0; Jiading Gai, Finance Department UIUC]
    
	Date        [08/12/2013]

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

#define WRITE2DISK 1

// Endianness coversion from big to little
// (eight bytes)
unsigned long long ByteSwap8(unsigned long long aNumber)
{
  return (((aNumber&0x00000000000000FF)<<56)+((aNumber&0x000000000000FF00)<<40)+
          ((aNumber&0x0000000000FF0000)<<24)+((aNumber&0x00000000FF000000)<< 8)+
          ((aNumber&0xFF00000000000000)>>56)+((aNumber&0x00FF000000000000)>>40)+
          ((aNumber&0x0000FF0000000000)>>24)+((aNumber&0x000000FF00000000)>> 8));
}
// (four bytes)
unsigned int ByteSwap4(unsigned int aNumber)
{
  return (((aNumber&0x000000FF)<<24)+((aNumber&0x0000FF00)<< 8)+
          ((aNumber&0x00FF0000)>> 8)+((aNumber&0xFF000000)>>24));
}
// (two bytes)
unsigned short ByteSwap2(unsigned short aNumber)
{
  return (((aNumber&0x00FF)<<8)+((aNumber&0xFF00)>>8));
}

int main(int argc, char *argv[])
{

  if(argc<4) {
      printf("Usage: parse_msg [input folder path] [input file name] [output folder path]\n");
	  abort();
  }

  time_t start_parse =  time(NULL);

  unsigned int count_subpenny =0;
  char msgType[17] = {'T','S','R','H','Y','L','A','F','E','C','X','D','U','P','Q','B','I'};
  char full_path_to_input_file[999];
  //argv[1]: folder pathname
  //argv[2]: file pathname
  sprintf(full_path_to_input_file,"%s/%s",argv[1],argv[2]);
 
  FILE *fp_in = fopen(full_path_to_input_file,"r");

  char full_path_to_output_folder[999];
  sprintf(full_path_to_output_folder,"%s/%s_out",argv[3],argv[2]);

  int mkdir_return_value = mkdir(full_path_to_output_folder,0777);
  if(-1==mkdir_return_value) {
      if(EEXIST==errno){
	      printf("Output directory %s already exists ... overwrite!\n",full_path_to_input_file);
      }
      else if(EACCES==errno) {
          printf("The parent directory does not allow write permission to the process.\n");
          abort();
      }
      else if(ENAMETOOLONG==errno) {
          printf("Pathname was too long.\n");
          abort();
      }
  }
  
  char csv_filename[999];//Upper limit on filename length: 999 characters 
  FILE *out_filename[17];
  for(int j=0;j<17;j++) {
     sprintf(csv_filename,"%s/seq_%c.csv",full_path_to_output_folder,msgType[j]);
	 out_filename[j] = fopen(csv_filename,"w");
  }

  char* msg_buffer = NULL;
  unsigned short *msg_len_info = NULL;
  //first two bytes before each msg starts encodes the length of the msg.
  msg_len_info = (unsigned short*)calloc(2,sizeof(char));
  unsigned short msg_length = 0;
  int total = 0;//total number of messages
  //there are a total of 17 message types. totalType counts type-wise total for each message type.
  int *totalType = (int*) calloc(17,sizeof(int));  
 
  
  printf("=========== Parsing v4.0 starts ===========\n");
  printf("Input file: %s\n",full_path_to_input_file);
  printf("Output folder: %s\n",full_path_to_output_folder);
  
  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////  First Pass - count the number of messages for each type  /////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  unsigned int timestamp_seconds = 0;
  while(1) {
     if(2 != fread((void*)msg_len_info,sizeof(char),2,fp_in)) { 
        printf("EOF!\n");
		break;
	 }

	 total++;//it also serves as the sequence number for the current message.
	 // big endian to little endian
	 msg_length = ByteSwap2(*msg_len_info);

     msg_buffer = (char*)malloc(msg_length*sizeof(char));
     fread((void*)msg_buffer,sizeof(char),msg_length,fp_in);
	 if('T'==msg_buffer[0]) {
        unsigned int timestamp_str = *((unsigned int*)(msg_buffer+1));
		timestamp_seconds = ByteSwap4(timestamp_str);
	    unsigned int hour = (unsigned int)floor(timestamp_seconds / 3600.0f);
		unsigned int minutes = (unsigned int)floor(timestamp_seconds/60) % 60;
#if WRITE2DISK		
		fprintf(out_filename[0],"\nTime Stamp:%2d:%02d,%d", hour, minutes, total);
#endif
        totalType[0]++;
	 }
	 if('S'==msg_buffer[0]) {
    	unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));
		unsigned int time_nano = ByteSwap4(time_nano_BE);
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
        fprintf(out_filename[1],"%.9lf,%c,%d\n", time_Combined, msg_buffer[5], total);
#endif
        totalType[1]++;
	 }
	 if('R'==msg_buffer[0]) {
		// Stock Directory
		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
		{
			stock[j] = msg_buffer[j+5];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';

		unsigned int RoundLotSize_str = *((unsigned int*)(msg_buffer+13));
		unsigned int RoundLotSize = ByteSwap4(RoundLotSize_str);
 
		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));
		unsigned int time_nano = ByteSwap4(time_nano_BE);
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
        fprintf(out_filename[2],"%.9lf,%s,%c,%c,%d,%c,%d\n", time_Combined, stock, 
				msg_buffer[11], msg_buffer[12], RoundLotSize, msg_buffer[17], total);
#endif
        totalType[2]++;
	 }
	 if('H'==msg_buffer[0]) {
		// Stock trading action
		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
        {	
			stock[j] = msg_buffer[j+5];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';

		char reason[5];
		for(int j=0;j<4;j++)
			reason[j] = msg_buffer[j+13];
		reason[4] = '\0';


		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
		fprintf(out_filename[3],"%.9f,%s,%c,%c,%s,%d\n", time_Combined, stock, msg_buffer[11], msg_buffer[12], reason, total);
#endif
        totalType[3]++;
	 }
	 /*v40 has no 'Y' type message*/
	 if('L'==msg_buffer[0]) {
		// Market Participant Position
		char mpid[5];
		for(int j=0;j<4;j++)
			mpid[j] = msg_buffer[j+5];
		mpid[4] = '\0';

		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
		{
			stock[j] = msg_buffer[j+9];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
		fprintf(out_filename[5], "%.9f,%s,%s,%c,%c,%c,%d\n", time_Combined, mpid, stock, 
		msg_buffer[15], msg_buffer[16], msg_buffer[17], total );	
#endif
        totalType[5]++;
	 }
	 if('A'==msg_buffer[0]) {
        // Add order: no mpid attribution
		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
		{
			stock[j] = msg_buffer[j+18];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';

		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+14));
		unsigned int share = ByteSwap4(share_BE);

		unsigned int price_BE = *((unsigned int*)(msg_buffer+24));
		float price = ((float) ByteSwap4(price_BE)) / 10000.0f;


		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

		//sub-penny pricing

#if WRITE2DISK		
//		if((ByteSwap4(price_BE) % 100) && price > 1.0f) {
           fprintf(out_filename[6], "%.9f,%lld,%c,%d,%s,%.4f,%d\n",time_Combined, ref_no, msg_buffer[13], share, stock, price, total );
//		   count_subpenny++;
//		}
#endif
        totalType[6]++;
	 }
	 if('F'==msg_buffer[0]) {
        // Add order with mpid attribution
		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
		{
			stock[j] = msg_buffer[j+18];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';


		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+14));
		unsigned int share = ByteSwap4(share_BE);

		unsigned int price_BE = *((unsigned int*)(msg_buffer+24));
		float price = ((float) ByteSwap4(price_BE)) / 10000.0f;

		char mpid[5];
		for(int j=0;j<4;j++)
			mpid[j] = msg_buffer[j+28];
		mpid[4] = '\0';


		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
		fprintf(out_filename[7], "%.9f,%lld,%c,%d,%s,%.4f,%s,%d\n",time_Combined, ref_no, msg_buffer[13], share, stock, price, mpid, total );		 
#endif
        totalType[7]++;
	 }
	 if('E'==msg_buffer[0]) {
        // Order executed message
		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+13));
		unsigned int share = ByteSwap4(share_BE);

        unsigned long long match_BE = *((unsigned long long*)(msg_buffer+17));
		unsigned long long match = ByteSwap8(match_BE);

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

		
#if WRITE2DISK		
		fprintf(out_filename[8], "%.9f,%lld,%d,%lld,%d\n", time_Combined, ref_no, share, match, total);
#endif
        totalType[8]++;
	 }
	 if('C'==msg_buffer[0]) {
		// Order executed with price message
		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+13));
		unsigned int share = ByteSwap4(share_BE);

        unsigned long long match_BE = *((unsigned long long*)(msg_buffer+17));
		unsigned long long match = ByteSwap8(match_BE);

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

		unsigned int price_BE = *((unsigned int*)(msg_buffer+26));
		float price = ((float) ByteSwap4(price_BE)) / 10000.0f;

#if WRITE2DISK		
		fprintf(out_filename[9], "%.9f,%lld,%d,%lld,%c,%.4f,%d\n", time_Combined, ref_no, share, match, msg_buffer[25], price, total );
#endif
        totalType[9]++;
	 }
	 if('X'==msg_buffer[0]) {
		// Order Cancel Message
		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+13));
		unsigned int share = ByteSwap4(share_BE);

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;


#if WRITE2DISK		
        fprintf(out_filename[10], "%.9f,%lld,%d,%d\n", time_Combined, ref_no, share, total );
#endif
        totalType[10]++;
	 }
	 if('D'==msg_buffer[0]) {
		// Order delete message
		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
		fprintf(out_filename[11], "%.9f,%lld,%d\n",time_Combined, ref_no, total);
#endif
        totalType[11]++;
	 }
	 if('U'==msg_buffer[0]) {
        // Order replace message
		unsigned long long old_ref_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long old_ref = ByteSwap8(old_ref_BE);

		unsigned long long new_ref_BE = *((unsigned long long*)(msg_buffer+13));
		unsigned long long new_ref = ByteSwap8(new_ref_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+21));
		unsigned int share = ByteSwap4(share_BE);

		unsigned int price_BE = *((unsigned int*)(msg_buffer+25));
		float price = ((float) ByteSwap4(price_BE)) / 10000.0f;


		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
		fprintf(out_filename[12], "%.9f,%lld,%lld,%d,%.4f,%d\n", time_Combined, old_ref, new_ref, share, price, total);
#endif
        totalType[12]++;
	 }
	 if('P'==msg_buffer[0]) {
        // Trade Message (Non-Cross)
		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
		{
			stock[j] = msg_buffer[j+18];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';


		unsigned long long ref_no_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long ref_no = ByteSwap8(ref_no_BE);

        unsigned int share_BE = *((unsigned int*)(msg_buffer+14));
		unsigned int share = ByteSwap4(share_BE);

        unsigned long long match_BE = *((unsigned long long*)(msg_buffer+28));
		unsigned long long match = ByteSwap8(match_BE);

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

		unsigned int price_BE = *((unsigned int*)(msg_buffer+24));
		float price = ((float) ByteSwap4(price_BE)) / 10000.0f;


#if WRITE2DISK		
//		if((ByteSwap4(price_BE) % 100) && price > 0.0f) {
		fprintf(out_filename[13], "%.9f,%lld,%c,%d,%s,%.4f,%lld,%d\n", time_Combined, ref_no, msg_buffer[13], share, stock, price, match, total);
//		count_subpenny++;
//		}
#endif
        totalType[13]++;
	 }
	 if('Q'==msg_buffer[0]) {
        //Cross Trade Message
        // Trade Message (Non-Cross)
		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++)
		{
			stock[j] = msg_buffer[j+13];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';


		/*Note that share has a length of 8 bytes in 'Q' type*/
        unsigned long long share_BE = *((unsigned long long*)(msg_buffer+5));
		unsigned long long share = ByteSwap8(share_BE);

		unsigned int price_BE = *((unsigned int*)(msg_buffer+19));
		float price = ((float) ByteSwap4(price_BE)) / 10000.0f;

        unsigned long long match_BE = *((unsigned long long*)(msg_buffer+23));
		unsigned long long match = ByteSwap8(match_BE);

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;


#if WRITE2DISK		
 		fprintf(out_filename[14], "%.9f,%lld,%s,%.4f,%lld,%c,%d\n", time_Combined, share, stock, price, match, msg_buffer[31], total);
#endif
        totalType[14]++;
	 }
	 if('B'==msg_buffer[0]) {
       //Broken Trade/Order Execution Message
       unsigned long long match_BE = *((unsigned long long*)(msg_buffer+5));
       unsigned long long match = ByteSwap8(match_BE);


	   unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
	   unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
	   double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;

#if WRITE2DISK		
       fprintf(out_filename[15],"%.9f,%lld,%d\n", time_Combined, match, total);
#endif
       totalType[15]++;
	 }
	 if('I'==msg_buffer[0]) {
        // NOII Message
       unsigned long long PairedShares_BE = *((unsigned long long*)(msg_buffer+5));
       unsigned long long PairedShares = ByteSwap8(PairedShares_BE);

       unsigned long long Imbalance_BE = *((unsigned long long*)(msg_buffer+13));
       unsigned long long Imbalance = ByteSwap8(Imbalance_BE);

  		char stock[9];
		int not_space = 0;
		for(int j=0;j<6;j++) 
		{
			stock[j] = msg_buffer[j+22];
			if(' '!=stock[j])
				not_space++;
			else
				break;
		}
		stock[not_space] = '\0';
     
		unsigned int FarPrice_BE = *((unsigned int*)(msg_buffer+28));
		float FarPrice = ((float) ByteSwap4(FarPrice_BE)) / 10000.0f;

		unsigned int NearPrice_BE = *((unsigned int*)(msg_buffer+32));
		float NearPrice = ((float) ByteSwap4(NearPrice_BE)) / 10000.0f;

        
		unsigned int CurrentReferencePrice_BE = *((unsigned int*)(msg_buffer+36));
		float CurrentReferencePrice = ((float) ByteSwap4(CurrentReferencePrice_BE)) / 10000.0f;

		unsigned int time_nano_BE = *((unsigned int*)(msg_buffer+1));//time in nanoseconds:big endian
		unsigned int time_nano = ByteSwap4(time_nano_BE);//Endianess conversion:big to little
		double time_Combined = (double) timestamp_seconds + (double) time_nano / 1000000000.0f;
		 
#if WRITE2DISK		
       fprintf(out_filename[16],"%.9f,%lld,%lld,%c,%s,%.4f,%.4f,%.4f,%c,%c,%d\n", time_Combined, PairedShares, Imbalance, msg_buffer[21], stock, FarPrice, NearPrice, CurrentReferencePrice, msg_buffer[40], msg_buffer[41], total);
#endif
        totalType[16]++;
	 }

	 free(msg_buffer);
  }
  printf("total  = %d\n",total);
  int total2 =0;
  for(int j=0;j<17;j++) {
     printf("Type[%d] = %d\n",j,totalType[j]);
	 total2 += totalType[j];
  }
  printf("total %d = total2 %d\n",total, total2);
  fflush(stdout);


  // garbage collection ...
  fclose(fp_in);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////// write data to disk  /////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////

  for(int j=0;j<17;j++) {
	 fclose(out_filename[j]);
  }

  printf("%d v.s. %d\n", count_subpenny, totalType[6]);

  time_t end_parse = time(NULL);
  printf("Time spent: %ld seconds\n", (end_parse-start_parse));
 
  return 1;
}
